package chrome.umf.controller.artist;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import chrome.umf.activities.R;
import chrome.umf.controller.map.Map;
import chrome.umf.model.artist.ArtistRecord;
import chrome.umf.model.day.DayRecord;
import chrome.umf.model.lookUpCountry.LookUpCountryRecord;
import chrome.umf.model.performance.PerformanceRecord;
import chrome.umf.model.performance.PerformanceTable;
import chrome.umf.model.stage.StageRecord;
import chrome.umf.utils.DateOperations;
import chrome.umf.utils.EStarPerformanceRate;
import chrome.umf.utils.IConstants;
import chrome.umf.utils.StarUpdater;

import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;


import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ArtistDetailActivity extends Activity implements IDebug, OnClickListener 
{

	Debugger debugger;

	private ImageView 	imgArtist;
	private ImageView 	imgCountry;
	private ImageView 	imgStar;
	private ImageView 	imgArtistName;
	private TextView 	txtCountry;
	private TextView 	txtGenre;
	private TextView 	txtDescription;
	private Context 	context;
	private TextView 	txtTime;
	private TextView 	txtDayName;
	private TextView 	txtStage;

	ArtistRecord artistRecord;
	Database database;
	private ImageButton imgButtonMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_artist_detail);
		context 		= 	this;
		debugger 		= 	new Debugger(this);

		Intent intent 	= 	getIntent();
		int artistId 	= 	intent.getIntExtra("id", IConstants.DEFAULT_ARTIST_ID);
		debugger.logForAll("id", " " + artistId);

		database = new Database();
		database.open();
		artistRecord = (ArtistRecord) database.fetchRowForTable(new ArtistRecord(), artistId);
		database.close();

		findViews();
		
		setViewsValues();
		
		imgButtonMap.setOnClickListener(this);
		imgStar.setOnClickListener(this);
	
	}
	
	public void findViews() 
	{
		imgArtist 		= (ImageView	) findViewById(R.id.imgArtistImageOnArtistDetail);
		imgCountry 		= (ImageView	) findViewById(R.id.imgCountryFlag);
		imgStar 		= (	ImageView	) findViewById(R.id.imgStarOnTopInArtistDetail);
		imgArtistName 	= (	ImageView	) findViewById(R.id.imgArtistName);
		txtCountry 		= (	TextView	) findViewById(R.id.txtCountryName);
		txtGenre 		= (	TextView	) findViewById(R.id.txtGentre);
		txtDescription 	= (	TextView	) findViewById(R.id.txtDesArtistDetail);
		txtTime 		= (	TextView	) findViewById(R.id.txtTime);
		txtDayName 		= (	TextView	) findViewById(R.id.txtDayName);
		txtStage 		= (	TextView	) findViewById(R.id.txtStage);
		imgButtonMap 	= (	ImageButton	) findViewById(R.id.imgButtonMap);
	}


	public void setViewsValues() 
	{
		setImageStarValue();
		setImageArtistValue();
		setImageArtistNameValue();
		setCountryImageAndName();
		
		// txtGenre
		txtGenre.setText(artistRecord.getGenre());

		// txtDescription
		txtDescription.setText(artistRecord.getDescription());

		String query = PerformanceTable.SELECT_BY_ARTIST_ID + artistRecord.getId();
		
		database.open();
		ArrayList<IRecords> aryPerformanceRecords = database.fetchAllRowsByQuery(new PerformanceRecord(), query);
		database.close();
		if(! aryPerformanceRecords.isEmpty())
		{
			PerformanceRecord performanceRecord 	  = (PerformanceRecord) aryPerformanceRecords.get(0);
			setTextViewPerformanceTime(performanceRecord);
	
			setTextViewDay(performanceRecord);
	
			setTextViewStage(performanceRecord);
		}

	}

	public void setTextViewStage(PerformanceRecord performanceRecord) 
	{
		database.open();
		StageRecord stageRecord = (StageRecord) database.fetchRowForTable(new StageRecord(), performanceRecord.getStageId());
		database.close();
		txtStage.setText(stageRecord.getName());
	}

	public void setTextViewDay(PerformanceRecord performanceRecord) 
	{
		String[] strAryStartDateTime = performanceRecord.getStartDateTime()	.trim().split(" ");
		String startDateOnly = strAryStartDateTime[0];
		database.open();
		ArrayList<IRecords> aryDayRecords = database.fetchAllRows(new DayRecord());
		database.close();
		for (int i = 0; i < aryDayRecords.size(); i++) 
		{
			DayRecord dayRecord = (DayRecord) aryDayRecords.get(i);
			String date = dayRecord.getDate();
			if (date.contains(startDateOnly)) 
			{
				txtDayName.setText(dayRecord.getName());
				break;
			}
		}
	}
	

	public void setTextViewPerformanceTime(PerformanceRecord performanceRecord) 
	{
		txtTime.setText(DateOperations.getTimeInHourAndMins
						  (performanceRecord.getStartDateTime(), IConstants.TIME_FORMATE_FOR_PERFORMANCE_TABLE) + " - " +
						DateOperations.getTimeInHourAndMins
						  (performanceRecord.getEndDateTime  (), IConstants.TIME_FORMATE_FOR_PERFORMANCE_TABLE ) );
		
	}

	public void setCountryImageAndName() 
	{
		if (artistRecord.getCountryId() > 0) 
		{
			// imgCountry
			String imageCountry = IConstants.START_WORD_FOR_COUNTRY_NAME_IN_IMAGE + artistRecord.getCountryId();
			imgCountry.setImageResource(context.getResources().
						      			getIdentifier("@drawable/" + imageCountry, null,context.getPackageName()));
			// txtCountry
			database.open();
			LookUpCountryRecord lookUpCountryRecord = new LookUpCountryRecord();
			lookUpCountryRecord = (LookUpCountryRecord) database .fetchRowForTable(new LookUpCountryRecord(),artistRecord.getCountryId());
			database.close();
			
			txtCountry.setText("" + lookUpCountryRecord.getName());
		}
	}

	public void setImageArtistNameValue() 
	{
		InputStream isName = null;
		try 
		{
			isName = this
					.getResources()
					.getAssets()
					.open(IConstants.ASSETS_PATH_FOR_ARTIST_NAME_IMAGE	+ artistRecord.getNameImagePath().toLowerCase()	
							+ IConstants.ASSETS_ETENSION_FOR_ARTIST_NAME_IMAGE);

		}
		catch (IOException e) 
		{
			Log.w("Image Name", " " + artistRecord.getNameImagePath() + e);
		}
		Bitmap imageName = BitmapFactory.decodeStream(isName);
		imgArtistName.setImageBitmap(imageName);
	}

	public void setImageArtistValue() 
	{
		if (artistRecord.getImageSmallPath() != null) 
		{
			InputStream is = null;
			try 
			{
				is = this.getResources().getAssets()
						.open(IConstants.ASSETS_PATH_FOR_ARTIST_SMALL_IMAGE + artistRecord.getImageSmallPath()	.toLowerCase() 
								+ IConstants.ASSETS_EXTENSION_FOR_ARTIST_SMALL_IMAGE);

			} catch (IOException e) 
			{
				Log.w("Image Artist", " " + artistRecord.getNameImagePath() + e);
			}
			
			Bitmap image = BitmapFactory.decodeStream(is);
			imgArtist.setImageBitmap(image);
		}
	}

	public void setImageStarValue() 
	{
		switch (artistRecord.getStarPerformance()) 
		{
			case EMPTY :	
				imgStar.setImageResource(R.drawable.starempty);
				break;
			case HALF :	
				imgStar.setImageResource(R.drawable.starhalf);
				break;
			case FULL :
				imgStar.setImageResource(R.drawable.starfull);
				break;
		}
	}

	
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
			case R.id.imgStarOnTopInArtistDetail :
			{
				new StarUpdater(imgStar, artistRecord).execute();
				break;
			}
			case R.id.imgButtonMap:
			{
				startActivity(new Intent(context, Map.class));
			}
		}
	}
	
	@Override
	public boolean isDebugable() 
	{
		return true;
	}
}