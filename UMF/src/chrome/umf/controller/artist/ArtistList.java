package chrome.umf.controller.artist;

import chrome.umf.activities.R;
import chrome.umf.model.artist.ArtistRecord;
import chrome.umf.utils.IConstants;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;


public class ArtistList extends Activity implements OnItemClickListener, OnClickListener, IDebug, TextWatcher 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_ARTISTLIST;

	private ArtistList 		context 			= null;
	public	ListView	 	listView 			= null;
	private Button 			btnAlphabatical 	= null;
	private Button 			btnRate 			= null;
	private EditText 		edtSearchList 		= null;
	private RelativeLayout 	lytEdtViewSearch 	= null;
	private Debugger 		debugger 			= null;
	public static boolean 	isByRank 			= false;
	
	private ExtendedBaseAdapterClass objExtendedBaseAdapter = null;
	
	/**
	 * @category starting function of on create of List Activity
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		debugger = new Debugger(this);
		debugger.logForInFunction();
		setContentView(R.layout.activity_artist_list);
		context = this;

		objExtendedBaseAdapter = new ExtendedBaseAdapterClass(this,	EArtistShow.ALPHABET, null);
		listView 			= (ListView) 		findViewById(R.id.	listViewArtistName);
		btnAlphabatical 	= (Button) 			findViewById(R.id.	btnAlphabatical);
		btnRate 			= (Button) 			findViewById(R.id.	btnRate);
		edtSearchList 		= (EditText) 		findViewById(R.id.	edtSearchList);
		lytEdtViewSearch 	= (RelativeLayout) 	findViewById(R.id.	lytEdtViewSearch);

		listView.setAdapter(objExtendedBaseAdapter);
		listView.setOnItemClickListener(this);
		btnAlphabatical.setOnClickListener(this);
		btnRate.setOnClickListener(this);
		
		//Set by default Sort by alphabet
		btnAlphabatical.performClick();
		
		edtSearchList.addTextChangedListener(this);
		debugger.logForOutFunction();
	}// END onCreate

	/**
	 * @category Listener for click on any item of List
	 */
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3)
	{
		debugger.logForInFunction();
		Intent intent = new Intent(context, ArtistDetailActivity.class);
		int artistId;
		
		switch (objExtendedBaseAdapter.eArtistShow) 
		{
			case ALPHABET		:	artistId = ((ArtistRecord) ExtendedBaseAdapterClass.arryFromStoreRecord.get(position)).getId();
									break;
			case RANK			:	artistId = ((ArtistRecord) ExtendedBaseAdapterClass.arryFromStoreRecord
						                      	.get(ExtendedBaseAdapterClass.hashMapForRank.get(position))).getId();
									break;
			case TEXT_CHANGE	:	artistId = ((ArtistRecord) ExtendedBaseAdapterClass.arryFromStoreRecord
												.get(ExtendedBaseAdapterClass.hashMapForChangeText.get(position))).getId();
									break;
			default				:	artistId	=	IConstants.DEFAULT_ARTIST_ID;
		}

		intent.putExtra("id", artistId);
		startActivity(intent);

	}

	public void onClick(View v) 
	{
		debugger.logForInFunction();
		switch (v.getId()) 
		{
		case R.id.btnAlphabatical:
					// Set visibility off for search alphabalical is on
					lytEdtViewSearch.setVisibility(View.VISIBLE);
					edtSearchList.setText("");
					objExtendedBaseAdapter = new ExtendedBaseAdapterClass(this,	EArtistShow.ALPHABET, null);
					isByRank = false;
					listView.setAdapter(objExtendedBaseAdapter);
					objExtendedBaseAdapter.notifyDataSetChanged();
					btnAlphabatical.setBackgroundResource(R.drawable.artistssortbuttonalpha_on);
					btnRate.setBackgroundResource(R.drawable.artistssortbuttonrank_off);
					break;

		case R.id.btnRate:
					objExtendedBaseAdapter = new ExtendedBaseAdapterClass(this,	EArtistShow.RANK, null);
					lytEdtViewSearch.setVisibility(View.GONE);
					isByRank = true;
					listView.setAdapter(objExtendedBaseAdapter);
					objExtendedBaseAdapter.notifyDataSetChanged();
					btnAlphabatical.setBackgroundResource(R.drawable.artistssortbuttonalpha_off);
					btnRate.setBackgroundResource(R.drawable.artistssortbuttonrank_on);
					listView.setFastScrollEnabled(true);
					break;
		}// end switch
		debugger.logForOutFunction();
	}// END onCLick

	@Override
	protected void onPause() 
	{
		debugger.logForInFunction();
		super.onPause();
	}// END onPause

	@Override
	protected void onPostResume() 
	{
		debugger.logForInFunction();
		super.onPostResume();
	}

	@Override
	protected void onRestart() 
	{
		objExtendedBaseAdapter.setArrayListFromDatabase(true);
		objExtendedBaseAdapter.notifyDataSetChanged();
		debugger.logForInFunction();
		super.onRestart();
	}

	@Override
	protected void onStart() 
	{
		debugger.logForInFunction();
		super.onStart();
	}

	@Override
	protected void onStop() 
	{
		debugger.logForInFunction();
		super.onStop();
	}

	@Override
	protected void onResume() 
	{
		debugger.logForInFunction();
		super.onResume();
	}// END onResume

	public void changeStarPerformance(int position, int drawableId) 
	{
		ImageView imageView;
		View v		= listView.getChildAt(position - listView.getFirstVisiblePosition());
		imageView 	= (ImageView) v.findViewById(R.id.imgStarOnArtistList);
		imageView.setImageResource(drawableId);
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public void onTextChanged(CharSequence chars, int start, int before, int count) 
	{
		objExtendedBaseAdapter = new ExtendedBaseAdapterClass(context, EArtistShow.TEXT_CHANGE, chars);
		listView.setAdapter(objExtendedBaseAdapter);
		objExtendedBaseAdapter.notifyDataSetChanged();
	}// end onTextChanged

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,	int after) 
	{}

	@Override
	public void afterTextChanged(Editable s) 
	{}
}