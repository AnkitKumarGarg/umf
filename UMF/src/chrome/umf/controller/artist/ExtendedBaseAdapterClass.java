package chrome.umf.controller.artist;

import java.util.ArrayList;
import java.util.HashMap;

import chrome.umf.activities.R;
import chrome.umf.model.artist.ArtistRecord;
import chrome.umf.model.artist.ArtistTable;
import chrome.umf.utils.EStarPerformanceRate;
import chrome.umf.utils.IConstants;
import chrome.umf.utils.StarUpdater;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ExtendedBaseAdapterClass extends BaseAdapter implements IDebug 
{
	private static final	boolean 			IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_EXTENDED_BASE_ADAPTER;
	private 				Debugger 			debugger;
	private 				Database 			database;
	public static 			ArrayList<IRecords> arryFromStoreRecord;
	private 				LayoutInflater 		inflater;
	private					ViewHolder 			holder;
	private 				Context 			_context;
	private 				ArtistList 			artistListObj;
	public static 			SparseIntArray		hashMapForChangeText = new SparseIntArray();
	public static 			SparseIntArray 		hashMapForRank = new SparseIntArray();
							EArtistShow 		eArtistShow;

	public ExtendedBaseAdapterClass(ArtistList artistListobj, EArtistShow eArtistShow, CharSequence changedChars) 
	{
		debugger 			= new Debugger(this);
		debugger.logForInFunction();
		_context 			= artistListobj;
		this.artistListObj 	= artistListobj;
		this.eArtistShow 	= eArtistShow;

		setArrayListFromDatabase(false);

		switch (eArtistShow) 
		{
			case ALPHABET		:												break;
			case RANK			:setHashMapToShowForRank();						break;
			case TEXT_CHANGE	:getHashMapToShowForTextChange(changedChars);	break;
		}

		inflater = LayoutInflater.from(_context);
	}

	public int getCount() 
	{
		debugger.logForInFunction();
		debugger.logForAll("Row Count : ", "= " + arryFromStoreRecord.size());
		switch (eArtistShow) 
		{
			case ALPHABET		:	return arryFromStoreRecord.size();
			case RANK			: 	return hashMapForRank.size();
			case TEXT_CHANGE	:	return hashMapForChangeText.size();
			default				:	return arryFromStoreRecord.size();
		}

	}

	public Object getItem(int position) 
	{
		debugger.logForInFunction();

		switch (eArtistShow) 
		{
			case ALPHABET	:	return (ArtistRecord) arryFromStoreRecord.get(position);
			case RANK		:	return (ArtistRecord) arryFromStoreRecord.get(hashMapForRank.get(position));
			case TEXT_CHANGE:	return (ArtistRecord) arryFromStoreRecord.get(hashMapForChangeText.get(position));
			default			:	return (ArtistRecord) arryFromStoreRecord.get(position);
		}
	}

	public long getItemId(int position) 
	{
		debugger.logForInFunction();
		debugger.logForAll("position : ", "= " + position);
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		debugger.logForInFunction();
		if (convertView == null) 
		{
			convertView = inflater.inflate(R.layout.artist_name_and_image_only,	null);
			holder = new ViewHolder();
			holder.imgArtistOnArtistList 		= (ImageView) convertView.findViewById(R.id.imgArtistOnArtistList);
			holder.txtArtistNameOnArtistList 	= (TextView)  convertView.findViewById(R.id.txtArtistNameOnArtistList);
			holder.imgStarOnArtistList 			= (ImageView) convertView.findViewById(R.id.imgStarOnArtistList);
			convertView.setTag(holder);
		}
		else 
		{
			holder = (ViewHolder) convertView.getTag();
		}

		final ArtistRecord artistRecord;
		switch (eArtistShow) 
		{
			case ALPHABET	:	artistRecord = (ArtistRecord) arryFromStoreRecord.get(position);							break;
			case RANK		:	artistRecord = (ArtistRecord) arryFromStoreRecord.get(hashMapForRank.get(position));		break;
			case TEXT_CHANGE:	artistRecord = (ArtistRecord) arryFromStoreRecord.get(hashMapForChangeText.get(position));	break;
			default			:	artistRecord = (ArtistRecord) arryFromStoreRecord.get(position);
		}

		String imageName = artistRecord.getImageSmallPath();
		if (imageName.length() > 0) 
		{
			debugger.logForAll("ImageName", "" + imageName);
			debugger.logForAll("Image No","" + _context.getResources().getIdentifier(imageName + 
					IConstants.ASSETS_EXTENSION_FOR_ARTIST_SMALL_IMAGE, "drawable",_context
					                                                  .getPackageName()));

			holder.imgArtistOnArtistList.setImageResource(_context.
					getResources().getIdentifier("@drawable/" + imageName.toLowerCase(), null,
							                              _context.getPackageName()));
		}
		else
		{
			holder.imgArtistOnArtistList.setImageResource(_context
					.getResources().getIdentifier(
							"@drawable/" + "artistdefault", null,
							_context.getPackageName()));
		}

		if (ArtistList.isByRank) 
		{
			holder.txtArtistNameOnArtistList.setText(""+ artistRecord.getRank() + " " + artistRecord.getName());
		}
		else 
		{
			holder.txtArtistNameOnArtistList.setText(artistRecord.getName());
		}

		switch (artistRecord.getStarPerformance()) 
		{
			case EMPTY	:	holder.imgStarOnArtistList.setImageResource(R.drawable.starempty);	break;
			case HALF	:	holder.imgStarOnArtistList.setImageResource(R.drawable.starhalf);	break;
			case FULL	:	holder.imgStarOnArtistList.setImageResource(R.drawable.starfull);	break;
		}

		holder.imgStarOnArtistList.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				
				new StarUpdater(R.id.imgStarOnArtistList, artistRecord , artistListObj.listView , position).execute();
			}
		});

		debugger.logForOutFunction();
		return convertView;
	}

	static class ViewHolder 
	{
		ImageView imgArtistOnArtistList;
		TextView txtArtistNameOnArtistList;
		ImageView imgStarOnArtistList;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	public void getHashMapToShowForTextChange(CharSequence changedChars) 
	{
		hashMapForChangeText.clear();
		int index = 0;
		for (int i = 0; i < arryFromStoreRecord.size(); i++) 
		{
			ArtistRecord artistRecord = (ArtistRecord) arryFromStoreRecord .get(i);
			if (artistRecord.getName().toLowerCase().contains(changedChars.toString().toLowerCase())) 
			{
				hashMapForChangeText.put(index, i);
				index++;
			}
		}
	}

	public void setHashMapToShowForRank() 
	{
		if (hashMapForRank.size() < 1) 
		{
			int index = 0;
			SparseArray<Integer> hashMapIdVsPosition = new SparseArray<Integer>();

			for (int position = 0; position < arryFromStoreRecord.size(); position++) 
			{
				ArtistRecord artistRecord = (ArtistRecord) arryFromStoreRecord .get(position);
				if (artistRecord.getRank() > 0) 
				{
					hashMapIdVsPosition.put(artistRecord.getId(), position);
					index++;
				}// end if
			}// end for

			ArrayList<IRecords> aryArtistRecordByRank = getArrayListByRank();
			for (int position = 0; position < aryArtistRecordByRank.size(); position++) 
			{
				ArtistRecord artistRecord = (ArtistRecord) aryArtistRecordByRank .get(position);
				if (artistRecord.getRank() > 0) 
				{
					hashMapForRank.put(position,hashMapIdVsPosition.get(artistRecord.getId()));
					index++;
				}// end if
			}// end for
		}
	}

	public void setArrayListFromDatabase(boolean again) 
	{
		if ((arryFromStoreRecord == null)|| again) 
		{
			arryFromStoreRecord =  getArrayListByQuery(ArtistTable.SELECT_QUERY_BY_NAME_ASC);
		} 
	}

	public ArrayList<IRecords> getArrayListByRank() 
	{
		return getArrayListByQuery(ArtistTable.SELECT_QUERY_BY_RANK_ASC);
	}

	private ArrayList<IRecords> getArrayListByQuery(String query) 
	{
		debugger.logForInFunction();
		database = new Database();
		database.open();

		// //Fetch ArrayList for get ListView adapter according to get Query

		ArrayList<IRecords> aryArtistRecords = new ArrayList<IRecords>();
		aryArtistRecords = database.fetchAllRowsByQuery(new ArtistRecord(), query);
		debugger.logForAll("length of array : ", " " + aryArtistRecords.size());
		database.close();
		debugger.logForOutFunction();
		return aryArtistRecords;
	}
}