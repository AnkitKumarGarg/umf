package chrome.umf.controller.lineup;

import java.util.ArrayList;

import chrome.umf.activities.R;
import chrome.umf.controller.planner.EDay;
import chrome.umf.model.day.DayRecord;
import chrome.umf.model.performance.PerformanceRecord;
import chrome.umf.model.stage.StageRecord;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LineUp extends Activity implements OnClickListener 
{
	private GestureDetector gestureDetector;
	RelativeLayout flipView;
	public static  ArrayList<PerformanceRecord> [][] aryOfArrayPerformanceRecords;
	Database database;
	Context context;
	ListView listView; 
	private static Button 		btnDay1;
	private Button 		btnDay2;
	private Button 		btnDay3;
	private TextView 	txtDayName;
	private static TextView 	txtStageName;
	
	ArrayList<IRecords> aryDayRecord;
	static ArrayList<IRecords> aryStageRecord;
	
	ListView liView;
	static LineUpBaseAdapter lineUpBaseAdapter;
	
	static int intStageIndex = 0;
	static int intDayIndex	 = EDay.DAY_1.ordinal();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_line_up);
		database	=	new Database();
	
		fillArray();
		
		flipView			= (RelativeLayout) 	findViewById(R.id.FlipViewInLineUp);
		btnDay1 			= (Button) 			findViewById(R.id.btnLineUpDay1);
		btnDay2 			= (Button) 			findViewById(R.id.btnLineUpDay2);
		btnDay3 			= (Button) 			findViewById(R.id.btnLineUpDay3);
		
		listView			= (ListView) 		findViewById(R.id.listViewLineUp);
		
		lineUpBaseAdapter	=	new LineUpBaseAdapter(this ,aryOfArrayPerformanceRecords[intDayIndex][intStageIndex]);
		listView.setAdapter(lineUpBaseAdapter);
		
		txtDayName 			= (TextView)	 	findViewById(R.id.txtDayNameLineUp);
		txtStageName 		= (TextView) 		findViewById(R.id.txtStageNameLineUp);
		
		
		btnDay1.setOnClickListener(this);
		btnDay2.setOnClickListener(this);
		btnDay3.setOnClickListener(this);
			
		gestureDetector	=	new GestureDetector(new MyGestureDetector());
		context = this;
		
		
		flipView.setOnTouchListener(new OnTouchListener() 
		{
			@Override
			public boolean onTouch(View v, MotionEvent event) 
			{
				if(gestureDetector.onTouchEvent(event))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		});
	}

	@Override
	public void onClick(View v) 
	{
		
		switch (v.getId()) 
		{
		case R.id.btnLineUpDay1: 	txtDayName.setText("" + ((DayRecord)aryDayRecord.get(0)).getName());
									btnDay1.setBackgroundResource(R.drawable.dayfilter_left_on);
									btnDay2.setBackgroundResource(R.drawable.dayfilter_middle_off);
									btnDay3.setBackgroundResource(R.drawable.dayfilter_right_off);
									txtStageName.setText(""+((StageRecord)aryStageRecord.get(intStageIndex)).getName());
									intDayIndex	=	EDay.DAY_1.ordinal();
									lineUpBaseAdapter	=	new LineUpBaseAdapter
											(this ,aryOfArrayPerformanceRecords[intDayIndex][intStageIndex]);
									listView.setAdapter(lineUpBaseAdapter);
									lineUpBaseAdapter.notifyDataSetChanged();
									break;

		case R.id.btnLineUpDay2: 	txtDayName.setText("" + ((DayRecord)aryDayRecord.get(1)).getName());
									btnDay1.setBackgroundResource(R.drawable.dayfilter_left_off);
									btnDay2.setBackgroundResource(R.drawable.dayfilter_middle_on);
									btnDay3.setBackgroundResource(R.drawable.dayfilter_right_off);
									
									intDayIndex	=	EDay.DAY_2.ordinal();
									lineUpBaseAdapter	=	new LineUpBaseAdapter
											(this ,aryOfArrayPerformanceRecords[intDayIndex][intStageIndex]);
									listView.setAdapter(lineUpBaseAdapter);
									lineUpBaseAdapter.notifyDataSetChanged();
									txtStageName.setText(""+aryStageRecord.get(intStageIndex));
									break;

		case R.id.btnLineUpDay3:	txtDayName.setText("" + ((DayRecord)aryDayRecord.get(2)).getName());
									btnDay1.setBackgroundResource(R.drawable.dayfilter_left_off);
									btnDay2.setBackgroundResource(R.drawable.dayfilter_middle_off);
									btnDay3.setBackgroundResource(R.drawable.dayfilter_right_on);

									intDayIndex	=	EDay.DAY_3.ordinal();
									lineUpBaseAdapter	=	new LineUpBaseAdapter
											(this ,aryOfArrayPerformanceRecords[intDayIndex][intStageIndex]);
									listView.setAdapter(lineUpBaseAdapter);
									lineUpBaseAdapter.notifyDataSetChanged();
									txtStageName.setText(""+aryStageRecord.get(intStageIndex));
									break;
		}
	}
	
	public static void changeViewByFlip(boolean isLeft)
	{
		if(isLeft)
		{
			if(LineUp.intStageIndex > 1)
        	{
				LineUp.intStageIndex --;
        		intStageIndex --;
        		btnDay1.performClick();
        	}
		}
		else
		{
			if(LineUp.intStageIndex < 5)
        	{
        		//txtStageName.setText(((StageRecord)LineUp.aryStageRecord.get(LineUp.intStageIndex)).getName());
				txtStageName.setText(""+ intStageIndex);
        		LineUp.intStageIndex ++;
        		intStageIndex ++;
        		btnDay1.performClick();
        	}
		}

		
	}

	public void fillArray()
	{
		database.open();
		aryDayRecord			=	database.fetchAllRows(new DayRecord());
		aryStageRecord			=	database.fetchAllRows(new StageRecord());
		
		int NO_OF_DAYS	=	3;
		int NO_OF_STAGE	=	aryStageRecord.size();
		//aryOfArrayPerformanceRecords	=	new PerformanceRecord[NO_OF_DAYS][NO_OF_STAGE];
		aryOfArrayPerformanceRecords	=	new ArrayList[NO_OF_DAYS][NO_OF_STAGE] ;
		
		ArrayList<IRecords> aryPerformanceRecord			=	database.fetchAllRows(new PerformanceRecord());
		database.close();
		
		for (int dayIndex= 0 ;dayIndex < aryDayRecord.size() ; dayIndex++)
		{
			for (int stageIndex= 0 ;stageIndex < aryStageRecord.size() ; stageIndex++)
			{
				aryOfArrayPerformanceRecords[dayIndex][stageIndex] = new ArrayList<PerformanceRecord>();
			}
		}
		
		PerformanceRecord performanceRecord;
		StageRecord stageRecord;
		DayRecord dayRecord;
		for (int performanceIndex = 0 ; performanceIndex< aryPerformanceRecord.size() ; performanceIndex++)
		{
			performanceRecord = (PerformanceRecord ) aryPerformanceRecord.get(performanceIndex);
			for (int dayIndex= 0 ;dayIndex < aryDayRecord.size() ; dayIndex++)
			{
				dayRecord	=	(DayRecord ) aryDayRecord.get(dayIndex);
				for (int stageIndex= 0 ;stageIndex < aryStageRecord.size() ; stageIndex++)
				{
					stageRecord	=	(StageRecord ) aryStageRecord.get(stageIndex);
					if((performanceRecord.getDayId() == dayRecord.getId())  && (performanceRecord.getStageId() == stageRecord.getId()))
					{
						aryOfArrayPerformanceRecords[dayIndex][stageIndex].add(performanceRecord);
					}
				}
			}
		}
	}
}