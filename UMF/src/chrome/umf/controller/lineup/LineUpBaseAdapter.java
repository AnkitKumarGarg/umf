package chrome.umf.controller.lineup;

import java.util.ArrayList;
import java.util.Date;

import chrome.umf.activities.R;
import chrome.umf.controller.planner.DateOperations;
import chrome.umf.controller.planner.EDay;
import chrome.umf.model.artist.ArtistRecord;
import chrome.umf.model.day.DayRecord;
import chrome.umf.model.performance.PerformanceRecord;
import chrome.umf.model.stage.StageRecord;
import chrome.umf.utils.EStarPerformanceRate;
import chrome.umf.utils.StarUpdater;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;


import android.content.Context;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LineUpBaseAdapter extends BaseAdapter implements IDebug
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_EXTENDED_BASE_ADAPTER;
	
	private Debugger 		debugger;
	private Database 		database;
	private LayoutInflater 	inflater;
	private ViewHolder 		holder;
	private Context 		_context;
	private LineUp 			lineUpObj;
	static 	EDay 			eDayNo;
	private ArrayList<PerformanceRecord> aryPerformanceRecords;
	
	public static Date[][] dateArrayForGap	;
	
	public static boolean isScheduled = false;

	public LineUpBaseAdapter(LineUp lineUpObj, ArrayList<PerformanceRecord> aryPerformanceRecords) 
	{

		debugger = new Debugger(this);
		debugger.logForInFunction();
		_context = lineUpObj;
		database = new Database();
		this.lineUpObj = lineUpObj;
		this.eDayNo = eDayNo;
		this.aryPerformanceRecords = aryPerformanceRecords;
		inflater = LayoutInflater.from(_context);
	}// END ExtendedBaseAdapterClass( , , )

	public int getCount() 
	{
			return aryPerformanceRecords.size();
		

	}// END getCount()

	public Object getItem(int position) 
	{
		debugger.logForInFunction();
		return aryPerformanceRecords.get(position);
		
	}// END getItem()

	public long getItemId(int position) 
	{
		debugger.logForInFunction();
		debugger.logForAll("position : ", "= " + position);
		return position;
	}// END getItemId()

	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		debugger.logForInFunction();
		
		
		if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.line_up_list,null);
				holder = new ViewHolder();
				holder.imgStar 		= (ImageView)	convertView.findViewById(R.id.imgStarOnLineUpList);
				holder.txtArtistName= (TextView)	convertView.findViewById(R.id.txtArtistNameLineUpList);
				holder.txtTime 		= (TextView) 	convertView.findViewById(R.id.txtScheduleTimeLineUpList);
				convertView.setTag(holder);
		}
		
		else
		{
			convertView.setTag(holder);
		}
		final PerformanceRecord performanceRecord =  (PerformanceRecord)aryPerformanceRecords.get(position); 
		
		holder.txtArtistName.setText("" + performanceRecord.getName());
		if(performanceRecord.getStartDateTime().length() > 0 && performanceRecord.getEndDateTime().length() > 0)
		{
			holder.txtTime.setText(DateOperations.getTimeInHourAndMins(performanceRecord.getStartDateTime()) + "- " + 
					DateOperations.getTimeInHourAndMins(performanceRecord.getEndDateTime()));
		}
		
		
		int artistId	=	performanceRecord.getArtistId();
		
		database.open();
		final ArtistRecord artistRecord	=	(ArtistRecord)database.fetchRowForTable(new ArtistRecord(), artistId);
		database.close();
		
		
		
		switch (artistRecord.getStarPerformance()) 
		{
		case EMPTY:
			holder.imgStar.setImageResource(R.drawable.starempty);
			break;
		case HALF:
			holder.imgStar.setImageResource(R.drawable.starhalf);
			break;
		case FULL:
			holder.imgStar.setImageResource(R.drawable.starfull);
			break;
		}

		holder.imgStar.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				
				new StarUpdater(R.id.imgStarOnPlannerListPort, artistRecord , lineUpObj.listView , position).execute();
			}
		});
		
		
		debugger.logForOutFunction();
		return convertView;
	}// end getView

	static class ViewHolder 
	{
		TextView txtTime;
		TextView txtArtistName;
		ImageView imgStar;
	}// END ViewHolder

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}