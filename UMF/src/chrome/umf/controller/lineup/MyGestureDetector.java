package chrome.umf.controller.lineup;

import chrome.umf.model.stage.StageRecord;
import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public class MyGestureDetector extends SimpleOnGestureListener
{
	private static final int SWIPE_MIN_DISTANCE = 20;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 100;
   

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,	float velocityY) 
	{
		
		// right to left swipe
        if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) 
        {
        	LineUp.changeViewByFlip(true);
        	Log.e("Swip left"," true");
        }
        // right to left swipe
        else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) 
        {
        	LineUp.changeViewByFlip(false);
        	Log.e("Swip Right"," true");

        }
		
		return super.onFling(e1, e2, velocityX, velocityY);
	}

	@Override
	public boolean onDown(MotionEvent e) 
	{
		// TODO Auto-generated method stub
		return true;
	}
	
}
