package chrome.umf.controller.main;
////Main
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;
import chrome.utils.database.Database;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.umf.activities.R;
import chrome.umf.utils.ETables;
import chrome.umf.utils.DatabaseUpdater;

public class Main extends Activity implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IS_DEBUGABLE_FLAG_SPLASH_SCREEN;
	private Debugger debugger;
	private ProgressBar progressBarOnSplash;
	Context context = null;
	DatabaseUpdater updateXml;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		debugger = new Debugger(this);
		debugger.logForInFunction();
		setContentView(R.layout.activity_splash_screen);

		progressBarOnSplash = (ProgressBar) findViewById(R.id.progressBarOnSplash);
		debugger = new Debugger(this);

		// Set set Database Context by this we update application context in
		// Database Helper
		context = this;
		
		Database.context = context;
		//  For Update Xml from internet and start next activity 
		new DatabaseUpdater(this, progressBarOnSplash, false, ETables.ALL).execute();

	}// END onCreate

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}// END isDebugable
}// END SplashScreen