package chrome.umf.controller.main;

import java.util.Date;

import chrome.umf.activities.R;
import chrome.umf.controller.artist.ArtistList;
import chrome.umf.controller.lineup.LineUp;
import chrome.umf.controller.map.Map;
import chrome.umf.controller.party.PartyActivity;
import chrome.umf.controller.planner.Planner;
import chrome.umf.utils.ETables;
import chrome.umf.utils.DatabaseUpdater;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.umf.utils.IConstants;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MenuActivity extends Activity implements IDebug, OnClickListener, OnTouchListener 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MAIN_ACTIVITY;
	private Debugger 	debugger;
	
	private TextView	txtTimer;
	public  Boolean 	isCounterStart;
	private Thread 		thread;
	private int 		orientation;
	private Date 		dateCurrent;
	private Date 		dateTarget;
	private Button 		btnRefresh;
	
	
	private ImageView	imgMenuIconLineUp;
	private ImageView	imgMenuIconArtist;
	private ImageView	imgMenuIconPlanner;
	private ImageView	imgMenuIconVideos;
	private ImageView	imgMenuIconMap;
	private ImageView	imgMenuIconParties;
	private ImageView	imgMenuIconAbout;
	private ImageView	imgMenuIconFeedback;
	private ImageView	imgMenuIconClassified;
	
		
	private ProgressBar progressBarOnMenuActivity;

	private Context 	context;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);

		debugger = new Debugger(this);
		debugger.logForInFunction();

		context = this;
		
		setViews();
		setDefaultImages();
		setListener();
		
		// Get asset manager for get .ttf from their to change textview style
		AssetManager assetManager = getAssets();
		Typeface typefaceStyleForTextView = Typeface.createFromAsset( assetManager, "lcddot.ttf");
		txtTimer.setTypeface(typefaceStyleForTextView);

		// Start new thread in background for change time in textview
		Runnable counterDown = new CountDownRunner();
		thread = new Thread(counterDown);
		thread.start();
		
		debugger.logForOutFunction();
	}// END onCreate

	private void setListener() 
	{
		imgMenuIconLineUp		.setOnTouchListener(this);
		imgMenuIconArtist 		.setOnTouchListener(this);
		imgMenuIconPlanner 		.setOnTouchListener(this);
		imgMenuIconVideos 		.setOnTouchListener(this);
		imgMenuIconMap 			.setOnTouchListener(this);
		imgMenuIconParties 		.setOnTouchListener(this);
		imgMenuIconAbout 		.setOnTouchListener(this);
		imgMenuIconFeedback 	.setOnTouchListener(this);
		imgMenuIconClassified 	.setOnTouchListener(this);
		
		btnRefresh				.setOnClickListener(this);
	}

	private void setDefaultImages() 
	{
		imgMenuIconLineUp		.setImageResource(R.drawable.menuicon_lineup);
		imgMenuIconAbout		.setImageResource(R.drawable.menuicon_about);
		imgMenuIconArtist		.setImageResource(R.drawable.menuicon_artists);
		imgMenuIconClassified	.setImageResource(R.drawable.menuicon_classifieds);
		imgMenuIconFeedback		.setImageResource(R.drawable.menuicon_feedback);
		imgMenuIconMap			.setImageResource(R.drawable.menuicon_map);
		imgMenuIconParties		.setImageResource(R.drawable.menuicon_parties);
		imgMenuIconPlanner		.setImageResource(R.drawable.menuicon_planner);
		imgMenuIconVideos		.setImageResource(R.drawable.menuicon_videos);
	}

	private void setViews() 
	{
		imgMenuIconLineUp  		  = (ImageView) 	findViewById(R.id.menuIconLineUp);
		imgMenuIconArtist 		  = (ImageView) 	findViewById(R.id.menuIconArtist);
		imgMenuIconPlanner 		  = (ImageView) 	findViewById(R.id.menuIconPlanner);
		imgMenuIconVideos 		  = (ImageView) 	findViewById(R.id.menuIconVideos);
		imgMenuIconMap 			  = (ImageView) 	findViewById(R.id.menuIconMap);
		imgMenuIconParties 		  = (ImageView) 	findViewById(R.id.menuIconParties);
		imgMenuIconAbout 		  = (ImageView) 	findViewById(R.id.menuIconAbout);
		imgMenuIconFeedback 	  = (ImageView) 	findViewById(R.id.menuIconFeedback);
		imgMenuIconClassified 	  = (ImageView) 	findViewById(R.id.menuIconClassified);
		
		txtTimer 				  =	(TextView) 		findViewById(R.id.	txtTimer);
		
		btnRefresh 				  =	(Button) 		findViewById(R.id.	btnRefresh);
		
		progressBarOnMenuActivity = (ProgressBar) 	findViewById(R.id.progressBarOnMenuActivity);
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		debugger.logForInFunction();
		// thread.start();
	}// END onResume

	@Override
	protected void onPause() 
	{
		super.onPause();
		
	}// END onPause
	
	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public void onClick(View v) 
	{
		new DatabaseUpdater(this, progressBarOnMenuActivity, true, ETables.ARTIST).execute();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		
		if(event.getAction() == MotionEvent.ACTION_DOWN)
		{
			switch(v.getId())
			{
			case R.id.menuIconLineUp 	: 	imgMenuIconLineUp.setImageResource(R.drawable.menuicon_lineup_onclick);
											break;
			case R.id.menuIconAbout 	: 	imgMenuIconAbout.setImageResource(R.drawable.menuicon_about_onclick);
											break;
			case R.id.menuIconArtist 	: 	imgMenuIconArtist.setImageResource(R.drawable.menuicon_artists_onclick);
											break;
			case R.id.menuIconClassified: 	imgMenuIconClassified.setImageResource(R.drawable.menuicon_classifieds_onclick);
											break;
			case R.id.menuIconFeedback 	: 	imgMenuIconFeedback.setImageResource(R.drawable.menuicon_feedback_onclick);
											break;
			case R.id.menuIconMap 		: 	imgMenuIconMap.setImageResource(R.drawable.menuicon_map_onclick);
											break;
			case R.id.menuIconParties 	: 	imgMenuIconParties.setImageResource(R.drawable.menuicon_parties_onclick);
											break;
			case R.id.menuIconPlanner 	: 	imgMenuIconPlanner.setImageResource(R.drawable.menuicon_planner_onclick);
											break;
			case R.id.menuIconVideos 	: 	imgMenuIconVideos.setImageResource(R.drawable.menuicon_videos_onclick);
											break;					
			}
			
		}
		else if( event.getAction() == MotionEvent.ACTION_UP)
		{
			switch(v.getId())
			{
			case R.id.menuIconLineUp 	: 	imgMenuIconLineUp.setImageResource(R.drawable.menuicon_lineup);
											startActivity(new Intent(context, LineUp.class));
											break;
			case R.id.menuIconAbout 	: 	imgMenuIconAbout.setImageResource(R.drawable.menuicon_about);
											break;
			case R.id.menuIconArtist 	: 	imgMenuIconArtist.setImageResource(R.drawable.menuicon_artists);
											startActivity(new Intent(context, ArtistList.class));
											break;
			case R.id.menuIconClassified: 	imgMenuIconClassified.setImageResource(R.drawable.menuicon_classifieds);
											break;
			case R.id.menuIconFeedback 	: 	imgMenuIconFeedback.setImageResource(R.drawable.menuicon_feedback);
											break;
			case R.id.menuIconMap 		: 	imgMenuIconMap.setImageResource(R.drawable.menuicon_map);
											startActivity(new Intent(context, Map.class));
											break;
			case R.id.menuIconParties 	: 	imgMenuIconParties.setImageResource(R.drawable.menuicon_parties);
											startActivity(new Intent(context, PartyActivity.class));
											break;
			case R.id.menuIconPlanner 	: 	imgMenuIconPlanner.setImageResource(R.drawable.menuicon_planner);
											startActivity(new Intent(context, Planner.class));
											break;
			case R.id.menuIconVideos 	: 	imgMenuIconVideos.setImageResource(R.drawable.menuicon_videos);
											break;
			}
		}
	
		return true;
	}
	
	class objClassForRun implements Runnable 
	{
		public void run() 
		{
			debugger.logForAll("ObjClassRun", "Start");
			try {
				debugger.logForAll("ObjClassRun", "In try");
				if (isCounterStart) {
					debugger.logForAll("ObjClassRun", "In if");
					dateCurrent = new Date();
					long diff = dateTarget.getTime() - dateCurrent.getTime();

					long diffDays = diff / (24 * 60 * 60 * 1000);
					long remainMiliSecAfterDays = diff - diffDays
							* (24 * 60 * 60 * 1000);

					long diffHours = remainMiliSecAfterDays / (60 * 60 * 1000);
					long remainMiliSecAfterHours = remainMiliSecAfterDays
							- diffHours * (60 * 60 * 1000);

					long diffMinutes = remainMiliSecAfterHours / (60 * 1000);
					long remainMiliSecAfterMins = remainMiliSecAfterHours
							- diffMinutes * (60 * 1000);

					long diffSeconds = remainMiliSecAfterMins / 1000;
					debugger.logForAll("Date", "is" + "Days:" + diffDays
							+ " Hours:" + diffHours + " Mins:" + diffMinutes
							+ " Sec:" + diffSeconds);
					txtTimer.setText("Days:" + diffDays + " Hours:" + diffHours
							+ " Mins:" + diffMinutes + " Sec:" + diffSeconds);
				}// end if
			}// end try
			catch (Exception e) {
				debugger.logForAll(1, "CounterDown", "Exception" + e);
			}
		}// END run
	}// END objectClassForRun

	class CountDownRunner implements Runnable 
	{
		public void run() 
		{
			debugger.logForAll("CounterDown", "Start");
			// Enter target date in format of DD-MM-YYYY-HH-MM-SS
			isCounterStart = true;
			dateTarget = new Date((IConstants.TARGET_YEAR - 1900),
					(IConstants.TARTGET_MONTH - 1), IConstants.TARGET_DATE, 9,
					10, 10);

			while (!Thread.currentThread().isInterrupted()) 
			{
				debugger.logForAll("CounterDown", "In while");
				try 
				{
					objClassForRun objRun = new objClassForRun();
					runOnUiThread(objRun);
					Thread.sleep(1000);
				}
				catch (InterruptedException e) 
				{
					Thread.currentThread().interrupt();
					Log.e("CounterDown", "Exception " + e);
				}
				catch (Exception e) 
				{
					Log.e( "CounterDown", "Exception" + e);
				}
			}
		}
	}

}