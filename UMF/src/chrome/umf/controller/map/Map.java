package chrome.umf.controller.map;

import java.util.ArrayList;
import java.util.List;

import chrome.umf.activities.R;
import chrome.umf.model.places.PlacesRecord;
import chrome.umf.model.stage.StageRecord;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;

public class Map extends MapActivity implements IDebug, OnClickListener 
{
	Context context;
	Debugger debugger;
	MapView mapView;
	private Button btnMapSatelliteView;
	private Button btnMapNormalView;
	public static GeoPoint lastTouchGeoPoint;
	public static int lastZoomLevel;
	List<Overlay> mapOverlays;
	GeoPoint point;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		context 			= this;
		
		mapView 			= (MapView) findViewById(R.id.	mapview);
		btnMapNormalView 	= (Button) 	findViewById(R.id.	btnMapNormalView);
		btnMapSatelliteView = (Button) 	findViewById(R.id.	btnMapSatelliteView);
		
		point = new GeoPoint((int) (29.52 * 1E6), (int) (77.22 * 1E6));

		btnMapNormalView.setOnClickListener(this);
		btnMapSatelliteView.setOnClickListener(this);

		mapView.setBuiltInZoomControls(true);
		mapView.setSatellite(false);

		mapOverlays = mapView.getOverlays();

		setPlacesIntoMap();
		setStageIntoMap();

		final MapDataObject data = (MapDataObject) getLastNonConfigurationInstance();
		if (data == null) 
		{

			mapView.getController().animateTo(point);
			mapView.getController().setZoom(18);
		}
		else 
		{
			mapView.getController().animateTo(data.getGeoPoint());
			mapView.getController().setZoom(data.getZoomLevel());
		}
	}

	public void setPlacesIntoMap() 
	{
		Database database = new Database();
		database.open();
		ArrayList<IRecords> aryPlacesRecords = database .fetchAllRows(new PlacesRecord());

		for (int i = 0; i < aryPlacesRecords.size(); i++) 
		{
			PlacesRecord placesRecord = (PlacesRecord) aryPlacesRecords.get(i);
			int drawableId = context.getResources().getIdentifier( "@drawable/" + placesRecord.getPlaceImageUrl().toLowerCase(),
					null, context.getPackageName());

			Drawable drawable;

			if (drawableId > 0) 
			{
				drawable = this.getResources().getDrawable(drawableId);
			}
			else 
			{
				drawable = this.getResources().getDrawable( R.drawable.placedrinks);
			}
			MapItemizedOverlay itemizedoverlay = new MapItemizedOverlay( drawable, this);

			double DLati = Double.parseDouble(placesRecord.getLatitude());
			double DLong = Double.parseDouble(placesRecord.getLongitude());
			point = new GeoPoint((int) (DLati * 1e6), (int) (DLong * 1e6));
			String text = placesRecord.getName();
			OverlayItem overlayitem = new OverlayItem(point, text, " ");
			itemizedoverlay.addOverlay(overlayitem);
			mapOverlays.add(itemizedoverlay);
			database.close();
		}
	}

	public void setStageIntoMap() 
	{
		Database database = new Database();
		database.open();
		ArrayList<IRecords> aryStageRecords = database .fetchAllRows(new StageRecord());

		for (int i = 0; i < aryStageRecords.size(); i++) 
		{
			StageRecord stageRecord = (StageRecord) aryStageRecords.get(i);
			int drawableId = context.getResources().getIdentifier( "@drawable/" + stageRecord.getMapImageUrl().toLowerCase(),
					null, context.getPackageName());

			Drawable drawable;

			if (drawableId > 0) 
			{
				drawable = this.getResources().getDrawable(drawableId);
			}
			else 
			{
				drawable = this.getResources().getDrawable(R.drawable.placedrinks);
			}
			MapItemizedOverlay itemizedoverlay = new MapItemizedOverlay(drawable, this);

			double lLati = Double.parseDouble(stageRecord.getLatitude());
			double lLong = Double.parseDouble(stageRecord.getLongitude());
			point = new GeoPoint((int) (lLati * 1e6), (int) (lLong * 1e6));
			String text = stageRecord.getName();
			OverlayItem overlayitem = new OverlayItem(point, text, " ");
			itemizedoverlay.addOverlay(overlayitem);
			mapOverlays.add(itemizedoverlay);
			database.close();
		}
	}

	@Override
	protected boolean isRouteDisplayed() 
	{
		// debugger.logForInFunction();
		return false;
	}

	@Override
	public Object onRetainNonConfigurationInstance() 
	{
		final MapDataObject data = collectMyLoadedData();
		return data;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) 
		{
			Toast.makeText(this, "lastscape", Toast.LENGTH_SHORT).show();
		}
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) 
		{
			Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
		}
	}

	private MapDataObject collectMyLoadedData() 
	{
		MapDataObject myDataObject = new MapDataObject();

		myDataObject.setGeoPoint(lastTouchGeoPoint);
		myDataObject.setZoomLevel(lastZoomLevel);
		return myDataObject;
	}

	@Override
	public boolean isDebugable() 
	{
		// debugger.logForInFunction();
		return true;
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			case R.id.btnMapNormalView:
				mapView.setSatellite(false);
				btnMapNormalView.setBackgroundResource(R.drawable.dayfilter_left_on);
				btnMapSatelliteView.setBackgroundResource(R.drawable.dayfilter_right_off);
				break;
	
			case R.id.btnMapSatelliteView:
				mapView.setSatellite(true);
				btnMapNormalView.setBackgroundResource(R.drawable.dayfilter_left_off);
				btnMapSatelliteView.setBackgroundResource(R.drawable.dayfilter_right_on);
				break;
		}
	}
}