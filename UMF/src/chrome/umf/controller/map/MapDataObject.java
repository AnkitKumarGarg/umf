package chrome.umf.controller.map;

import com.google.android.maps.GeoPoint;

public class MapDataObject 
{
	private GeoPoint geoPoint;
	private int zoomLevel;

	public GeoPoint getGeoPoint() 
	{
		return geoPoint;
	}

	public void setGeoPoint(GeoPoint geoPoint) 
	{
		this.geoPoint = geoPoint;
	}

	public int getZoomLevel() 
	{
		return zoomLevel;
	}

	public void setZoomLevel(int zoomLevel) 
	{
		this.zoomLevel = zoomLevel;
	}
}
