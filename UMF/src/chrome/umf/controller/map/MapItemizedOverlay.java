package chrome.umf.controller.map;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class MapItemizedOverlay extends ItemizedOverlay<OverlayItem> implements IDebug 
{
	Context mContext;
	long lastTouchTime = -1;
	Debugger debugger = new Debugger(this);
	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();

	public MapItemizedOverlay(Drawable defaultMarker) 
	{
		super(boundCenterBottom(defaultMarker));
		debugger.logForInFunction();
	}

	@Override
	protected OverlayItem createItem(int i) 
	{
		return mOverlays.get(i);
	}

	@Override
	public int size() 
	{
		debugger.logForInFunction();
		return mOverlays.size();
	}

	public MapItemizedOverlay(Drawable defaultMarker, Context context) 
	{
		super(boundCenterBottom(defaultMarker));
		debugger.logForInFunction();
		mContext = context;
	}

	public void addOverlay(OverlayItem overlay) 
	{
		debugger.logForInFunction();
		mOverlays.add(overlay);
		populate();
	}

	@Override
	protected boolean onTap(int index) 
	{
		debugger.logForInFunction();
		OverlayItem item = mOverlays.get(index);
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.show();
		return true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) 
	{
		debugger.logForInFunction();
		if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
		{
			long thisTime = System.currentTimeMillis();

			if (thisTime - lastTouchTime < ViewConfiguration.getDoubleTapTimeout()) 
			{
				GeoPoint point = mapView.getProjection().fromPixels((int) motionEvent.getX(), (int) motionEvent.getY());
				mapView.getController().animateTo(point);
				mapView.getController().zoomIn();
				lastTouchTime = -1;
			} 
			else 
			{
				// Too slow :)
				lastTouchTime = thisTime;
			}
		}
		if (motionEvent.getAction() == MotionEvent.BUTTON_SECONDARY) 
		{
			// mapView.getController().zoomOut();
		}
		Map.lastTouchGeoPoint = mapView.getProjection().fromPixels( (int) motionEvent.getX(), (int) motionEvent.getY());
		Map.lastZoomLevel = mapView.getZoomLevel();
		
		return false;
	}

	@Override
	public boolean isDebugable() 
	{
		return true;
	}
}