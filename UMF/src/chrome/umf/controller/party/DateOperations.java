package chrome.umf.controller.party;

import java.util.Calendar;
import java.util.Date;

class DateOperations {
	public static String getdateInSpecificString(Date date) 
	{
		String day = null;
		String month = null;
		int intDate;
		int year;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		switch (calendar.get(Calendar.DAY_OF_WEEK)) 
		{
			case Calendar.SUNDAY	:  	day = "Sunday"	;	break;
			case Calendar.MONDAY	:	day = "Monday"	;	break;
			case Calendar.TUESDAY	:	day = "Tuesday"	;	break;
			case Calendar.WEDNESDAY	:	day = "Wednesday";	break;
			case Calendar.THURSDAY	:	day = "Thursday";	break;
			case Calendar.FRIDAY	:	day = "Friday"	;	break;
			case Calendar.SATURDAY	:	day = "Saturday";	break;
		}

		switch (calendar.get(Calendar.MONTH)) 
		{
			case Calendar.JANUARY	:	month = "January"	;	break;
			case Calendar.FEBRUARY	:	month = "February"	;	break;
			case Calendar.MARCH		:	month = "March"		;	break;
			case Calendar.APRIL		:	month = "April"		;	break;
			case Calendar.MAY		:	month = "May"		;	break;
			case Calendar.JUNE		:	month = "June"		;	break;
			case Calendar.JULY		:	month = "July"		;	break;
			case Calendar.AUGUST	:	month = "August"	;	break;
			case Calendar.SEPTEMBER	:	month = "September"	;	break;
			case Calendar.OCTOBER	:	month = "October"	;	break;
			case Calendar.NOVEMBER	:	month = "November"	;	break;
			case Calendar.DECEMBER	:	month = "December"	;	break;
		}


		intDate = calendar.get(Calendar.DATE);
		year = calendar.get(Calendar.YEAR);

		String returnString = "" + day + ", " + month + " " + intDate + " ("+ year + ")";
		return returnString;
	}
	
	public static String getTimeInHourAndMins(String strTime ) 
	{
		return chrome.umf.utils.DateOperations.getTimeInHourAndMins(strTime, "HH:mm");	
	}	
}

