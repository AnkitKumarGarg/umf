package chrome.umf.controller.party;

import java.util.ArrayList;
import java.util.Date;

import chrome.umf.activities.R;
import chrome.umf.model.party.PartyRecord;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;

public class PartyActivity extends Activity 
{
	private PartyListAdapterClass partyListAdapterClass;
	private ListView listView;
	private Context context;
	private TextView txtPartyDateAndTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_party);
		listView 			= (ListView) findViewById(R.id.ListViewparty);
		txtPartyDateAndTime = (TextView) findViewById(R.id.txtPartyDateAndTime);
		context 			= this;
		Database database 	= new Database();
		database.open();
		ArrayList<IRecords> aryIRecords = database.fetchAllRows(new PartyRecord());
		database.close();
		partyListAdapterClass = new PartyListAdapterClass(this, aryIRecords);
		listView.setAdapter(partyListAdapterClass);

	}

	public void setDateOnBar(Date date) 
	{
		txtPartyDateAndTime.setText(DateOperations.getdateInSpecificString(date));
	}
}
