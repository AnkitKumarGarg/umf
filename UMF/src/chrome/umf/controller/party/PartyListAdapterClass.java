package chrome.umf.controller.party;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import chrome.umf.activities.R;
import chrome.umf.model.party.PartyRecord;
import chrome.utils.database.IRecords;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PartyListAdapterClass extends BaseAdapter implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_EXTENDED_BASE_ADAPTER;
	private Debugger debugger;

	private static ArrayList<IRecords> arryFromStoreRecord;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private Context _context;
	private PartyActivity objPartyActivity;
	private Activity activity;

	private static java.util.Date previousDate = new Date(0, 0, 0);

	public PartyListAdapterClass(PartyActivity context, ArrayList<IRecords> aryIRecords) 
	{
		debugger 			= new Debugger(this);
		debugger.logForInFunction();
		_context		 	= context;
		objPartyActivity 	= context;
		activity 			= context;
		arryFromStoreRecord = aryIRecords;
		inflater 			= LayoutInflater.from(context);
	}// END ExtendedBaseAdapterClass( , , )

	public int getCount() 
	{
		debugger.logForInFunction();
		debugger.logForAll("Row Count : ", "= " + arryFromStoreRecord.size());
		return arryFromStoreRecord.size();
	}// END getCount()

	public Object getItem(int position) 
	{
		debugger.logForInFunction();
		return arryFromStoreRecord.get(position);
	}// END getItem()

	public long getItemId(int position) 
	{
		debugger.logForInFunction();
		debugger.logForAll("position : ", "= " + position);
		return position;
	}// END getItemId()

	public View getView(final int position, View convertView, ViewGroup parent) 
	{

		debugger.logForInFunction();
		if (convertView == null) 
		{
			convertView = inflater.inflate(R.layout.party_list_view, null);
			holder = new ViewHolder();
			holder.txtName 			= (TextView) convertView.findViewById(R.id.txtNamePartyList);
			holder.txtVenue 		= (TextView) convertView.findViewById(R.id.txtVenueNamePartyList);
			holder.txtPartyTime 	= (TextView) convertView.findViewById(R.id.txtTimePartyList);
			holder.txtDescription 	= (TextView) convertView.findViewById(R.id.txtDescriptionPartyList);
			convertView.setTag(holder);
		}// end if
		else {
			holder = (ViewHolder) convertView.getTag();
		}// end else

		PartyRecord partyRecord = (PartyRecord) arryFromStoreRecord.get(position);
		holder.txtName.setText(partyRecord.getName());
		holder.txtVenue.setText(partyRecord.getVenueName());

		if(partyRecord.getStartTime()!=null && partyRecord.getEndTime()!=null)
		{
			holder.txtPartyTime.setText(DateOperations.getTimeInHourAndMins(partyRecord.getStartTime())+ " - "
										+ DateOperations.getTimeInHourAndMins(partyRecord.getEndTime()));
		}
		else
		{
			holder.txtPartyTime.setText(" ");
		}
		

		// set date
		String datestr = "";
		try 
		{
			datestr = partyRecord.getStartDate().trim();
			datestr = datestr.replaceAll("-", "/");
			java.util.Date date;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			date = dateFormat.parse(datestr);

			if ((date.getDate() != previousDate.getDate()) 
					|| (date.getMonth() != previousDate.getMonth())
					|| (date.getYear() != previousDate.getYear()))

			{
				previousDate = date;
				objPartyActivity.setDateOnBar(date);
			}
		} 
		catch (Exception e) 
		{
			debugger.logForAll(1, "Error date" + datestr, "" + e.getMessage());
		}

		holder.txtDescription.setText(partyRecord.getDescription());

		if (partyRecord.getIsHighlighted().contains("1")) 
		{
			int myDynamicColor = Color.parseColor("#E7F3FF");
			// LinearLayout lyt = (LinearLayout)
			// activity.findViewById(R.id.lytPartList);

			convertView.setBackgroundColor(myDynamicColor);
			// lyt.setBackgroundColor(myDynamicColor);
			debugger.logForAll(1, "Color", " " + partyRecord.getIsHighlighted());
		} 
		else 
		{
			int myDynamicColor = Color.parseColor("#FFFFFF");
			convertView.setBackgroundColor(myDynamicColor);
		}

		debugger.logForOutFunction();
		return convertView;
	}// end getView

	static class ViewHolder 
	{
		TextView txtName;
		TextView txtVenue;
		TextView txtPartyTime;
		TextView txtDescription;
	}// END ViewHolder

	@Override
	public boolean isDebugable() 
	{
		return true;
	}
}