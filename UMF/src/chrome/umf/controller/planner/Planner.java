package chrome.umf.controller.planner;

import java.util.ArrayList;

import chrome.umf.activities.R;
import chrome.umf.controller.artist.ArtistDetailActivity;
import chrome.umf.model.day.DayRecord;
import chrome.umf.model.performance.PerformanceRecord;
import chrome.umf.utils.IConstants;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class Planner extends Activity implements OnClickListener, OnItemClickListener 
{

	Context context;
	ListView 	listView;
	private Button 		btnDay1;
	private Button 		btnDay2;
	private Button 		btnDay3;
	private TextView 	txtDayName;
	private TextView 	txtDateName;
	private TextView 	txtCustomTitleBar;
	private RelativeLayout	relativeLayoutBottomButtons;


	PlannerBaseAdapterPortrait plannerBaseAdapterPortrait;
	Database database;

	String[] strDayName = new String[3];
	String[] strDateName = new String[3];

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_planner);
		context = this;
		database = new Database();
		Database.context = context;
		
		listView 			= (ListView) 	findViewById(R.id.listViewPortraitPlanner);
		btnDay1 			= (Button) 		findViewById(R.id.btnPlannerDay1);
		btnDay2 			= (Button) 		findViewById(R.id.btnPlannerDay2);
		btnDay3 			= (Button) 		findViewById(R.id.btnPlannerDay3);
		txtCustomTitleBar 	= (TextView) 	findViewById(IConstants.CUSTOM_TITLE_BAR_ID);

		txtDayName 			= (TextView) 	findViewById(R.id.txtDayPlanner);
		txtDateName 		= (TextView) 	findViewById(R.id.txtDatePlanner);
		
		relativeLayoutBottomButtons	=	(RelativeLayout) findViewById(R.id.bottomRelativeLyt);

		btnDay1.setOnClickListener(this);
		btnDay2.setOnClickListener(this);
		btnDay3.setOnClickListener(this);
		
		plannerBaseAdapterPortrait = new PlannerBaseAdapterPortrait(this,EDay.DAY_1, null);
		listView.setAdapter(plannerBaseAdapterPortrait);
		listView.setOnItemClickListener(this);

	}
	
	@Override
	protected void onStart() 
	{
		super.onStart();
		database.open();
		ArrayList<IRecords> aryDayRecords = database.fetchAllRows(new DayRecord());
		database.close();
		for (int i = 0; i < aryDayRecords.size(); i++) 
		{
			DayRecord dayRecord = (DayRecord) aryDayRecords.get(i);
			strDayName[i] = dayRecord.getName();
			strDateName[i] = DateOperations.getdateInSpecificString(dayRecord.getDate());
		}

	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		
		if(PlannerBaseAdapterPortrait.isScheduled)
		{
			plannerBaseAdapterPortrait = new PlannerBaseAdapterPortrait(this,PlannerBaseAdapterPortrait.eDayNo, null);
			
			
			listView.setAdapter(plannerBaseAdapterPortrait);
			plannerBaseAdapterPortrait.notifyDataSetChanged();
			txtCustomTitleBar.setText("Schedule Gap");
			relativeLayoutBottomButtons.setVisibility(View.GONE);
		}
		else
		{
			relativeLayoutBottomButtons.setVisibility(View.VISIBLE);
			btnDay1.performClick();
		}
	}
	

	@Override
	public void onBackPressed() 
	{
		if(PlannerBaseAdapterPortrait.isScheduled)
		{
			switch(PlannerBaseAdapterPortrait.eDayNo)
			{
			case DAY_1: btnDay1.performClick();		break;
			case DAY_2: btnDay2.performClick();		break;
			case DAY_3: btnDay3.performClick();
			}
			PlannerBaseAdapterPortrait.isScheduled = false;
			relativeLayoutBottomButtons.setVisibility(View.VISIBLE);
		}
		else
		{
			super.onBackPressed();
		}
	}

	
	@Override
	protected void onPause() 
	{
		super.onPause();		
	}

	public void changeStarPerformance(int position, int drawableId) 
	{
		ImageView imageView;
		View v = listView.getChildAt(position - listView.getFirstVisiblePosition());
		imageView = (ImageView) v.findViewById(R.id.imgStarOnPlannerListPort);
		imageView.setImageResource(drawableId);
	}

	@Override
	public void onClick(View v) 
	{
		plannerBaseAdapterPortrait.isScheduled = false;
		switch (v.getId()) 
		{
		case R.id.btnPlannerDay1: 
			plannerBaseAdapterPortrait = new PlannerBaseAdapterPortrait(this,EDay.DAY_1, null);
			listView.setAdapter(plannerBaseAdapterPortrait);
			plannerBaseAdapterPortrait.notifyDataSetChanged();
			btnDay1.setBackgroundResource(R.drawable.dayfilter_left_on);
			btnDay2.setBackgroundResource(R.drawable.dayfilter_middle_off);
			btnDay3.setBackgroundResource(R.drawable.dayfilter_right_off);
			txtDateName.setText(strDateName[0]);
			txtDayName.setText(strDayName[0]);
			txtCustomTitleBar.setText("Planner");
			break;

		case R.id.btnPlannerDay2: 
			plannerBaseAdapterPortrait = new PlannerBaseAdapterPortrait(this,EDay.DAY_2, null);
			listView.setAdapter(plannerBaseAdapterPortrait);
			plannerBaseAdapterPortrait.notifyDataSetChanged();
			btnDay1.setBackgroundResource(R.drawable.dayfilter_left_off);
			btnDay2.setBackgroundResource(R.drawable.dayfilter_middle_on);
			btnDay3.setBackgroundResource(R.drawable.dayfilter_right_off);
			txtDateName.setText(strDateName[1]);
			txtDayName.setText(strDayName[1]);
			txtCustomTitleBar.setText("Planner");
			break;

		case R.id.btnPlannerDay3:
			plannerBaseAdapterPortrait = new PlannerBaseAdapterPortrait(this,EDay.DAY_3, null);
			listView.setAdapter(plannerBaseAdapterPortrait);
			plannerBaseAdapterPortrait.notifyDataSetChanged();
			btnDay1.setBackgroundResource(R.drawable.dayfilter_left_off);
			btnDay2.setBackgroundResource(R.drawable.dayfilter_middle_off);
			btnDay3.setBackgroundResource(R.drawable.dayfilter_right_on);
			txtDateName.setText(strDateName[2]);
			txtDayName.setText(strDayName[2]);
			txtCustomTitleBar.setText("Planner");
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) 
	{
		Intent intent = new Intent(context, ArtistDetailActivity.class);
		int artistId;

		if ((PlannerBaseAdapterPortrait.hashMap[PlannerBaseAdapterPortrait.eDayNo.ordinal()].get(position) < 0)) 
		{
			PlannerBaseAdapterPortrait.isScheduled = true;
			
			plannerBaseAdapterPortrait.scheduleStartTime = 
					plannerBaseAdapterPortrait.dateArrayForGap[-1 * 
					                (PlannerBaseAdapterPortrait.hashMap
					                		[PlannerBaseAdapterPortrait.eDayNo.ordinal()].get(position))][0];
			
			plannerBaseAdapterPortrait.scheduleEndTime = 
					plannerBaseAdapterPortrait.dateArrayForGap[-1 * 
					                (PlannerBaseAdapterPortrait.hashMap
					                		[PlannerBaseAdapterPortrait.eDayNo.ordinal()].get(position))][1];
			
			
			plannerBaseAdapterPortrait = new PlannerBaseAdapterPortrait(this,PlannerBaseAdapterPortrait.eDayNo, null);

			listView.setAdapter(plannerBaseAdapterPortrait);
			plannerBaseAdapterPortrait.notifyDataSetChanged();
			txtCustomTitleBar.setText("Schedule Gap");
			relativeLayoutBottomButtons.setVisibility(View.GONE);
		}
		else 
		{
			switch (plannerBaseAdapterPortrait.eDayNo) 
			{
				case DAY_1:		artistId = ((PerformanceRecord) ((PlannerBaseAdapterPortrait.
											aryOfArrayPerformanceRecords.get(0)).get(position))).getArtistId();
								break;
				case DAY_2:		artistId = ((PerformanceRecord) ((PlannerBaseAdapterPortrait.
											aryOfArrayPerformanceRecords.get(1)).get(position))).getArtistId();
								break;
				case DAY_3:		artistId = ((PerformanceRecord) ((PlannerBaseAdapterPortrait.
											aryOfArrayPerformanceRecords.get(2)).get(position))).getArtistId();
								break;
				default:		artistId = IConstants.DEFAULT_ARTIST_ID;
			}

			intent.putExtra("id", artistId);
			startActivity(intent);
		}
	}

	@Override
	protected void onRestart() 
	{
		plannerBaseAdapterPortrait.notifyDataSetChanged();
		super.onRestart();
	}
}