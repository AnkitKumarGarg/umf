package chrome.umf.controller.planner;

import java.util.ArrayList;
import java.util.Date;

import chrome.umf.activities.R;
import chrome.umf.model.artist.ArtistRecord;
import chrome.umf.model.day.DayRecord;
import chrome.umf.model.performance.PerformanceRecord;
import chrome.umf.model.stage.StageRecord;
import chrome.umf.utils.EStarPerformanceRate;
import chrome.umf.utils.StarUpdater;
import chrome.utils.database.Database;
import chrome.utils.database.IRecords;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import android.content.Context;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PlannerBaseAdapterPortrait extends BaseAdapter implements IDebug
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_EXTENDED_BASE_ADAPTER;
	public static ArrayList<ArrayList<IRecords>> aryOfArrayPerformanceRecords;
	public static ArrayList<IRecords> arryFromStoreRecord;
	public static SparseIntArray hashMapForPlannedItem[] = new SparseIntArray[3];
	public static SparseIntArray hashMapForScheduleItem[] = new SparseIntArray[3];
	public static SparseIntArray hashMap[];
	
	static Date scheduleStartTime;
	static Date scheduleEndTime;
	
	private Debugger 		debugger;
	private Database 		database;
	private LayoutInflater 	inflater;
	private ViewHolder 		holder;
	private Context 		_context;
	private Planner 		plannerObj;
	static 	EDay 			eDayNo;
	
	public static Date[][] dateArrayForGap	;
	
	public static boolean isScheduled = false;

	public PlannerBaseAdapterPortrait(Planner plannerObj, EDay eDayNo, CharSequence changedChars) 
	{

		debugger = new Debugger(this);
		debugger.logForInFunction();
		_context = plannerObj;
		database = new Database();
		this.plannerObj = plannerObj;
		this.eDayNo = eDayNo;

		SetData();
		setHashMap();
		
		if(isScheduled)
		{
			hashMap	=	hashMapForScheduleItem;
		}
		else
		{
			hashMap	=	hashMapForPlannedItem;
		}
		
		switch (eDayNo) 
		{
		case DAY_1:arryFromStoreRecord = aryOfArrayPerformanceRecords.get(eDayNo.ordinal());
			break;
		case DAY_2:arryFromStoreRecord = aryOfArrayPerformanceRecords.get(eDayNo.ordinal());
			break;
		case DAY_3:arryFromStoreRecord = aryOfArrayPerformanceRecords.get(eDayNo.ordinal());
		}

		inflater = LayoutInflater.from(_context);
	}// END ExtendedBaseAdapterClass( , , )

	public int getCount() 
	{
		debugger.logForInFunction();
		if(isScheduled)
		{
			hashMap	=	hashMapForScheduleItem;
		}
		else
		{
			hashMap	=	hashMapForPlannedItem;
		}
		return hashMap[eDayNo.ordinal()].size();
	}// END getCount()

	public Object getItem(int position) 
	{
		debugger.logForInFunction();
		return (PerformanceRecord) arryFromStoreRecord.get(position);
		
	}// END getItem()

	public long getItemId(int position) 
	{
		debugger.logForInFunction();
		debugger.logForAll("position : ", "= " + position);
		return position;
	}// END getItemId()

	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		debugger.logForInFunction();
		
		if ((hashMap[eDayNo.ordinal()].get(position) < 0))
		{
			convertView = inflater.inflate(R.layout.planner_list_portrait_blank,null);
			holder = new ViewHolder();
			holder.txtTime	= (TextView ) convertView.findViewById(R.id.txtScheduleTimePlannerListPortBlank);
			String strStart , strEnd;
			try
			{
				strStart ="" + 
			DateOperations.getTimeInHourAndMins(dateArrayForGap[-1 * (hashMap[eDayNo.ordinal()].get(position))][0]);
			}
			
			catch(Exception e)
			{
				strStart = ""; 
			}
			
			try
			{
				strEnd ="" +DateOperations.getTimeInHourAndMins(dateArrayForGap[-1 * (hashMap[eDayNo.ordinal()].get(position))][1]);
			}
			
			catch(Exception e)
			{
				strEnd = ""; 
			}
			holder.txtTime.setText("" + strStart + " - " + strEnd);
			convertView.setTag(holder);
			return convertView;
		}
		
		else if((isScheduled && convertView != null))
		{
			holder = (ViewHolder) convertView.getTag();
		}
		else if ((isScheduled ) || ((! isScheduled))) 
			{
				convertView = inflater.inflate(R.layout.planner_list_portrait,
						null);
				holder = new ViewHolder();
				holder.imgStar 		= (ImageView)	convertView.findViewById(R.id.imgStarOnPlannerListPort);
				holder.txtArtistName= (TextView)	convertView.findViewById(R.id.txtArtistNamePlannerListPort);
				holder.txtStage 	= (TextView) 	convertView.findViewById(R.id.txtStageNamePlannerListPort);
				holder.txtTime 		= (TextView) 	convertView.findViewById(R.id.txtScheduleTimePlannerListPort);
				convertView.setTag(holder);
			}
			
			
			
		final PerformanceRecord performanceRecord = 
				(PerformanceRecord)arryFromStoreRecord.get((hashMap[eDayNo.ordinal()].get(position))); 
		
		holder.txtArtistName.setText("" + performanceRecord.getName());
		if(performanceRecord.getStartDateTime().length() > 0 && performanceRecord.getEndDateTime().length() > 0)
		{
			holder.txtTime.setText(DateOperations.getTimeInHourAndMins(performanceRecord.getStartDateTime()) + "- " + 
					DateOperations.getTimeInHourAndMins(performanceRecord.getEndDateTime()));
		}
		database.open();
		StageRecord stageRecord = (StageRecord)database.fetchRowForTable(new StageRecord(), performanceRecord.getStageId());
		database.close();
				
		holder.txtStage.setText(stageRecord.getName());
		
		
		int artistId	=	performanceRecord.getArtistId();
		
		database.open();
		final ArtistRecord artistRecord	=	(ArtistRecord)database.fetchRowForTable(new ArtistRecord(), artistId);
		database.close();
		
		
		switch (artistRecord.getStarPerformance()) 
		{
		case EMPTY:
			holder.imgStar.setImageResource(R.drawable.starempty);
			break;
		case HALF:
			holder.imgStar.setImageResource(R.drawable.starhalf);
			break;
		case FULL:
			holder.imgStar.setImageResource(R.drawable.starfull);
			break;
		}

		holder.imgStar.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			{
				
				new StarUpdater(R.id.imgStarOnPlannerListPort, artistRecord , plannerObj.listView , position).execute();
			}
		});
		
		
		debugger.logForOutFunction();
		return convertView;
	}// end getView

	static class ViewHolder 
	{
		TextView txtTime;
		TextView txtArtistName;
		TextView txtStage;
		ImageView imgStar;
	}// END ViewHolder

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}


	public void SetData() 
	{
		if (aryOfArrayPerformanceRecords == null) 
			setArrayListOfAllPerformance();
	}

	

	private void setArrayListOfAllPerformance() 
	{
		debugger.logForInFunction();
		database.open();

		 //Fetch ArrayList for get ListView adapter according to get Query
		aryOfArrayPerformanceRecords	=	new ArrayList<ArrayList<IRecords>>();
		ArrayList<IRecords> aryDayRecord	=	database.fetchAllRows(new DayRecord());
		int NO_OF_DAYS	=	3;
		for(int i = 0 ; i< NO_OF_DAYS ; i++)
		{
			DayRecord	dayRecord	=	(DayRecord)aryDayRecord.get(i);
			ArrayList<IRecords> aryPerformanceRecords = new ArrayList<IRecords>();
			aryPerformanceRecords = database.fetchManyRowsByGivenValue(new PerformanceRecord(), dayRecord.getDate());
			aryOfArrayPerformanceRecords.add(aryPerformanceRecords);
		}
		database.close();
		debugger.logForOutFunction();
	}
	
	public void setHashMap()
	{
		
		database.open();
		int 	scheduleKey 	= 	0;
		int 	plannedKey		=	0;
		boolean isFirstRecord 	= false;
		boolean isLastRecord 	= false;
		hashMapForScheduleItem[eDayNo.ordinal()] = new SparseIntArray();
		hashMapForPlannedItem[eDayNo.ordinal()]  = new SparseIntArray();
		
		PerformanceRecord performanceRecordPrev;
				
		ArrayList<IRecords> aryPerformanceRecords = aryOfArrayPerformanceRecords.get(eDayNo.ordinal());
		
		dateArrayForGap 		=	new Date[aryPerformanceRecords.size()][2];
		int topOfArrayForGap	=	0;
		
		for(int i = 0 ; i < aryPerformanceRecords.size() ; i++)
		{
			if(i == 0)
			{
				isFirstRecord = true;
			}
			else
			{
				isFirstRecord = false;
			}
			
			if(i == (aryPerformanceRecords.size() - 1))
			{
				isLastRecord = true;
			}
			else
			{
				isLastRecord = false;
			}
			PerformanceRecord performanceRecord = 	(PerformanceRecord) aryPerformanceRecords.get(i);
			ArtistRecord artistRecord 			=	(ArtistRecord)database.fetchRowForTable(new ArtistRecord(), 
					performanceRecord.getArtistId()); 
			
			//Algo for divide planned and scheduled performance
			//1.If performance is star less
			if(artistRecord.getStarPerformance() == EStarPerformanceRate.EMPTY)
			{
				if(scheduleStartTime != null && scheduleEndTime != null)
				{
					if
					(
						scheduleEndTime.getHours() < 
						DateOperations.getDateObject(performanceRecord.getStartDateTime()).getHours()
						|| 
						(
							scheduleEndTime.getHours() == 
							DateOperations.getDateObject(performanceRecord.getStartDateTime()).getHours() 
							&& scheduleEndTime.getMinutes() <= 
							DateOperations.getDateObject(performanceRecord.getStartDateTime()).getMinutes()
						)
						||
						scheduleStartTime.getHours() > 
						DateOperations.getDateObject(performanceRecord.getEndDateTime()).getHours()
						||
						(
								scheduleStartTime.getHours() == 
								DateOperations.getDateObject(performanceRecord.getEndDateTime()).getHours() 
								&& scheduleStartTime.getMinutes() >= 
								DateOperations.getDateObject(performanceRecord.getEndDateTime()).getMinutes()
						)
					)
					{}
					else
					{
						hashMapForScheduleItem[eDayNo.ordinal()].append(scheduleKey, i);
						scheduleKey ++;
						
					}
				
				}
				//1.1 If it is find and last record in list
				if(isFirstRecord && isLastRecord)
				{
					
					dateArrayForGap[++ topOfArrayForGap][0] = 
							DateOperations.getDateObject(performanceRecord.getStartDateTime());
					
					dateArrayForGap[topOfArrayForGap][1] = 
							DateOperations.getDateObject(performanceRecord.getEndDateTime());
					
					hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, (-1)* topOfArrayForGap);
					plannedKey ++;
				}
				
				//1.2 For first starless record	
				else if(isFirstRecord)
					{
						dateArrayForGap[++ topOfArrayForGap][0] = 
								DateOperations.getDateObject(performanceRecord.getStartDateTime());
						
						dateArrayForGap[topOfArrayForGap][1] = 
								DateOperations.getDateObject(performanceRecord.getEndDateTime());
						
						hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, (-1)* topOfArrayForGap);
						plannedKey ++;
						
						
					}
				
				//1.3 If performance is last and its above also has star less performance
				else if(isLastRecord && ((hashMapForPlannedItem[eDayNo.ordinal()].get(plannedKey -1) < 0)))
					{
						dateArrayForGap[topOfArrayForGap][1] = 
								DateOperations.getDateObject(performanceRecord.getEndDateTime());
						
					} 
				//1.3 If performance is last and its above  has star performance
				else if (isLastRecord && (!(hashMapForPlannedItem[eDayNo.ordinal()].get(plannedKey -1) < 0)))
					{
					
					performanceRecordPrev =(PerformanceRecord)
							aryPerformanceRecords.get(hashMapForPlannedItem[eDayNo.ordinal()].get(plannedKey - 1));
					
					
					
					
					Date preStartTimePlanned = DateOperations.getDateObject(performanceRecordPrev.getStartDateTime());
					
					Date preEndTimePlanned = DateOperations.getDateObject(performanceRecordPrev.getEndDateTime());
					
					Date nowStartTimePlanned = DateOperations.getDateObject(performanceRecord.getStartDateTime());
					
					Date nowEndTimePlanned = DateOperations.getDateObject(performanceRecord.getEndDateTime());
					
					//1.3.1 If previous star performance end time is less then new performance end time
					if((preEndTimePlanned.getHours() < nowEndTimePlanned.getHours()) || 
							((preEndTimePlanned.getHours() == nowEndTimePlanned.getHours()) && 
									(preEndTimePlanned.getMinutes() < nowEndTimePlanned.getMinutes())))
						{
				
							dateArrayForGap[++ topOfArrayForGap][0] = 
									DateOperations.getDateObject(performanceRecordPrev.getEndDateTime());
							dateArrayForGap[topOfArrayForGap][1] = 
									DateOperations.getDateObject(performanceRecord.getEndDateTime());
							
							hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, (-1)* topOfArrayForGap);
							plannedKey ++;
						}
					}
			}
			//2. For Star Performance 
			else
			{
				//2.1 If it is not 1st record in the Planned list
				if(hashMapForPlannedItem[eDayNo.ordinal()].size() > 0)
				{
					//2.1.1 If its previous performance is starless
					if(hashMapForPlannedItem[eDayNo.ordinal()].get(plannedKey -1) < 0)
					{
						
						//2.1.1.1 if it is second record and first starless is meaning less
						if(hashMapForPlannedItem[eDayNo.ordinal()].size() == 1 && 
								((dateArrayForGap[1][0].getHours() == 
										DateOperations.getDateObject(performanceRecord.getStartDateTime()).getHours()) &&
									 dateArrayForGap[1][0].getMinutes() ==
										(DateOperations.getDateObject(performanceRecord.getStartDateTime()).getMinutes())))
						{
							topOfArrayForGap--;
							plannedKey--;
							hashMapForPlannedItem[eDayNo.ordinal()].delete(0);
							hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, i);
							plannedKey ++;
						}
						else
						{
							dateArrayForGap[topOfArrayForGap][1] = 
									DateOperations.getDateObject(performanceRecord.getStartDateTime());
							
							hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, i);
							plannedKey ++;
						}
					}
					//2.1.2 If previous is star performance
					else
					{
					
						performanceRecordPrev =(PerformanceRecord)
								aryPerformanceRecords.get(hashMapForPlannedItem[eDayNo.ordinal()].get(plannedKey - 1));
					
						Date preStartTimePlanned = DateOperations.getDateObject(performanceRecordPrev.getStartDateTime());
						
						Date preEndTimePlanned = DateOperations.getDateObject(performanceRecordPrev.getEndDateTime());
						
						Date nowStartTimePlanned = DateOperations.getDateObject(performanceRecord.getStartDateTime());
						
						Date nowEndTimePlanned = DateOperations.getDateObject(performanceRecord.getEndDateTime());
						
						//2.1.2.1 If previous star performance end time is less then new performance start time
						if((preEndTimePlanned.getHours() < nowStartTimePlanned.getHours()) || 
								((preEndTimePlanned.getHours() == nowStartTimePlanned.getHours()) && 
										(preEndTimePlanned.getMinutes() < nowStartTimePlanned.getMinutes())))
						{
							dateArrayForGap[++ topOfArrayForGap][0] = 
									DateOperations.getDateObject(performanceRecordPrev.getEndDateTime());
							dateArrayForGap[topOfArrayForGap][1] = 
									DateOperations.getDateObject(performanceRecord.getStartDateTime());
							
							hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, (-1)* topOfArrayForGap);
							plannedKey ++;
							
							hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, i);
							plannedKey ++;
						}
						//2.1.2.2 If previous star performance end time is greater then new performance start time
						else
						{
							hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, i);
							plannedKey ++;
						}
						
					}
				}
				//2.2 If it is first record in the list
				else
				{
					hashMapForPlannedItem[eDayNo.ordinal()].append(plannedKey, i);
					plannedKey ++;
				}
			}
		}
		database.close();
	}
}