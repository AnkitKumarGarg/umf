package chrome.umf.model.artist;

import chrome.umf.utils.EStarPerformanceRate;
import chrome.umf.utils.IConstants;
import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class ArtistRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_ARTIST;
	public static final ArtistTable ARTIST_TABLE = new ArtistTable();

	private int autoID;

	private int 	Id;
	private String 	Name;
	private String 	Description;
	private int 	Rank;
	private String 	ImageSmallPath;
	private String 	ImageLargePath;
	private String 	NameImagePath;
	private String 	Genre;
	private String 	URL;
	private int 	CountryId;
	private String 	VideoURL;
	private int 	StarPerformance = 0;

	public ArtistTable getTableObject() 
	{
		return ARTIST_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return Id;
	}

	public void setId(int id) 
	{
		Id = id;
	}

	public String getName() 
	{
		return Name;
	}

	public void setName(String name) 
	{
		Name = name;
	}

	public String getDescription() 
	{
		return Description;
	}

	public void setDescription(String description) 
	{
		Description = description;
	}

	public int getRank() 
	{
		return Rank;
	}

	public void setRank(int rank) 
	{
		Rank = rank;
	}

	public String getImageSmallPath() 
	{
		return ImageSmallPath;
	}

	public void setImageSmallPath(String imageSmallPath) 
	{
		ImageSmallPath = imageSmallPath;
	}

	public String getImageLargePath() 
	{
		return ImageLargePath;
	}

	public void setImageLargePath(String imageLargePath) 
	{
		ImageLargePath = imageLargePath;
	}

	public String getNameImagePath() 
	{
		return NameImagePath;
	}

	public void setNameImagePath(String nameImagePath) 
	{
		NameImagePath = nameImagePath;
	}

	public String getGenre() 
	{
		return Genre;
	}

	public void setGenre(String genre) 
	{
		Genre = genre;
	}

	public String getURL() 
	{
		return URL;
	}

	public void setURL(String uRL) 
	{
		URL = uRL;
	}

	public int getCountryId() 
	{
		return CountryId;
	}

	public void setCountryId(int countryId) 
	{
		CountryId = countryId;
	}

	public String getVideoURL() 
	{
		return VideoURL;
	}

	public void setVideoURL(String videoURL) 
	{
		VideoURL = videoURL;
	}

	public EStarPerformanceRate getStarPerformance() 
	{
		switch(StarPerformance)
		{
			case IConstants.INT_STAR_EMPTY 		: 	return EStarPerformanceRate.EMPTY;
			case IConstants.INT_STAR_HALF		:	return EStarPerformanceRate.HALF;
			default 							:	return EStarPerformanceRate.FULL;			
		}
	}

	public void setStarPerformance(EStarPerformanceRate starPerformance) 
	{
		switch(starPerformance)
		{
			case EMPTY 		: 	StarPerformance = IConstants.INT_STAR_EMPTY ;	break;
			case HALF		:	StarPerformance = IConstants.INT_STAR_HALF ;	break;
			default 		:	StarPerformance = IConstants.INT_STAR_FULL ;	break;			
		}
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}
