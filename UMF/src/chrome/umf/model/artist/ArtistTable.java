package chrome.umf.model.artist;

import java.util.ArrayList;

import chrome.umf.utils.EStarPerformanceRate;
import chrome.umf.utils.IConstants;
import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ArtistTable implements ITable, IDebug 
{
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_ARTIST;

	public static final String TABLE_NAME 			= "tableArtistRecord";
	public static final String COL_AUTO_ID			= "autoId";
	public static final String COL_ARTIST_ID 		= "Id";
	public static final String COL_NAME 			= "Name";
	public static final String COL_DESCRIPTION 		= "Description";
	public static final String COL_RANK 			= "Rank";
	public static final String COL_IMAGE_SMALL_PATH = "ImageSmallPath";
	public static final String COL_IMAGE_LARGE_PATH = "ImageLargePath";
	public static final String COL_NAME_IMAGE_PATH 	= "NameImagePath";
	public static final String COL_GENRE 			= "Genre";
	public static final String COL_URL 				= "URL";
	public static final String COL_COUNTRY_ID 		= "CountryId";
	public static final String COL_VEDIO_URL 		= "VideoURL";
	public static final String COL_STAR_PERFORMANCE = "StarPerform";
	public static ContentValues values = new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME + " ( " 
												+ COL_AUTO_ID 			+ " INTEGER " + " primary key autoincrement,"
												+ COL_ARTIST_ID 		+ " INTEGER , " 
												+ COL_NAME 				+ " text , "
												+ COL_DESCRIPTION 		+ " text , " 
												+ COL_RANK 				+ " INTEGER , "
												+ COL_IMAGE_SMALL_PATH 	+ " text , " 
												+ COL_IMAGE_LARGE_PATH 	+ " text , " 
												+ COL_NAME_IMAGE_PATH 	+ " text , " 
												+ COL_GENRE 			+ " text , " 
												+ COL_URL 				+ " text , " 
												+ COL_COUNTRY_ID 		+ " INTEGER , " 
												+ COL_STAR_PERFORMANCE 	+ " INTEGER , "
												+ COL_VEDIO_URL 		+ " text" + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public final static String SELECT_QUERY_BY_RANK_ASC = "select * from "
			+ ArtistTable.TABLE_NAME + " group by ABS(" + ArtistTable.COL_RANK
			+ ")" + " having " + ArtistTable.COL_RANK + " > 0";

	public final static String SELECT_QUERY_BY_NAME_ASC = "select * from "
			+ ArtistTable.TABLE_NAME + " ORDER BY " + ArtistTable.COL_NAME
			+ " ASC";

	public final static String UPDATE_QUERY_FOR_STAR_PERFORMANCE = "UPDATE"
			+ ArtistTable.TABLE_NAME
			+ "SET COL_STAR_PERFORMANCE = ? where COL_ARTIST_ID = ?";

	public ArtistTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		ArtistRecord objArtistRecord = (ArtistRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(ArtistTable.COL_ARTIST_ID		, objArtistRecord.getId());
		values.put(ArtistTable.COL_NAME				, objArtistRecord.getName());
		values.put(ArtistTable.COL_DESCRIPTION		, objArtistRecord.getDescription());
		values.put(ArtistTable.COL_RANK				, objArtistRecord.getRank());
		values.put(ArtistTable.COL_IMAGE_SMALL_PATH , objArtistRecord.getImageSmallPath());
		values.put(ArtistTable.COL_IMAGE_LARGE_PATH , objArtistRecord.getImageLargePath());
		values.put(ArtistTable.COL_NAME_IMAGE_PATH  , objArtistRecord.getNameImagePath());
		values.put(ArtistTable.COL_GENRE			, objArtistRecord.getGenre());
		values.put(ArtistTable.COL_URL				, objArtistRecord.getURL());
		values.put(ArtistTable.COL_COUNTRY_ID		, objArtistRecord.getCountryId());
		values.put(ArtistTable.COL_STAR_PERFORMANCE	, 0);
		values.put(ArtistTable.COL_VEDIO_URL		, objArtistRecord.getVideoURL());
		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId,SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Id", " " + rowId);
		Cursor cursor = database.rawQuery("select * from " + ArtistTable.TABLE_NAME + " where Id = " + rowId, null);
		cursor.moveToFirst();
		ArtistRecord objArtistRecord = (ArtistRecord) beanClassObject;

		objArtistRecord.setAutoID		 (cursor.getInt 	(cursor.getColumnIndex(ArtistTable.COL_AUTO_ID)));
		objArtistRecord.setId			 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_ARTIST_ID)));
		objArtistRecord.setName			 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_NAME)));
		objArtistRecord.setDescription	 (cursor.getString 	(cursor.getColumnIndex(ArtistTable.COL_DESCRIPTION)));
		objArtistRecord.setRank			 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_RANK)));
		objArtistRecord.setImageSmallPath(cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_IMAGE_SMALL_PATH)));
		objArtistRecord.setImageLargePath(cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_IMAGE_LARGE_PATH)));
		objArtistRecord.setNameImagePath (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_NAME_IMAGE_PATH)));
		objArtistRecord.setGenre		 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_GENRE)));
		objArtistRecord.setURL			 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_URL)));
		objArtistRecord.setCountryId	 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_COUNTRY_ID)));
		
		switch(cursor.getInt(cursor.getColumnIndex(ArtistTable.COL_STAR_PERFORMANCE)))
		{
			case 0 : objArtistRecord.setStarPerformance(EStarPerformanceRate.EMPTY);	break;
			case 50: objArtistRecord.setStarPerformance(EStarPerformanceRate.HALF);		break;
			case 100: objArtistRecord.setStarPerformance(EStarPerformanceRate.FULL);	
		}
		
		objArtistRecord.setVideoURL(cursor.getString(cursor	.getColumnIndex(ArtistTable.COL_VEDIO_URL)));

		debugger.logForAll(	"ArtistName"," " + cursor.getString(cursor .getColumnIndex(ArtistTable.COL_NAME)));

		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objArtistRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from "
				+ ArtistTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			ArtistRecord objArtistRecord = new ArtistRecord();

			objArtistRecord.setAutoID		 (cursor.getInt 	(cursor.getColumnIndex(ArtistTable.COL_AUTO_ID)));
			objArtistRecord.setId			 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_ARTIST_ID)));
			objArtistRecord.setName			 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_NAME)));
			objArtistRecord.setDescription	 (cursor.getString 	(cursor.getColumnIndex(ArtistTable.COL_DESCRIPTION)));
			objArtistRecord.setRank			 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_RANK)));
			objArtistRecord.setImageSmallPath(cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_IMAGE_SMALL_PATH)));
			objArtistRecord.setImageLargePath(cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_IMAGE_LARGE_PATH)));
			objArtistRecord.setNameImagePath (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_NAME_IMAGE_PATH)));
			objArtistRecord.setGenre		 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_GENRE)));
			objArtistRecord.setURL			 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_URL)));
			objArtistRecord.setCountryId	 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_COUNTRY_ID)));
			
			switch(cursor.getInt(cursor.getColumnIndex(ArtistTable.COL_STAR_PERFORMANCE)))
			{
			case 0 : objArtistRecord.setStarPerformance(EStarPerformanceRate.EMPTY);	break;
			case 50: objArtistRecord.setStarPerformance(EStarPerformanceRate.HALF);		break;
			case 100: objArtistRecord.setStarPerformance(EStarPerformanceRate.FULL);	
			}
			
			objArtistRecord.setVideoURL(cursor.getString(cursor	.getColumnIndex(ArtistTable.COL_VEDIO_URL)));

			aryListOfRecord.add(objArtistRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			ArtistRecord objArtistRecord = new ArtistRecord();

			objArtistRecord.setAutoID		 (cursor.getInt 	(cursor.getColumnIndex(ArtistTable.COL_AUTO_ID)));
			objArtistRecord.setId			 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_ARTIST_ID)));
			objArtistRecord.setName			 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_NAME)));
			objArtistRecord.setDescription	 (cursor.getString 	(cursor.getColumnIndex(ArtistTable.COL_DESCRIPTION)));
			objArtistRecord.setRank			 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_RANK)));
			objArtistRecord.setImageSmallPath(cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_IMAGE_SMALL_PATH)));
			objArtistRecord.setImageLargePath(cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_IMAGE_LARGE_PATH)));
			objArtistRecord.setNameImagePath (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_NAME_IMAGE_PATH)));
			objArtistRecord.setGenre		 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_GENRE)));
			objArtistRecord.setURL			 (cursor.getString	(cursor.getColumnIndex(ArtistTable.COL_URL)));
			objArtistRecord.setCountryId	 (cursor.getInt		(cursor.getColumnIndex(ArtistTable.COL_COUNTRY_ID)));
			
			switch(cursor.getInt(cursor.getColumnIndex(ArtistTable.COL_STAR_PERFORMANCE)))
			{
			case 0 : objArtistRecord.setStarPerformance(EStarPerformanceRate.EMPTY);	break;
			case 50: objArtistRecord.setStarPerformance(EStarPerformanceRate.HALF);		break;
			case 100: objArtistRecord.setStarPerformance(EStarPerformanceRate.FULL);	
			}
			
			objArtistRecord.setVideoURL(cursor.getString(cursor	.getColumnIndex(ArtistTable.COL_VEDIO_URL)));

			aryListOfRecord.add(objArtistRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		ArtistRecord artistRecord 	= (ArtistRecord) BeanClassObject;
		ContentValues values 		= new ContentValues();
		switch(artistRecord.getStarPerformance())
		{
		case EMPTY 	: values.put(COL_STAR_PERFORMANCE, IConstants.INT_STAR_EMPTY);		break;
		case HALF	: values.put(COL_STAR_PERFORMANCE, IConstants.INT_STAR_HALF);		break;
		case FULL	: values.put(COL_STAR_PERFORMANCE, IConstants.INT_STAR_FULL);	break;
		}
		

		database.update(TABLE_NAME, values,COL_ARTIST_ID + " = " + artistRecord.getId(), null);
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		return null;
	}
}