package chrome.umf.model.artist;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class ArtistXMLHandler extends ChromeDefaultHandler implements IDebug 
{
	private static boolean isUpdateInDatabase;

	private static final String strUrl = IUrl.URL_ARTIST;
	private static final ITable table = new ArtistTable();

	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_ARTIST;
	private Debugger debugger;

	private boolean flag_Artist 		= false;
	private boolean flag_Id 			= false;
	private boolean flag_Name 			= false;
	private boolean flag_Description 	= false;
	private boolean flag_Rank 			= false;
	private boolean flag_ImageSmallPath = false;
	private boolean flag_ImageLargePath = false;
	private boolean flag_NameImagePath 	= false;
	private boolean flag_Genre 			= false;
	private boolean flag_URL 			= false;
	private boolean flag_CountryId 		= false;
	private boolean flag_VideoURL 		= false;

	private ArtistRecord artistRecord 	= null;
	private Database database 			= null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();

		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All ", " Start");
		debugger.logForAll(2, "ID		Name	Description	Rank	ImageSmallPath	MapImageUrl ",	" Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals("artist")) 
		{
			flag_Artist = true;
			artistRecord = new ArtistRecord();
		}

		else if (localName.equals("Id")) 
		{
			flag_Id = true;
		}
		
		else if (localName.equals("Name")) 
		{
			flag_Name = true;
		}
		
		else if (localName.equals("Description")) 
		{
			flag_Description = true;
		}
		
		else if (localName.equals("Rank")) 
		{
			flag_Rank = true;
		} 
		
		else if (localName.equals("ImageSmallPath")) 
		{
			flag_ImageSmallPath = true;
		}
		
		else if (localName.equals("ImageLargePath")) 
		{
			flag_ImageLargePath = true;
		} 
		
		else if (localName.equals("NameImagePath")) 
		{
			flag_NameImagePath = true;
		} 
		
		else if (localName.equals("Genre")) 
		{
			flag_Genre = true;
		} 
		
		else if (localName.equals("URL")) 
		{
			flag_URL = true;
		}
		
		else if (localName.equals("CountryId")) 
		{
			flag_CountryId = true;
		} 
		
		else if (localName.equals("VideoURL")) 
		{
			flag_VideoURL = true;
		}
	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_Id) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				artistRecord.setId(Integer.parseInt(new String(ch, start, length).trim()));
		}

		else if (flag_Rank) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				artistRecord.setRank(Integer.parseInt(new String(ch, start,	length).trim()));
		} 
		
		else if (flag_CountryId) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				artistRecord.setCountryId(Integer.parseInt(new String(ch, start, length).trim()));
		}

		// Set value for string data
		else if (flag_Name) 
		{
			artistRecord.setName((new String(ch, start, length)).trim());
		} 
		
		else if (flag_Description) 
		{
			artistRecord.setDescription((new String(ch, start, length)).trim());
		} 
		
		else if (flag_ImageSmallPath) 
		{
			artistRecord.setImageSmallPath((new String(ch, start, length)).trim());
		} 
		
		else if (flag_ImageLargePath) 
		{
			artistRecord.setImageLargePath((new String(ch, start, length)).trim());
		} 
		
		else if (flag_NameImagePath) 
		{
			artistRecord.setNameImagePath((new String(ch, start, length)).trim());
		} 
		
		else if (flag_Genre) 
		{
			artistRecord.setGenre((new String(ch, start, length)).trim());
		} 
		
		else if (flag_URL) 
		{
			artistRecord.setURL((new String(ch, start, length)).trim());
		} 
		
		else if (flag_VideoURL) 
		{
			artistRecord.setVideoURL((new String(ch, start, length)).trim());
		}
	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals("Id")) 
		{
			flag_Id = false;
		} 
		
		else if (localName.equals("Name")) 
		{
			flag_Name = false;
		} 
		
		else if (localName.equals("Description")) 
		{
			flag_Description = false;
		} 
		
		else if (localName.equals("Rank")) 
		{
			flag_Rank = false;
		} 
		
		else if (localName.equals("ImageSmallPath")) 
		{
			flag_ImageSmallPath = false;
		} 
		
		else if (localName.equals("ImageLargePath")) 
		{
			flag_ImageLargePath = false;
		} 
		
		else if (localName.equals("NameImagePath")) 
		{
			flag_NameImagePath = false;
		} 
		
		else if (localName.equals("Genre")) 
		{
			flag_Genre = false;
		} 
		
		else if (localName.equals("URL")) 
		{
			flag_URL = false;
		} 
		
		else if (localName.equals("CountryId")) 
		{
			flag_CountryId = false;
		} 
		
		else if (localName.equals("VideoURL")) 
		{
			flag_VideoURL = false;
		}

		else if (localName.equals("artist")) 
		{
			debugger.logForAll(2, "R: ", 		  "ID : " 		+ artistRecord.getId()
												+ "Name : " 	+ artistRecord.getName() 
												+ " ImageName:"	+ artistRecord.getNameImagePath());
			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(artistRecord);
			}
			flag_Artist = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();

		if (isUpdateInDatabase) 
		{
			database.close();
		}

		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		ArtistXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}

}// END ArtistXmlHandler