package chrome.umf.model.day;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class DayRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE 	= IDebug.IS_DEBUGABLE_FLAG_MODEL_DAY;
	public static final DayTable DAY_TABLE 		= new DayTable();

	private int autoID;

	private int 	id;
	private String 	name;
	private String 	date;
	private String 	startTime;
	private String 	endTime;
	private int 	festivalId;

	public DayTable getTableObject() 
	{
		return DAY_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDate() 
	{
		return date;
	}

	public void setDate(String date) 
	{
		this.date = date;
	}

	public String getStartTime() 
	{
		return startTime;
	}

	public void setStartTime(String startTime) 
	{
		this.startTime = startTime;
	}

	public String getEndTime() 
	{
		return endTime;
	}

	public void setEndTime(String endTime) 
	{
		this.endTime = endTime;
	}

	public int getFestivalId() 
	{
		return festivalId;
	}

	public void setFestivalId(int festivalId) 
	{
		this.festivalId = festivalId;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}