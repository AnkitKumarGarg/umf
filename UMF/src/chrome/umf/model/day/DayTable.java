package chrome.umf.model.day;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DayTable implements ITable, IDebug 
{
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_DAY;

	public static final String TABLE_NAME 		= "tableDayRecord";
	public static final String COL_AUTO_ID 		= "autoId";
	public static final String COL_ID 			= "id";
	public static final String COL_NAME 		= "name";
	public static final String COL_DATE 		= "date";
	public static final String COL_START_TIME 	= "startTime";
	public static final String COL_END_TIME 	= "endTime";
	public static final String COL_FESTIVAL_ID 	= "festivalId";

	public static ContentValues values = new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME	+ " ( " 
									+ COL_AUTO_ID 		+ " INTEGER " + " primary key autoincrement,"
									+ COL_ID 			+ " INTEGER , " 
									+ COL_NAME 			+ " text , " 
									+ COL_DATE			+ " text , " 
									+ COL_START_TIME 	+ " text , " 
									+ COL_END_TIME		+ " text , " 
									+ COL_FESTIVAL_ID 	+ " INTEGER " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public DayTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		DayRecord objDayRecord = (DayRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(DayTable.	COL_ID			, objDayRecord.getId());
		values.put(DayTable.	COL_NAME		, objDayRecord.getName());
		values.put(DayTable.	COL_DATE		, objDayRecord.getDate());
		values.put(DayTable.	COL_START_TIME	, objDayRecord.getStartTime());
		values.put(DayTable.	COL_END_TIME	, objDayRecord.getEndTime());
		values.put(DayTable.	COL_FESTIVAL_ID	, objDayRecord.getFestivalId());
		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId, SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from "	+ DayTable.TABLE_NAME + " where rowId = " + rowId, null);
		cursor.moveToFirst();
		DayRecord objDayRecord = (DayRecord) beanClassObject;

		objDayRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_AUTO_ID)));
		objDayRecord.setId			(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_ID)));
		objDayRecord.setName		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_NAME)));
		objDayRecord.setDate		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_DATE)));
		objDayRecord.setStartTime	(cursor.getString	(cursor.getColumnIndex(DayTable.COL_START_TIME)));
		objDayRecord.setEndTime		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_END_TIME)));
		objDayRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_FESTIVAL_ID)));
		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objDayRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from "	+ DayTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			DayRecord objDayRecord = new DayRecord();

			objDayRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_AUTO_ID)));
			objDayRecord.setId			(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_ID)));
			objDayRecord.setName		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_NAME)));
			objDayRecord.setDate		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_DATE)));
			objDayRecord.setStartTime	(cursor.getString	(cursor.getColumnIndex(DayTable.COL_START_TIME)));
			objDayRecord.setEndTime		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_END_TIME)));
			objDayRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_FESTIVAL_ID)));
			
			aryListOfRecord.add(objDayRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(
			SQLiteDatabase database, String strQuery) {
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) {
			DayRecord objDayRecord = new DayRecord();

			objDayRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_AUTO_ID)));
			objDayRecord.setId			(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_ID)));
			objDayRecord.setName		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_NAME)));
			objDayRecord.setDate		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_DATE)));
			objDayRecord.setStartTime	(cursor.getString	(cursor.getColumnIndex(DayTable.COL_START_TIME)));
			objDayRecord.setEndTime		(cursor.getString	(cursor.getColumnIndex(DayTable.COL_END_TIME)));
			objDayRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(DayTable.COL_FESTIVAL_ID)));
			
			aryListOfRecord.add(objDayRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}