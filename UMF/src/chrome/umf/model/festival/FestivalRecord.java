package chrome.umf.model.festival;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class FestivalRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_FESTIVAL;
	public static final FestivalTable FESTIVAL_TABLE = new FestivalTable();

	private int autoID;

	private int 	id;
	private String 	name;
	private String 	description;
	private String 	locationVenue;
	private String 	locationAddress1;
	private String 	locationAddress2;
	private String 	locationCity;
	private String 	locationState;
	private int 	lookupCountry_ID;
	private String	dateStart;
	private String 	dateEnd;
	private String 	review;
	private String 	imagePath;
	private String 	url;
	private int 	lokupGenre_ID;

	public FestivalTable getTableObject() 
	{
		return FESTIVAL_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getLocationVenue() 
	{
		return locationVenue;
	}

	public void setLocationVenue(String locationVenue) 
	{
		this.locationVenue = locationVenue;
	}

	public String getLocationAddress1() 
	{
		return locationAddress1;
	}

	public void setLocationAddress1(String locationAddress1) 
	{
		this.locationAddress1 = locationAddress1;
	}

	public String getLocationAddress2() 
	{
		return locationAddress2;
	}

	public void setLocationAddress2(String locationAddress2) 
	{
		this.locationAddress2 = locationAddress2;
	}

	public String getLocationCity() 
	{
		return locationCity;
	}

	public void setLocationCity(String locationCity) 
	{
		this.locationCity = locationCity;
	}

	public String getLocationState() 
	{
		return locationState;
	}

	public void setLocationState(String locationState) 
	{
		this.locationState = locationState;
	}

	public int getLookupCountry_ID() 
	{
		return lookupCountry_ID;
	}

	public void setLookupCountry_ID(int lookupCountry_ID) 
	{
		this.lookupCountry_ID = lookupCountry_ID;
	}

	public String getDateStart() 
	{
		return dateStart;
	}

	public void setDateStart(String dateStart) 
	{
		this.dateStart = dateStart;
	}

	public String getDateEnd() 
	{
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) 
	{
		this.dateEnd = dateEnd;
	}

	public String getReview() 
	{
		return review;
	}

	public void setReview(String review) 
	{
		this.review = review;
	}

	public String getImagePath() 
	{
		return imagePath;
	}

	public void setImagePath(String imagePath) 
	{
		this.imagePath = imagePath;
	}

	public String getUrl() 
	{
		return url;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public int getLokupGenre_ID() 
	{
		return lokupGenre_ID;
	}

	public void setLokupGenre_ID(int lokupGenre_ID) 
	{
		this.lokupGenre_ID = lokupGenre_ID;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}