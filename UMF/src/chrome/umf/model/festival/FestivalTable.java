package chrome.umf.model.festival;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FestivalTable implements ITable, IDebug 
{
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_FESTIVAL;

	public static final String TABLE_NAME = "tableFestivalRecord";

	public static final String COL_AUTO_ID 				= "autoId";
	public static final String COL_ID 					= "id";
	public static final String COL_NAME 				= "name";
	public static final String COL_DESCRIOTION 			= "Description";
	public static final String COL_LOCATION_VENUE 		= "LocationVenue";
	public static final String COL_LOCATION_ADDRESS_1 	= "LocationAddress1";
	public static final String COL_LOCATION_ADDRESS_2 	= "LocationAddress2";
	public static final String COL_LOCATION_CITY 		= "LocationCity";
	public static final String COL_LOCATION_STATE 		= "LocationState";
	public static final String COL_LOOK_UP_COUNTRY_ID 	= "LookupCountry_ID";
	public static final String COL_DATE_START 			= "DateStart";
	public static final String COL_DATE_END 			= "DateEnd";
	public static final String COL_REVIEW 				= "Review";
	public static final String COL_IMAGE_PATH 			= "ImagePath";
	public static final String COL_URL	 				= "URL";
	public static final String COL_LOOK_UP_GENRE_ID 	= "LookupGenre_ID";

	public static ContentValues values = new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME	+ " ( " 
						+ COL_AUTO_ID 				+ " INTEGER " + " primary key autoincrement,"
						+ COL_ID 					+ " INTEGER , " 
						+ COL_NAME 					+ " text , " 
						+ COL_DESCRIOTION 			+ " text , " 
						+ COL_LOCATION_VENUE 		+ " text , "
						+ COL_LOCATION_ADDRESS_1 	+ " text , " 
						+ COL_LOCATION_ADDRESS_2 	+ " text , " 
						+ COL_LOCATION_CITY 		+ " text , " 
						+ COL_LOCATION_STATE 		+ " text , " 
						+ COL_LOOK_UP_COUNTRY_ID 	+ " INTEGER , "
						+ COL_DATE_START 			+ " text , " 
						+ COL_DATE_END 				+ " text , "
						+ COL_REVIEW 				+ " text , " 
						+ COL_IMAGE_PATH 			+ " text , " 
						+ COL_URL 					+ " text , " 
						+ COL_LOOK_UP_GENRE_ID 		+ " INTEGER " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public FestivalTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		FestivalRecord objFestivalRecord = (FestivalRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(FestivalTable.COL_ID					, objFestivalRecord.getId());
		values.put(FestivalTable.COL_NAME				, objFestivalRecord.getName());
		values.put(FestivalTable.COL_DESCRIOTION		, objFestivalRecord.getDescription());
		values.put(FestivalTable.COL_LOCATION_VENUE		, objFestivalRecord.getLocationVenue());
		values.put(FestivalTable.COL_LOCATION_ADDRESS_1	, objFestivalRecord.getLocationAddress1());
		values.put(FestivalTable.COL_LOCATION_ADDRESS_2	, objFestivalRecord.getLocationAddress2());
		values.put(FestivalTable.COL_LOCATION_CITY		, objFestivalRecord.getLocationCity());
		values.put(FestivalTable.COL_LOCATION_STATE		, objFestivalRecord.getLocationState());
		values.put(FestivalTable.COL_LOOK_UP_COUNTRY_ID	, objFestivalRecord.getLookupCountry_ID());
		values.put(FestivalTable.COL_DATE_START			, objFestivalRecord.getDateStart());
		values.put(FestivalTable.COL_DATE_END			, objFestivalRecord.getDateEnd());
		values.put(FestivalTable.COL_REVIEW				, objFestivalRecord.getReview());
		values.put(FestivalTable.COL_IMAGE_PATH			, objFestivalRecord.getImagePath());
		values.put(FestivalTable.COL_URL				, objFestivalRecord.getUrl());
		values.put(FestivalTable.COL_LOOK_UP_GENRE_ID	, objFestivalRecord.getLokupGenre_ID());

		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId,SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + FestivalTable.TABLE_NAME + " where rowId = " + rowId, null);
		cursor.moveToFirst();
		FestivalRecord objFestivalRecord = (FestivalRecord) beanClassObject;

		objFestivalRecord.setAutoID				(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_AUTO_ID)));
		objFestivalRecord.setId					(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_ID)));
		objFestivalRecord.setName				(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_NAME)));
		objFestivalRecord.setDescription		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DESCRIOTION)));
		objFestivalRecord.setLocationVenue		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_VENUE)));
		objFestivalRecord.setLocationAddress1	(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_ADDRESS_1)));
		objFestivalRecord.setLocationAddress2	(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_ADDRESS_2)));
		objFestivalRecord.setLocationCity		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_CITY)));
		objFestivalRecord.setLocationState		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_STATE)));
		objFestivalRecord.setLookupCountry_ID	(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_LOOK_UP_COUNTRY_ID)));
		objFestivalRecord.setDateStart			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DATE_START)));
		objFestivalRecord.setDateEnd			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DATE_END)));
		objFestivalRecord.setReview				(cursor.getString	(cursor	.getColumnIndex(FestivalTable.COL_REVIEW)));
		objFestivalRecord.setImagePath			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_IMAGE_PATH)));
		objFestivalRecord.setUrl				(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_URL)));
		objFestivalRecord.setLokupGenre_ID		(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_LOOK_UP_GENRE_ID)));

		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objFestivalRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + FestivalTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			FestivalRecord objFestivalRecord = new FestivalRecord();

			objFestivalRecord.setAutoID				(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_AUTO_ID)));
			objFestivalRecord.setId					(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_ID)));
			objFestivalRecord.setName				(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_NAME)));
			objFestivalRecord.setDescription		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DESCRIOTION)));
			objFestivalRecord.setLocationVenue		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_VENUE)));
			objFestivalRecord.setLocationAddress1	(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_ADDRESS_1)));
			objFestivalRecord.setLocationAddress2	(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_ADDRESS_2)));
			objFestivalRecord.setLocationCity		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_CITY)));
			objFestivalRecord.setLocationState		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_STATE)));
			objFestivalRecord.setLookupCountry_ID	(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_LOOK_UP_COUNTRY_ID)));
			objFestivalRecord.setDateStart			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DATE_START)));
			objFestivalRecord.setDateEnd			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DATE_END)));
			objFestivalRecord.setReview				(cursor.getString	(cursor	.getColumnIndex(FestivalTable.COL_REVIEW)));
			objFestivalRecord.setImagePath			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_IMAGE_PATH)));
			objFestivalRecord.setUrl				(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_URL)));
			objFestivalRecord.setLokupGenre_ID		(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_LOOK_UP_GENRE_ID)));

			aryListOfRecord.add(objFestivalRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			FestivalRecord objFestivalRecord = new FestivalRecord();

			objFestivalRecord.setAutoID				(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_AUTO_ID)));
			objFestivalRecord.setId					(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_ID)));
			objFestivalRecord.setName				(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_NAME)));
			objFestivalRecord.setDescription		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DESCRIOTION)));
			objFestivalRecord.setLocationVenue		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_VENUE)));
			objFestivalRecord.setLocationAddress1	(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_ADDRESS_1)));
			objFestivalRecord.setLocationAddress2	(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_ADDRESS_2)));
			objFestivalRecord.setLocationCity		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_CITY)));
			objFestivalRecord.setLocationState		(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_LOCATION_STATE)));
			objFestivalRecord.setLookupCountry_ID	(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_LOOK_UP_COUNTRY_ID)));
			objFestivalRecord.setDateStart			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DATE_START)));
			objFestivalRecord.setDateEnd			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_DATE_END)));
			objFestivalRecord.setReview				(cursor.getString	(cursor	.getColumnIndex(FestivalTable.COL_REVIEW)));
			objFestivalRecord.setImagePath			(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_IMAGE_PATH)));
			objFestivalRecord.setUrl				(cursor.getString	(cursor.getColumnIndex(FestivalTable.COL_URL)));
			objFestivalRecord.setLokupGenre_ID		(cursor.getInt		(cursor.getColumnIndex(FestivalTable.COL_LOOK_UP_GENRE_ID)));

			aryListOfRecord.add(objFestivalRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}