package chrome.umf.model.festival;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class FestivalXMLHandler extends ChromeDefaultHandler implements IDebug {

	private static final String TAGM_ROW 				= "row";
	private static final String TAG_ID 					= "Id";
	private static final String TAG_NAME 				= "Name";
	private static final String TAG_DESCRIOTION 		= "Description";
	private static final String TAG_LOCATION_VENUE 		= "LocationVenue";
	private static final String TAG_LOCATION_ADDRESS_1 	= "LocationAddress1";
	private static final String TAG_LOCATION_ADDRESS_2 	= "LocationAddress2";
	private static final String TAG_LOCATION_CITY 		= "LocationCity";
	private static final String TAG_LOCATION_STATE 		= "LocationState";
	private static final String TAG_LOOK_UP_COUNTRY_ID 	= "LookupCountry_ID";
	private static final String TAG_DATE_START 			= "DateStart";
	private static final String TAG_DATE_END 			= "DateEnd";
	private static final String TAG_REVIEW 				= "Review";
	private static final String TAG_IMAGE_PATH 			= "ImagePath";
	private static final String TAG_URL 				= "URL";
	private static final String TAG_LOOK_UP_GENRE_ID 	= "LookupGenre_ID";

	private static boolean isUpdateInDatabase;

	private static final String strUrl 					= IUrl.URL_FESTIVAL;
	private static final ITable table 					= new FestivalTable();

	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_FESTIVAL;
	private Debugger debugger;

	private boolean flag_ROW 				= false;
	private boolean flag_ID 				= false;
	private boolean flag_NAME 				= false;
	private boolean flag_DESCRIOTION 		= false;
	private boolean flag_LOCATION_VENUE 	= false;
	private boolean flag_LOCATION_ADDRESS_1 = false;
	private boolean flag_LOCATION_ADDRESS_2 = false;
	private boolean flag_LOCATION_CITY 		= false;
	private boolean flag_LOCATION_STATE 	= false;
	private boolean flag_LOOK_UP_COUNTRY_ID = false;
	private boolean flag_DATE_START 		= false;
	private boolean flag_DATE_END 			= false;
	private boolean flag_REVIEW 			= false;
	private boolean flag_IMAGE_PATH 		= false;
	private boolean flag_URL 				= false;
	private boolean flag_LOOK_UP_GENRE_ID 	= false;

	private FestivalRecord festivalRecord = null;
	private Database database = null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();

		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All ", " Start");
		debugger.logForAll(2, "ID		Name	Description ", " Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals(TAGM_ROW)) 
		{
			flag_ROW = true;
			festivalRecord = new FestivalRecord();
		}

		else if (localName.equals(TAG_ID)) 
		{
			flag_ID = true;
		}
		
		else if (localName.equals(TAG_NAME)) 
		{
			flag_NAME = true;
		}
		
		else if (localName.equals(TAG_DESCRIOTION)) 
		{
			flag_DESCRIOTION = true;
		} 
		
		else if (localName.equals(TAG_LOCATION_VENUE)) 
		{
			flag_LOCATION_VENUE = true;
		}
		
		else if (localName.equals(TAG_LOCATION_ADDRESS_1)) 
		{
			flag_LOCATION_ADDRESS_1 = true;
		}
		
		else if (localName.equals(TAG_LOCATION_ADDRESS_2)) 
		{
			flag_LOCATION_ADDRESS_2 = true;
		} 
		
		else if (localName.equals(TAG_LOCATION_CITY)) 
		{
			flag_LOCATION_CITY = true;
		}
		
		else if (localName.equals(TAG_LOCATION_STATE)) 
		{
			flag_LOCATION_STATE = true;
		} 
		
		else if (localName.equals(TAG_LOOK_UP_COUNTRY_ID)) 
		{
			flag_LOOK_UP_COUNTRY_ID = true;
		}
		
		else if (localName.equals(TAG_DATE_START)) 
		{
			flag_DATE_START = true;
		}
		
		else if (localName.equals(TAG_DATE_END)) 
		{
			flag_DATE_END = true;
		}
		
		else if (localName.equals(TAG_REVIEW)) 
		{
			flag_REVIEW = true;
		}
		
		else if (localName.equals(TAG_IMAGE_PATH)) 
		{
			flag_IMAGE_PATH = true;
		}
		
		else if (localName.equals(TAG_URL)) 
		{
			flag_URL = true;
		}
		
		else if (localName.equals(TAG_LOOK_UP_GENRE_ID)) 
		{
			flag_LOOK_UP_GENRE_ID = true;
		}

	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_ID) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				festivalRecord.setId(Integer.parseInt(new String(ch, start,	length).trim()));
		}

		else if (flag_LOOK_UP_COUNTRY_ID) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				festivalRecord.setLookupCountry_ID(Integer.parseInt(new String(	ch, start, length).trim()));
		}

		else if (flag_LOOK_UP_GENRE_ID) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				festivalRecord.setLokupGenre_ID(Integer.parseInt(new String(ch,	start, length).trim()));
		}

		// Set value for string data
		else if (flag_NAME) 
		{
			festivalRecord.setName((new String(ch, start, length)).trim());
		}
		
		else if (flag_DESCRIOTION) 
		{
			festivalRecord.setDescription((new String(ch, start, length)).trim());
		} 
		
		else if (flag_LOCATION_VENUE) 
		{
			festivalRecord.setLocationVenue((new String(ch, start, length))
					.trim());
		} 
		
		else if (flag_LOCATION_ADDRESS_1) 
		{
			festivalRecord.setLocationAddress1((new String(ch, start, length))
					.trim());
		} 
		
		else if (flag_LOCATION_ADDRESS_2) 
		{
			festivalRecord.setLocationAddress2((new String(ch, start, length))
					.trim());
		} 
		
		else if (flag_LOCATION_CITY) 
		{
			festivalRecord.setLocationCity((new String(ch, start, length)).trim());
		} 
		
		else if (flag_LOCATION_STATE) 
		{
			festivalRecord.setLocationState((new String(ch, start, length))	.trim());
		}
		
		else if (flag_DATE_START) 
		{
			festivalRecord.setDateStart((new String(ch, start, length)).trim());
		}
		
		else if (flag_DATE_END) 
		{
			festivalRecord.setDateEnd((new String(ch, start, length)).trim());
		} 
		
		else if (flag_REVIEW) 
		{
			festivalRecord.setReview((new String(ch, start, length)).trim());
		} 
		
		else if (flag_IMAGE_PATH) 
		{
			festivalRecord.setImagePath((new String(ch, start, length)).trim());
		} 
		
		else if (flag_URL) 
		{
			festivalRecord.setUrl((new String(ch, start, length)).trim());
		}

	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals(TAG_ID)) 
		{
			flag_ID = false;
		}
		
		else if (localName.equals(TAG_NAME)) 
		{
			flag_NAME = false;
		}
		
		else if (localName.equals(TAG_DESCRIOTION)) 
		{
			flag_DESCRIOTION = false;
		} 
		
		else if (localName.equals(TAG_LOCATION_VENUE)) 
		{
			flag_LOCATION_VENUE = false;
		}
		
		else if (localName.equals(TAG_LOCATION_ADDRESS_1)) 
		{
			flag_LOCATION_ADDRESS_1 = false;
		}
		
		else if (localName.equals(TAG_LOCATION_ADDRESS_2)) 
		{
			flag_LOCATION_ADDRESS_2 = false;
		}
		
		else if (localName.equals(TAG_LOCATION_CITY)) 
		{
			flag_LOCATION_CITY = false;
		}
		
		else if (localName.equals(TAG_LOCATION_STATE)) 
		{
			flag_LOCATION_STATE = false;
		} 
		
		else if (localName.equals(TAG_LOOK_UP_COUNTRY_ID)) 
		{
			flag_LOOK_UP_COUNTRY_ID = false;
		}
		
		else if (localName.equals(TAG_DATE_START)) 
		{
			flag_DATE_START = false;
		}
		
		else if (localName.equals(TAG_DATE_END)) 
		{
			flag_DATE_END = false;
		} 
		
		else if (localName.equals(TAG_REVIEW)) 
		{
			flag_REVIEW = false;
		} 
		
		else if (localName.equals(TAG_IMAGE_PATH)) 
		{
			flag_IMAGE_PATH = false;
		} 
		
		else if (localName.equals(TAG_URL)) 
		{
			flag_URL = false;
		}
		
		else if (localName.equals(TAG_LOOK_UP_GENRE_ID)) 
		{
			flag_LOOK_UP_GENRE_ID = false;
		}

		else if (localName.equals(TAGM_ROW)) 
		{
			debugger.logForAll(	2,	"R: ",	"  \nID:" 					+ festivalRecord.getId() 
											+ "  \nName:"				+ festivalRecord.getName() 
											+ "  \nDescription:"		+ festivalRecord.getDescription()
											+ "  \nLocation Venue:" 	+ festivalRecord.getLocationVenue()
											+ "  \nLocation Address 1:"	+ festivalRecord.getLocationAddress1()
											+ "  \nLocation Address 2:"	+ festivalRecord.getLocationAddress2()
											+ "  \nLocation City:"		+ festivalRecord.getLocationCity()
											+ "  \nLocation State:"		+ festivalRecord.getLocationState()
											+ "  \nLook Up County Id:"	+ festivalRecord.getLookupCountry_ID()
											+ "  \nDate Start:" 		+ festivalRecord.getDateStart()
											+ "  \nDate End:" 			+ festivalRecord.getDateEnd()
											+ "  \nReview:" 			+ festivalRecord.getReview()
											+ "  \nImage Path:" 		+ festivalRecord.getImagePath()
											+ "  \nUrl:" 				+ festivalRecord.getUrl()
											+ "  \nLook Up Genere Id:"  + festivalRecord.getLokupGenre_ID());

			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(festivalRecord);
			}
			flag_ROW = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();

		if (isUpdateInDatabase) 
		{
			database.close();
		}

		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		FestivalXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}

}// END ArtistXmlHandler