package chrome.umf.model.festivalVideo;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class FestivalVideoRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_FESTIVAL_VIDEO;
	public static final FestivalVideoTable FESTIVAL_VIDEO_TABLE = new FestivalVideoTable();

	private int 	autoID;

	private int 	id;
	private String 	name;
	private String 	url;
	private int 	festivalId;

	public FestivalVideoTable getTableObject() 
	{
		return FESTIVAL_VIDEO_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getUrl() 
	{
		return url;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public int getFestivalId() 
	{
		return festivalId;
	}

	public void setFestivalId(int festivalId) 
	{
		this.festivalId = festivalId;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}