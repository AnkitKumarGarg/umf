package chrome.umf.model.festivalVideo;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FestivalVideoTable implements ITable, IDebug 
{
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_FESTIVAL_VIDEO;

	public static final String TABLE_NAME 		= "tableFestivalVideoRecord";
	public static final String COL_AUTO_ID 		= "autoId";
	public static final String COL_ID 			= "id";
	public static final String COL_NAME 		= "name";
	public static final String COL_URL 			= "date";
	public static final String COL_FESTIVAL_ID 	= "festivalId";

	public static ContentValues values 			= new ContentValues();

	public final static String CREATE_TABLE 	= "create table " + TABLE_NAME + " ( " 
											+ COL_AUTO_ID 		+ " INTEGER " + " primary key autoincrement,"
											+ COL_ID 			+ " INTEGER , " 
											+ COL_NAME 			+ " text , " 
											+ COL_URL			+ " text , " 
											+ COL_FESTIVAL_ID 	+ " INTEGER " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public FestivalVideoTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		FestivalVideoRecord objFestivalVideoRecord = (FestivalVideoRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(FestivalVideoTable.COL_ID			,objFestivalVideoRecord.getId());
		values.put(FestivalVideoTable.COL_NAME			,objFestivalVideoRecord.getName());
		values.put(FestivalVideoTable.COL_URL			,objFestivalVideoRecord.getUrl());
		values.put(FestivalVideoTable.COL_FESTIVAL_ID	,objFestivalVideoRecord.getFestivalId());

		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId, SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + FestivalVideoTable.TABLE_NAME + " where rowId = " + rowId,	null);
		cursor.moveToFirst();
		FestivalVideoRecord objFestivalVideoRecord = (FestivalVideoRecord) beanClassObject;

		objFestivalVideoRecord.setAutoID	(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_AUTO_ID)));
		objFestivalVideoRecord.setId		(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_ID)));
		objFestivalVideoRecord.setName		(cursor.getString	(cursor.getColumnIndex(FestivalVideoTable.	COL_NAME)));
		objFestivalVideoRecord.setUrl		(cursor.getString	(cursor.getColumnIndex(FestivalVideoTable.	COL_URL)));
		objFestivalVideoRecord.setFestivalId(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_FESTIVAL_ID)));

		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objFestivalVideoRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + FestivalVideoTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			FestivalVideoRecord objFestivalVideoRecord = new FestivalVideoRecord();

			objFestivalVideoRecord.setAutoID	(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_AUTO_ID)));
			objFestivalVideoRecord.setId		(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_ID)));
			objFestivalVideoRecord.setName		(cursor.getString	(cursor.getColumnIndex(FestivalVideoTable.	COL_NAME)));
			objFestivalVideoRecord.setUrl		(cursor.getString	(cursor.getColumnIndex(FestivalVideoTable.	COL_URL)));
			objFestivalVideoRecord.setFestivalId(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_FESTIVAL_ID)));

			aryListOfRecord.add(objFestivalVideoRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			FestivalVideoRecord objFestivalVideoRecord = new FestivalVideoRecord();

			objFestivalVideoRecord.setAutoID	(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_AUTO_ID)));
			objFestivalVideoRecord.setId		(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_ID)));
			objFestivalVideoRecord.setName		(cursor.getString	(cursor.getColumnIndex(FestivalVideoTable.	COL_NAME)));
			objFestivalVideoRecord.setUrl		(cursor.getString	(cursor.getColumnIndex(FestivalVideoTable.	COL_URL)));
			objFestivalVideoRecord.setFestivalId(cursor.getInt		(cursor.getColumnIndex(FestivalVideoTable.	COL_FESTIVAL_ID)));

			aryListOfRecord.add(objFestivalVideoRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}