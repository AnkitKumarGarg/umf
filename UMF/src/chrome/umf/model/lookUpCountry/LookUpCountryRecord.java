package chrome.umf.model.lookUpCountry;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class LookUpCountryRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_LOOK_UP_COUNTRY;
	public static final LookUpCountryTable LookUpCountry_TABLE = new LookUpCountryTable();

	private int autoID;

	private int id;
	private String name;

	public LookUpCountryTable getTableObject() 
	{
		return LookUpCountry_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

}