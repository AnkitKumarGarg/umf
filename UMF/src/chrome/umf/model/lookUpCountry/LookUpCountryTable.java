package chrome.umf.model.lookUpCountry;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public  class LookUpCountryTable implements ITable, IDebug {
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_LOOK_UP_COUNTRY;

	public static final String TABLE_NAME 	= "tableLookUpCountryRecord";
	public static final String COL_AUTO_ID 	= "autoId";
	public static final String COL_ID 		= "id";
	public static final String COL_NAME 	= "name";

	public static ContentValues values 		= new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME + " ( " 
												+ COL_AUTO_ID 	+ " INTEGER " + " primary key autoincrement,"
												+ COL_ID 		+ " INTEGER , " 
												+ COL_NAME 		+ " text  " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public LookUpCountryTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		LookUpCountryRecord objLookUpCountryRecord = (LookUpCountryRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(LookUpCountryTable.COL_ID, objLookUpCountryRecord.getId());
		values.put(LookUpCountryTable.COL_NAME,	objLookUpCountryRecord.getName());
		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId,SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + LookUpCountryTable.TABLE_NAME + " where id = " + rowId, null);
		cursor.moveToFirst();
		LookUpCountryRecord objLookUpCountryRecord = (LookUpCountryRecord) beanClassObject;
		if(cursor.getCount()>0)
		{
		objLookUpCountryRecord.setAutoID	(cursor.getInt		(cursor.getColumnIndex(LookUpCountryTable.COL_AUTO_ID)));
		objLookUpCountryRecord.setId		(cursor.getInt		(cursor.getColumnIndex(LookUpCountryTable.COL_ID)));
		objLookUpCountryRecord.setName		(cursor.getString	(cursor.getColumnIndex(LookUpCountryTable.COL_NAME)));
		}
		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objLookUpCountryRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + LookUpCountryTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			LookUpCountryRecord objLookUpCountryRecord = new LookUpCountryRecord();

			objLookUpCountryRecord.setAutoID	(cursor.getInt		(cursor.getColumnIndex(LookUpCountryTable.COL_AUTO_ID)));
			objLookUpCountryRecord.setId		(cursor.getInt		(cursor.getColumnIndex(LookUpCountryTable.COL_ID)));
			objLookUpCountryRecord.setName		(cursor.getString	(cursor.getColumnIndex(LookUpCountryTable.COL_NAME)));

			aryListOfRecord.add(objLookUpCountryRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			LookUpCountryRecord objLookUpCountryRecord = new LookUpCountryRecord();

			objLookUpCountryRecord.setAutoID	(cursor.getInt		(cursor.getColumnIndex(LookUpCountryTable.COL_AUTO_ID)));
			objLookUpCountryRecord.setId		(cursor.getInt		(cursor.getColumnIndex(LookUpCountryTable.COL_ID)));
			objLookUpCountryRecord.setName		(cursor.getString	(cursor.getColumnIndex(LookUpCountryTable.COL_NAME)));

			aryListOfRecord.add(objLookUpCountryRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}