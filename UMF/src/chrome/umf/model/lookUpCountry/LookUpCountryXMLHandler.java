package chrome.umf.model.lookUpCountry;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class LookUpCountryXMLHandler extends ChromeDefaultHandler implements IDebug 
{
	private static boolean isUpdateInDatabase;

	private static final String strUrl = IUrl.URL_LOOK_UP_COUNTRY;
	private static final ITable table = new LookUpCountryTable();

	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_LOOK_UP_COUNTRY;
	private Debugger debugger;

	private boolean flag_Row 	= false;
	private boolean flag_Id 	= false;
	private boolean flag_Name 	= false;

	private LookUpCountryRecord lookUpCountryRecord = null;
	private Database database 	= null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();

		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All ", " Start");
		debugger.logForAll(2, "ID		Name	", " Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals("row")) 
		{
			flag_Row = true;
			lookUpCountryRecord = new LookUpCountryRecord();
		}

		else if (localName.equals("ID")) 
		{
			flag_Id = true;
		} 
		
		else if (localName.equals("Name")) 
		{
			flag_Name = true;
		}

	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_Id) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				lookUpCountryRecord.setId(Integer.parseInt(new String(ch, start, length).trim()));
		}

		// Set value for string data
		else if (flag_Name) 
		{
			lookUpCountryRecord.setName((new String(ch, start, length)).trim());
		}

	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals("ID")) 
		{
			flag_Id = false;
		}
		
		else if (localName.equals("Name")) 
		{
			flag_Name = false;
		}

		else if (localName.equals("row")) 
		{
			debugger.logForAll(2, "R: ", "  ID:" + lookUpCountryRecord.getId() + "  Name:" + lookUpCountryRecord.getName() );

			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(lookUpCountryRecord);
			}
			flag_Row = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();

		if (isUpdateInDatabase) 
		{
			database.close();
		}

		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		LookUpCountryXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}

}// END ArtistXmlHandler