package chrome.umf.model.party;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class PartyRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PARTY;
	public static final PartyTable PARTY_TABLE = new PartyTable();

	private int autoID;

	private int 	id;
	private String 	name;
	private String 	description;
	private String 	startDate;
	private String 	startTime;
	private String 	endDate;
	private String 	endTime;
	private String 	venueName;
	private String	venueAddress;
	private String	url;
	private String 	isSponsored;
	private String 	isHighlighted;

	public PartyTable getTableObject() 
	{
		return PARTY_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getStartDate() 
	{
		return startDate;
	}

	public void setStartDate(String startDate) 
	{
		this.startDate = startDate;
	}

	public String getStartTime() 
	{
		return startTime;
	}

	public void setStartTime(String startTime) 
	{
		this.startTime = startTime;
	}

	public String getEndDate() 
	{
		return endDate;
	}

	public void setEndDate(String endDate) 
	{
		this.endDate = endDate;
	}

	public String getEndTime() 
	{
		return endTime;
	}

	public void setEndTime(String endTime) 
	{
		this.endTime = endTime;
	}

	public String getVenueName() 
	{
		return venueName;
	}

	public void setVenueName(String venueName) 
	{
		this.venueName = venueName;
	}

	public String getVenueAddress() 
	{
		return venueAddress;
	}

	public void setVenueAddress(String venueAddress) 
	{
		this.venueAddress = venueAddress;
	}

	public String getUrl() 
	{
		return url;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getIsSponsored() 
	{
		return isSponsored;
	}

	public void setIsSponsored(String isSponsored) 
	{
		this.isSponsored = isSponsored;
	}

	public String getIsHighlighted() 
	{
		return isHighlighted;
	}

	public void setIsHighlighted(String isHighlighted) 
	{
		this.isHighlighted = isHighlighted;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}