package chrome.umf.model.party;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PartyTable implements ITable, IDebug {
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PARTY;

	public static final String TABLE_NAME = "tablePartyRecord";

	public static final String COL_AUTO_ID 			= "autoId";
	public static final String COL_ID 				= "ID";
	public static final String COL_NAME 			= "NAME";
	public static final String COL_DESCRIOTION 		= "DESCRIPTION";
	public static final String COL_START_DATE 		= "START_DATE";
	public static final String COL_START_TIME 		= "START_TIME";
	public static final String COL_END_DATE 		= "END_DATE";
	public static final String COL_END_TIME 		= "END_TIME";
	public static final String COL_VENUE_NAME	 	= "VENUE_NAME";
	public static final String COL_VENUE_ADDRESS 	= "VENUE_ADDRESS";
	public static final String COL_URL 				= "URL";
	public static final String COL_IS_SPONSORED 	= "IS_SPONSORED";
	public static final String COL_IS_HIGHLIGHTED 	= "IS_HIGHLIGHTED";

	public static ContentValues values = new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME + " ( " 
									+ COL_AUTO_ID 		+ " INTEGER " + " primary key autoincrement,"
									+ COL_ID 			+ " INTEGER , " 
									+ COL_NAME 			+ " text , " 
									+ COL_DESCRIOTION	+ " text , " 
									+ COL_START_DATE 	+ " text , " 
									+ COL_START_TIME 	+ " text , " 
									+ COL_END_DATE 		+ " text , " 
									+ COL_END_TIME 		+ " text , " 
									+ COL_VENUE_NAME 	+ " text , " 
									+ COL_VENUE_ADDRESS + " text , " 
									+ COL_IS_SPONSORED 	+ " text , " 
									+ COL_URL 			+ " text , "
									+ COL_IS_HIGHLIGHTED+ " text " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public PartyTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		PartyRecord objPartyRecord = (PartyRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(PartyTable.COL_ID			, objPartyRecord.getId());
		values.put(PartyTable.COL_NAME			, objPartyRecord.getName());
		values.put(PartyTable.COL_DESCRIOTION	, objPartyRecord.getDescription());
		values.put(PartyTable.COL_START_DATE	, objPartyRecord.getStartDate());
		values.put(PartyTable.COL_START_TIME	, objPartyRecord.getStartTime());
		values.put(PartyTable.COL_END_DATE		, objPartyRecord.getEndDate());
		values.put(PartyTable.COL_END_TIME		, objPartyRecord.getEndTime());
		values.put(PartyTable.COL_VENUE_NAME	, objPartyRecord.getVenueName());
		values.put(PartyTable.COL_VENUE_ADDRESS	, objPartyRecord.getVenueAddress());
		values.put(PartyTable.COL_IS_SPONSORED	, objPartyRecord.getIsSponsored());
		values.put(PartyTable.COL_URL			, objPartyRecord.getUrl());
		values.put(PartyTable.COL_IS_HIGHLIGHTED, objPartyRecord.getIsHighlighted());

		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId, SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + PartyTable.TABLE_NAME + " where rowId = " + rowId, null);
		cursor.moveToFirst();
		PartyRecord objPartyRecord = (PartyRecord) beanClassObject;

		objPartyRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(PartyTable.COL_AUTO_ID)));
		objPartyRecord.setId			(cursor.getInt		(cursor.getColumnIndex(PartyTable.COL_ID)));
		objPartyRecord.setName			(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_NAME)));
		objPartyRecord.setDescription	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_DESCRIOTION)));
		objPartyRecord.setStartDate		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_START_DATE)));
		objPartyRecord.setStartTime		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_START_TIME)));
		objPartyRecord.setEndDate		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_END_DATE)));
		objPartyRecord.setEndTime		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_END_TIME)));
		objPartyRecord.setVenueName		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_VENUE_NAME)));
		objPartyRecord.setVenueAddress	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_VENUE_ADDRESS)));
		objPartyRecord.setIsSponsored	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_IS_SPONSORED)));
		objPartyRecord.setUrl			(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_URL)));
		objPartyRecord.setIsHighlighted	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_IS_HIGHLIGHTED)));

		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objPartyRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from "+ PartyTable.TABLE_NAME 
							+ " ORDER BY " + COL_START_DATE + " , "+ COL_END_TIME + " , " + COL_START_TIME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PartyRecord objPartyRecord = new PartyRecord();

			objPartyRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(PartyTable.COL_AUTO_ID)));
			objPartyRecord.setId			(cursor.getInt		(cursor.getColumnIndex(PartyTable.COL_ID)));
			objPartyRecord.setName			(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_NAME)));
			objPartyRecord.setDescription	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_DESCRIOTION)));
			objPartyRecord.setStartDate		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_START_DATE)));
			objPartyRecord.setStartTime		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_START_TIME)));
			objPartyRecord.setEndDate		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_END_DATE)));
			objPartyRecord.setEndTime		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_END_TIME)));
			objPartyRecord.setVenueName		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_VENUE_NAME)));
			objPartyRecord.setVenueAddress	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_VENUE_ADDRESS)));
			objPartyRecord.setIsSponsored	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_IS_SPONSORED)));
			objPartyRecord.setUrl			(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_URL)));
			objPartyRecord.setIsHighlighted	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_IS_HIGHLIGHTED)));

			aryListOfRecord.add(objPartyRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition( SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PartyRecord objPartyRecord = new PartyRecord();

			objPartyRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(PartyTable.COL_AUTO_ID)));
			objPartyRecord.setId			(cursor.getInt		(cursor.getColumnIndex(PartyTable.COL_ID)));
			objPartyRecord.setName			(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_NAME)));
			objPartyRecord.setDescription	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_DESCRIOTION)));
			objPartyRecord.setStartDate		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_START_DATE)));
			objPartyRecord.setStartTime		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_START_TIME)));
			objPartyRecord.setEndDate		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_END_DATE)));
			objPartyRecord.setEndTime		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_END_TIME)));
			objPartyRecord.setVenueName		(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_VENUE_NAME)));
			objPartyRecord.setVenueAddress	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_VENUE_ADDRESS)));
			objPartyRecord.setIsSponsored	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_IS_SPONSORED)));
			objPartyRecord.setUrl			(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_URL)));
			objPartyRecord.setIsHighlighted	(cursor.getString	(cursor.getColumnIndex(PartyTable.COL_IS_HIGHLIGHTED)));

			aryListOfRecord.add(objPartyRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}