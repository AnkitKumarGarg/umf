package chrome.umf.model.party;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class PartyXMLHandler extends ChromeDefaultHandler implements IDebug 
{

	private static final String TAGM_ROW 			= "row";
	private static final String TAG_ID 				= "ID";
	private static final String TAG_NAME 			= "NAME";
	private static final String TAG_DESCRIPTION 	= "DESCRIPTION";
	private static final String TAG_START_DATE 		= "START_DATE";
	private static final String TAG_START_TIME 		= "START_TIME";
	private static final String TAG_END_DATE 		= "END_DATE";
	private static final String TAG_END_TIME 		= "END_TIME";
	private static final String TAG_VENUE_NAME 		= "VENUE_NAME";
	private static final String TAG_VENUE_ADDRESS 	= "VENUE_ADDRESS";
	private static final String TAG_URL 			= "URL";
	private static final String TAG_IS_SPONSORED 	= "IS_SPONSORED";
	private static final String TAG_IS_HIGHLIGHTED 	= "IS_HIGHLIGHTED";

	private static boolean isUpdateInDatabase;

	private static final String strUrl = IUrl.URL_PARTY;
	private static final ITable table = new PartyTable();

	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PARTY;
	private Debugger debugger;

	private boolean flag_ROW 			= false;
	private boolean flag_ID 			= false;
	private boolean flag_NAME 			= false;
	private boolean flag_DESCRIPTION 	= false;
	private boolean flag_START_DATE 	= false;
	private boolean flag_START_TIME 	= false;
	private boolean flag_END_DATE 		= false;
	private boolean flag_END_TIME 		= false;
	private boolean flag_VENUE_NAME 	= false;
	private boolean flag_VENUE_ADDRESS 	= false;
	private boolean flag_URL 			= false;
	private boolean flag_IS_SPONSORED 	= false;
	private boolean flag_IS_HIGHLIGHTED = false;

	private PartyRecord partyRecord 	= null;
	private Database 	database 		= null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();

		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All ", " Start");
		debugger.logForAll(2, "ID		Name	Description ", " Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals(TAGM_ROW)) 
		{
			flag_ROW = true;
			partyRecord = new PartyRecord();
		}

		else if (localName.equals(TAG_ID)) 
		{
			flag_ID = true;
		}
		
		else if (localName.equals(TAG_NAME)) 
		{
			flag_NAME = true;
		}
		
		else if (localName.equals(TAG_DESCRIPTION)) 
		{
			flag_DESCRIPTION = true;
		}
		
		else if (localName.equals(TAG_START_DATE)) 
		{
			flag_START_DATE = true;
		}
		
		else if (localName.equals(TAG_START_TIME)) 
		{
			flag_START_TIME = true;
		}
		
		else if (localName.equals(TAG_END_DATE)) 
		{
			flag_END_DATE = true;
		}
		
		else if (localName.equals(TAG_END_TIME)) 
		{
			flag_END_TIME = true;
		}
		
		else if (localName.equals(TAG_VENUE_NAME)) 
		{
			flag_VENUE_NAME = true;
		} 
		
		else if (localName.equals(TAG_VENUE_ADDRESS)) 
		{
			flag_VENUE_ADDRESS = true;
		}
		
		else if (localName.equals(TAG_URL)) 
		{
			flag_URL = true;
		}
		
		else if (localName.equals(TAG_IS_SPONSORED)) 
		{
			flag_IS_SPONSORED = true;
		}
		
		else if (localName.equals(TAG_IS_HIGHLIGHTED)) 
		{
			flag_IS_HIGHLIGHTED = true;
		}
	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_ID) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				partyRecord.setId(Integer .parseInt(new String(ch, start, length).trim()));
		}

		// Set value for string data
		else if (flag_NAME) 
		{
			partyRecord.setName((new String(ch, start, length)).trim());
		}
		
		else if (flag_DESCRIPTION) 
		{
			partyRecord.setDescription((new String(ch, start, length)).trim());
		}
		
		else if (flag_START_DATE) 
		{
			partyRecord.setStartDate((new String(ch, start, length)).trim());
		}
		
		else if (flag_START_TIME) 
		{
			partyRecord.setStartTime((new String(ch, start, length)).trim());
		}
		
		else if (flag_END_DATE) 
		{
			partyRecord.setEndDate((new String(ch, start, length)).trim());
		}
		
		else if (flag_END_TIME) 
		{
			partyRecord.setEndTime((new String(ch, start, length)).trim());
		} 
		
		else if (flag_VENUE_NAME) 
		{
			partyRecord.setVenueName((new String(ch, start, length)).trim());
		}
		
		else if (flag_VENUE_ADDRESS) 
		{
			partyRecord.setVenueAddress((new String(ch, start, length)).trim());
		}
		
		else if (flag_VENUE_ADDRESS) 
		{
			partyRecord.setVenueAddress((new String(ch, start, length)).trim());
		}
		
		else if (flag_URL) 
		{
			partyRecord.setUrl((new String(ch, start, length)).trim());
		}
		
		else if (flag_IS_SPONSORED) 
		{
			partyRecord.setIsSponsored((new String(ch, start, length)).trim());
		}
		
		else if (flag_IS_HIGHLIGHTED) 
		{
			partyRecord.setIsHighlighted((new String(ch, start, length)).trim());
		}

	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals(TAG_ID)) 
		{
			flag_ID = false;
		}
		
		else if (localName.equals(TAG_NAME)) 
		{
			flag_NAME = false;
		}
		
		else if (localName.equals(TAG_DESCRIPTION)) 
		{
			flag_DESCRIPTION = false;
		}
		
		else if (localName.equals(TAG_START_DATE)) 
		{
			flag_START_DATE = false;
		}
		
		else if (localName.equals(TAG_START_TIME)) 
		{
			flag_START_TIME = false;
		}
		
		else if (localName.equals(TAG_END_DATE)) 
		{
			flag_END_DATE = false;
		}
		
		else if (localName.equals(TAG_END_TIME)) 
		{
			flag_END_TIME = false;
		}
		
		else if (localName.equals(TAG_VENUE_NAME)) 
		{
			flag_VENUE_NAME = false;
		}
		
		else if (localName.equals(TAG_VENUE_ADDRESS)) 
		{
			flag_VENUE_ADDRESS = false;
		} 
		
		else if (localName.equals(TAG_URL)) 
		{
			flag_URL = false;
		}
		
		else if (localName.equals(TAG_IS_SPONSORED)) 
		{
			flag_IS_SPONSORED = false;
		} 
		
		else if (localName.equals(TAG_IS_HIGHLIGHTED)) 
		{
			flag_IS_HIGHLIGHTED = false;
		}

		else if (localName.equals(TAGM_ROW)) 
		{
			debugger.logForAll( 2,"  \n\nR: ","  \nID:" + partyRecord.getId() 
					+ "  \nName:"				+ partyRecord.getName() 
					+ "  \nDescription:" 		+ partyRecord.getDescription() 
					+ "  \nStartDate:"			+ partyRecord.getStartDate() 
					+ "  \nStart Time:"			+ partyRecord.getStartTime() 
					+ "  \nEnd Date:"			+ partyRecord.getEndDate() 
					+ "  \nEnd Time:"			+ partyRecord.getEndTime() 
					+ "  \nVenue Name:" 		+ partyRecord.getVenueName() 
					+ "  \nVenue Address:"		+ partyRecord.getVenueAddress() 
					+ "  \nUrl:"				+ partyRecord.getUrl() 
					+ "  \nIs Sponsored:"		+ partyRecord.getIsSponsored()
					+ "  \nIs Highlighted:"		+ partyRecord.getIsHighlighted());

			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(partyRecord);
			}
			flag_ROW = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();

		if (isUpdateInDatabase) 
		{
			database.close();
		}

		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		PartyXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}
}// END ArtistXmlHandler