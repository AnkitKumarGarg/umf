package chrome.umf.model.performance;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class PerformanceRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PERFORMANCE;
	public static final PerformanceTable PERFORMANCE_TABLE = new PerformanceTable();

	private int 	autoID;

	private int 	id;
	private String 	name;
	private String 	description;
	private String	startDateTime;
	private String 	endDateTime;
	private int 	festivalId;
	private int 	artistId;
	private int 	stageId;
	private int 	dayId;
	private int 	test;

	public int getTest() 
	{
		return test;
	}

	public void setTest(int test) 
	{
		this.test = test;
	}

	public PerformanceTable getTableObject() 
	{
		return PERFORMANCE_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getStartDateTime() 
	{
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) 
	{
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() 
	{
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) 
	{
		this.endDateTime = endDateTime;
	}

	public int getFestivalId() 
	{
		return festivalId;
	}

	public void setFestivalId(int festivalId) 
	{
		this.festivalId = festivalId;
	}

	public int getArtistId() 
	{
		return artistId;
	}

	public void setArtistId(int artistId) 
	{
		this.artistId = artistId;
	}

	public int getStageId() 
	{
		return stageId;
	}

	public void setStageId(int stageId) 
	{
		this.stageId = stageId;
	}

	public int getDayId() 
	{
		return dayId;
	}

	public void setDayId(int dayId) 
	{
		this.dayId = dayId;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}