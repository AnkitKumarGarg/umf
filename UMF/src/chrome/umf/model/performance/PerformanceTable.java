package chrome.umf.model.performance;

import java.util.ArrayList;

import chrome.umf.model.artist.ArtistTable;
import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PerformanceTable implements ITable, IDebug 
{
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PERFORMANCE;

	public static final String TABLE_NAME = "tablePerformanceRecord";

	public static final String COL_AUTO_ID 			= "autoId";
	public static final String COL_ID 				= "ID";
	public static final String COL_NAME 			= "NAME";
	public static final String COL_DESCRIOTION 		= "DESCRIPTION";
	public static final String COL_START_DATE_TIME 	= "START_DATE_TIME";
	public static final String COL_END_DATE_TIME 	= "END_DATE_TIME";
	public static final String COL_FESTIVAL_ID 		= "FESTIVAL_ID";
	public static final String COL_ARTIST_ID	 	= "ARTIST_ID";
	public static final String COL_STAGE_ID 		= "STAGE_ID";
	public static final String COL_DAY_ID 			= "DAY_ID";

	public static ContentValues values = new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME+ " ( " 
										+ COL_AUTO_ID 			+ " INTEGER " + " primary key autoincrement,"
										+ COL_ID 				+ " INTEGER , " 
										+ COL_NAME 				+ " text , " 
										+ COL_DESCRIOTION		+ " text , " 
										+ COL_START_DATE_TIME 	+ " text , " 
										+ COL_END_DATE_TIME		+ " text , " 
										+ COL_FESTIVAL_ID 		+ " INTEGER , " 
										+ COL_ARTIST_ID			+ " INTEGER , " 
										+ COL_DAY_ID 			+ " INTEGER , " 
										+ COL_STAGE_ID			+ " INTEGER  " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public final static String SELECT_BY_ARTIST_ID = "select * from " + TABLE_NAME + " where " + COL_ARTIST_ID + " = ";

	public final static String SELECT_BY_ARTIST_ID_IS_PRESENT = "SELECT * FROM "+ TABLE_NAME
			+ " WHERE " + COL_ARTIST_ID	+ " IN (SELECT "+ ArtistTable.COL_ARTIST_ID	+ " FROM "+ ArtistTable.TABLE_NAME
			+ " ) ORDER BY " + COL_START_DATE_TIME + " , " + COL_END_DATE_TIME;

	public PerformanceTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		PerformanceRecord objPerformanceRecord = (PerformanceRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(PerformanceTable.COL_ID				,objPerformanceRecord.getId());
		values.put(PerformanceTable.COL_NAME			,objPerformanceRecord.getName());
		values.put(PerformanceTable.COL_DESCRIOTION		,objPerformanceRecord.getDescription());
		values.put(PerformanceTable.COL_START_DATE_TIME	,objPerformanceRecord.getStartDateTime());
		values.put(PerformanceTable.COL_END_DATE_TIME	,objPerformanceRecord.getEndDateTime());
		values.put(PerformanceTable.COL_FESTIVAL_ID		,objPerformanceRecord.getFestivalId());
		values.put(PerformanceTable.COL_ARTIST_ID		,objPerformanceRecord.getArtistId());
		values.put(PerformanceTable.COL_DAY_ID			,objPerformanceRecord.getDayId());
		values.put(PerformanceTable.COL_STAGE_ID		,objPerformanceRecord.getStageId());

		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId,SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + PerformanceTable.TABLE_NAME + " where rowId = " + rowId, null);
		cursor.moveToFirst();
		PerformanceRecord objPerformanceRecord = (PerformanceRecord) beanClassObject;

		objPerformanceRecord.setAutoID			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_AUTO_ID)));
		objPerformanceRecord.setId				(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ID)));
		objPerformanceRecord.setName			(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_NAME)));
		objPerformanceRecord.setDescription		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_DESCRIOTION)));
		objPerformanceRecord.setStartDateTime	(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_START_DATE_TIME)));
		objPerformanceRecord.setEndDateTime		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_END_DATE_TIME)));
		objPerformanceRecord.setFestivalId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_FESTIVAL_ID)));
		objPerformanceRecord.setArtistId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ARTIST_ID)));
		objPerformanceRecord.setStageId			(cursor.getInt		(cursor.getColumnIndex(PerformanceTable.COL_STAGE_ID)));
		objPerformanceRecord.setDayId			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_DAY_ID)));

		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objPerformanceRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + PerformanceTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PerformanceRecord objPerformanceRecord = new PerformanceRecord();

			objPerformanceRecord.setAutoID			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_AUTO_ID)));
			objPerformanceRecord.setId				(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ID)));
			objPerformanceRecord.setName			(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_NAME)));
			objPerformanceRecord.setDescription		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_DESCRIOTION)));
			objPerformanceRecord.setStartDateTime	(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_START_DATE_TIME)));
			objPerformanceRecord.setEndDateTime		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_END_DATE_TIME)));
			objPerformanceRecord.setFestivalId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_FESTIVAL_ID)));
			objPerformanceRecord.setArtistId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ARTIST_ID)));
			objPerformanceRecord.setStageId			(cursor.getInt		(cursor.getColumnIndex(PerformanceTable.COL_STAGE_ID)));
			objPerformanceRecord.setDayId			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_DAY_ID)));

			aryListOfRecord.add(objPerformanceRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition( SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PerformanceRecord objPerformanceRecord = new PerformanceRecord();

			objPerformanceRecord.setAutoID			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_AUTO_ID)));
			objPerformanceRecord.setId				(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ID)));
			objPerformanceRecord.setName			(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_NAME)));
			objPerformanceRecord.setDescription		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_DESCRIOTION)));
			objPerformanceRecord.setStartDateTime	(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_START_DATE_TIME)));
			objPerformanceRecord.setEndDateTime		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_END_DATE_TIME)));
			objPerformanceRecord.setFestivalId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_FESTIVAL_ID)));
			objPerformanceRecord.setArtistId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ARTIST_ID)));
			objPerformanceRecord.setStageId			(cursor.getInt		(cursor.getColumnIndex(PerformanceTable.COL_STAGE_ID)));
			objPerformanceRecord.setDayId			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_DAY_ID)));

			aryListOfRecord.add(objPerformanceRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database , String value) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("SELECT * FROM "	+ TABLE_NAME 
				+ " WHERE " + COL_START_DATE_TIME + " LIKE '%" + value + "%' AND "
				+ COL_ARTIST_ID	+ " IN (SELECT " + ArtistTable.COL_ARTIST_ID+ " FROM "+ ArtistTable.TABLE_NAME
				+ " ) ORDER BY " + COL_START_DATE_TIME + " , " + COL_END_DATE_TIME,null);
		
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PerformanceRecord objPerformanceRecord = new PerformanceRecord();

			objPerformanceRecord.setAutoID			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_AUTO_ID)));
			objPerformanceRecord.setId				(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ID)));
			objPerformanceRecord.setName			(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_NAME)));
			objPerformanceRecord.setDescription		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_DESCRIOTION)));
			objPerformanceRecord.setStartDateTime	(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_START_DATE_TIME)));
			objPerformanceRecord.setEndDateTime		(cursor.getString	(cursor	.getColumnIndex(PerformanceTable.COL_END_DATE_TIME)));
			objPerformanceRecord.setFestivalId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_FESTIVAL_ID)));
			objPerformanceRecord.setArtistId		(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_ARTIST_ID)));
			objPerformanceRecord.setStageId			(cursor.getInt		(cursor.getColumnIndex(PerformanceTable.COL_STAGE_ID)));
			objPerformanceRecord.setDayId			(cursor.getInt		(cursor	.getColumnIndex(PerformanceTable.COL_DAY_ID)));

			aryListOfRecord.add(objPerformanceRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow
}