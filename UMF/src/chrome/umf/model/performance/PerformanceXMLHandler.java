package chrome.umf.model.performance;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import android.util.Log;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class PerformanceXMLHandler extends ChromeDefaultHandler implements IDebug 
{

	private static final String TAGM_ROW 			= "row";
	private static final String TAG_ID 				= "Id";
	private static final String TAG_NAME 			= "Name";
	private static final String TAG_DESCRIPTION 	= "Description";
	private static final String TAG_START_DATE_TIME = "StartDateTime";
	private static final String TAG_END_DATE_TIME 	= "EndDateTime";
	private static final String TAG_FESTIVAL_ID 	= "FestivalId";
	private static final String TAG_ARTIST_ID 		= "ArtistId";
	private static final String TAG_STAGE_ID 		= "StageId";
	private static final String TAG_DAY_ID 			= "DayId";
	
	//use string buffer because of error in xml for StartDate
	private static StringBuffer strBuffForStartDate	=	new StringBuffer("");

	private static boolean isUpdateInDatabase;

	private static final String strUrl 				= IUrl.URL_PERFORMANCE;
	private static final ITable table 				= new PerformanceTable();

	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PERFORMANCE;
	private Debugger debugger;

	private boolean flag_ROW 				= false;
	private boolean flag_ID 				= false;
	private boolean flag_NAME 				= false;
	private boolean flag_DESCRIPTION 		= false;
	private boolean flag_START_DATE_TIME 	= false;
	private boolean flag_END_DATE_TIME 		= false;
	private boolean flag_FESTIVAL_ID 		= false;
	private boolean flag_ARTIST_ID 			= false;
	private boolean flag_STAGE_ID 			= false;
	private boolean flag_DAY_ID 			= false;
	
	private PerformanceRecord performanceRecord = null;
	private Database database 					= null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();

		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All ", " Start");
		debugger.logForAll(2, "ID		Name	Description ", " Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals(TAGM_ROW)) 
		{
			flag_ROW = true;
			performanceRecord = new PerformanceRecord();
		}

		else if (localName.equals(TAG_ID)) 
		{
			flag_ID = true;
		}
		
		else if (localName.equals(TAG_NAME)) 
		{
			flag_NAME = true;
		}
		
		else if (localName.equals(TAG_DESCRIPTION)) 
		{
			flag_DESCRIPTION = true;
		}
		
		else if (localName.equals(TAG_START_DATE_TIME)) 
		{
			Log.e("In BeginStart for startDate","buff length " + strBuffForStartDate.length());
			
			if(strBuffForStartDate.length()>0)
			{
				strBuffForStartDate.delete(0, strBuffForStartDate.length());
			}
			
			Log.e("In EndingStart for startDate","buff length " + strBuffForStartDate.length());
			
			flag_START_DATE_TIME = true;			
		} 
		
		else if (localName.equals(TAG_END_DATE_TIME)) 
		{
			flag_END_DATE_TIME = true;
		} 
		
		else if (localName.equals(TAG_FESTIVAL_ID)) 
		{
			flag_FESTIVAL_ID = true;
		}
		
		else if (localName.equals(TAG_ARTIST_ID)) 
		{
			flag_ARTIST_ID = true;
		}
		
		else if (localName.equals(TAG_STAGE_ID)) 
		{
			flag_STAGE_ID = true;
		}
		
		else if (localName.equals(TAG_DAY_ID)) 
		{
			flag_DAY_ID = true;
		}
	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_ID) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				performanceRecord.setId(Integer.parseInt(new String(ch, start,	length).trim()));
		}

		else if (flag_ARTIST_ID) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				performanceRecord.setArtistId(Integer.parseInt(new String(ch,start, length).trim()));
		}

		else if (flag_FESTIVAL_ID) 
		{
			performanceRecord.setFestivalId(Integer.parseInt(new String(ch,	start, length).trim()));
		}

		else if (flag_STAGE_ID) 
		{
			performanceRecord.setStageId(Integer.parseInt(new String(ch, start,	length).trim()));
		}

		else if (flag_DAY_ID) 
		{
			performanceRecord.setDayId(Integer.parseInt(new String(ch, start,length).trim()));
		}

		// Set value for string data
		else if (flag_NAME) 
		{
			performanceRecord.setName((new String(ch, start, length)).trim());
		}
		
		else if (flag_DESCRIPTION) 
		{
			performanceRecord.setDescription((new String(ch, start, length)).trim());
		}
		
		else if (flag_START_DATE_TIME) 
		{
			strBuffForStartDate.append(new String(ch, start, length));
			performanceRecord.setStartDateTime(strBuffForStartDate.toString().trim());
			Log.e("character " +(new String(ch, start, length)).trim()," ");
					
		}
		
		else if (flag_END_DATE_TIME) 
		{
			performanceRecord.setEndDateTime((new String(ch, start, length)).trim());
		}

	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals(TAG_ID)) 
		{
			flag_ID = false;
		}
		
		else if (localName.equals(TAG_NAME)) 
		{
			flag_NAME = false;
		}
		
		else if (localName.equals(TAG_DESCRIPTION)) 
		{
			flag_DESCRIPTION = false;
		}
		
		else if (localName.equals(TAG_START_DATE_TIME)) 
		{
			flag_START_DATE_TIME = false;Log.e("In end for start date"," ");
		}
		
		else if (localName.equals(TAG_END_DATE_TIME)) 
		{
			flag_END_DATE_TIME = false;
		} 
		
		else if (localName.equals(TAG_FESTIVAL_ID)) 
		{
			flag_FESTIVAL_ID = false;
		}
		
		else if (localName.equals(TAG_ARTIST_ID)) 
		{
			flag_ARTIST_ID = false;
		}
		
		else if (localName.equals(TAG_STAGE_ID)) 
		{
			flag_STAGE_ID = false;
		}
		
		else if (localName.equals(TAG_DAY_ID)) 
		{
			flag_DAY_ID = false;
		}

		else if (localName.equals(TAGM_ROW)) 
		{
			debugger.logForAll(	2,	"  \n\nR: ","  \nID:" 				+ performanceRecord.getId() 
												+ "  \nName:" 			+ performanceRecord.getName() 
												+ "  \nDescription:"	+ performanceRecord.getDescription()
												+ "  \nStartDateTime:"	+ performanceRecord.getStartDateTime()
												+ "  \nEnd DateTime:"	+ performanceRecord.getEndDateTime()
												+ "  \nFestivalId:"		+ performanceRecord.getFestivalId()
												+ "  \nArtistId:" 		+ performanceRecord.getArtistId()
												+ "  \nStageId:" 		+ performanceRecord.getStageId()
												+ "  \nDayId:" 			+ performanceRecord.getDayId());

			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(performanceRecord);
			}
			flag_ROW = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();

		if (isUpdateInDatabase) 
		{
			database.close();
		}

		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		PerformanceXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}

}// END ArtistXmlHandler