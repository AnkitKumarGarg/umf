package chrome.umf.model.places;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class PlacesRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PLACES;
	public static final PlacesTable PLACE_TABLE = new PlacesTable();

	private int autoID;

	private int 	id;
	private int 	festivalId;
	private int 	typeId;

	private String 	name;
	private String 	latitude;
	private String 	longitude;
	private String 	placeImageUrl;

	public PlacesTable getTableObject() 
	{
		return PLACE_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public int getFestivalId() 
	{
		return festivalId;
	}

	public void setFestivalId(int festivalId) 
	{
		this.festivalId = festivalId;
	}

	public int getTypeId() 
	{
		return typeId;
	}

	public void setTypeId(int typeId) 
	{
		this.typeId = typeId;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getLatitude() 
	{
		return latitude;
	}

	public void setLatitude(String latitude) 
	{
		this.latitude = latitude;
	}

	public String getLongitude() 
	{
		return longitude;
	}

	public void setLongitude(String longitude) 
	{
		this.longitude = longitude;
	}

	public String getPlaceImageUrl() 
	{
		return placeImageUrl;
	}

	public void setPlaceImageUrl(String placeImageUrl) 
	{
		this.placeImageUrl = placeImageUrl;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}