package chrome.umf.model.places;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PlacesTable implements ITable, IDebug 
{
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PLACES;

	public static final String TABLE_NAME 			= "tablePlacesRecord";
	public static final String COL_AUTO_ID 			= "autoId";
	public static final String COL_ID 				= "id";
	public static final String COL_NAME 			= "name";
	public static final String COL_FESTIVAL_ID 		= "festivalId";
	public static final String COL_TYPE_ID 			= "typeId";
	public static final String COL_LATITUDE 		= "latitude";
	public static final String COL_LONGITUDE 		= "longitude";
	public static final String COL_PLACE_IMAGE_URL 	= "placeImageUrl";
	
	public static ContentValues values = new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME + " ( " 
									+ COL_AUTO_ID 			+ " INTEGER " + " primary key autoincrement,"
									+ COL_ID 				+ " INTEGER , " 
									+ COL_NAME 				+ " text , " 
									+ COL_FESTIVAL_ID 		+ " INTEGER , " 
									+ COL_TYPE_ID 			+ " INTEGER , " 
									+ COL_LATITUDE			+ " text , " 
									+ COL_LONGITUDE 		+ " text , " 
									+ COL_PLACE_IMAGE_URL	+ " text  " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public PlacesTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		PlacesRecord objArtistRecord = (PlacesRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(PlacesTable.COL_ID				, objArtistRecord.getId());
		values.put(PlacesTable.COL_NAME				, objArtistRecord.getName());
		values.put(PlacesTable.COL_FESTIVAL_ID		, objArtistRecord.getFestivalId());
		values.put(PlacesTable.COL_TYPE_ID			, objArtistRecord.getTypeId());
		values.put(PlacesTable.COL_LATITUDE			, objArtistRecord.getLatitude());
		values.put(PlacesTable.COL_LONGITUDE		, objArtistRecord.getLongitude());
		values.put(PlacesTable.COL_PLACE_IMAGE_URL	, objArtistRecord.getPlaceImageUrl());
		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId, SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from "	+ PlacesTable.TABLE_NAME + " where rowId = " + rowId, null);
		cursor.moveToFirst();
		PlacesRecord objArtistRecord = (PlacesRecord) beanClassObject;

		objArtistRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_AUTO_ID)));
		objArtistRecord.setId			(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_ID)));
		objArtistRecord.setName			(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_NAME)));
		objArtistRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_FESTIVAL_ID)));
		objArtistRecord.setTypeId		(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_TYPE_ID)));
		objArtistRecord.setLatitude		(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_LATITUDE)));
		objArtistRecord.setLongitude	(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_LONGITUDE)));
		objArtistRecord.setPlaceImageUrl(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_PLACE_IMAGE_URL)));
		
		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objArtistRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + PlacesTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PlacesRecord objArtistRecord = new PlacesRecord();

			objArtistRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_AUTO_ID)));
			objArtistRecord.setId			(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_ID)));
			objArtistRecord.setName			(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_NAME)));
			objArtistRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_FESTIVAL_ID)));
			objArtistRecord.setTypeId		(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_TYPE_ID)));
			objArtistRecord.setLatitude		(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_LATITUDE)));
			objArtistRecord.setLongitude	(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_LONGITUDE)));
			objArtistRecord.setPlaceImageUrl(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_PLACE_IMAGE_URL)));
			
			aryListOfRecord.add(objArtistRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			PlacesRecord objArtistRecord = new PlacesRecord();

			objArtistRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_AUTO_ID)));
			objArtistRecord.setId			(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_ID)));
			objArtistRecord.setName			(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_NAME)));
			objArtistRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_FESTIVAL_ID)));
			objArtistRecord.setTypeId		(cursor.getInt		(cursor.getColumnIndex(PlacesTable.	COL_TYPE_ID)));
			objArtistRecord.setLatitude		(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_LATITUDE)));
			objArtistRecord.setLongitude	(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_LONGITUDE)));
			objArtistRecord.setPlaceImageUrl(cursor.getString	(cursor.getColumnIndex(PlacesTable.	COL_PLACE_IMAGE_URL)));
			
			aryListOfRecord.add(objArtistRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}