package chrome.umf.model.places;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class PlacesXMLHandler extends ChromeDefaultHandler implements IDebug 
{
	private static final String 	strUrl = IUrl.URL_PLACES;
	private static boolean 			isUpdateInDatabase;

	private static final ITable 	table = new PlacesTable();

	private static final boolean 	IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_PLACES;
	private Debugger debugger;

	private boolean flag_row 			= false;
	private boolean flag_Id 			= false;
	private boolean flag_name 			= false;
	private boolean flag_latitude 		= false;
	private boolean flag_typeId 		= false;
	private boolean flag_longitude	 	= false;
	private boolean flag_festivalId 	= false;
	private boolean flag_placeImageUrl 	= false;

	private PlacesRecord placeRecord 	= null;
	private Database database 			= null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();
		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All record ", " Start");
		debugger.logForAll(	2,	"ID	Name	Latitude	TypeId   Longitude	FestivalId   PlaceImageUrl "," Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals("row")) 
		{
			flag_row = true;
			placeRecord = new PlacesRecord();
		}

		else if (localName.equals("ID")) 
		{
			flag_Id = true;
		}
		
		else if (localName.equals("NAME")) 
		{
			flag_name = true;
		}
		
		else if (localName.equals("LATITUDE")) 
		{
			flag_latitude = true;
		}
		
		else if (localName.equals("TYPE_ID")) 
		{
			flag_typeId = true;
		}
		
		else if (localName.equals("LONGITUDE")) 
		{
			flag_longitude = true;
		}
		
		else if (localName.equals("FESTIVAL_ID")) 
		{
			flag_festivalId = true;
		}
		
		else if (localName.equals("PLACE_IMAGE_URL")) 
		{
			flag_placeImageUrl = true;
		}
	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_Id) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				placeRecord.setId(Integer.parseInt(new String(ch, start, length).trim()));
		}

		else if (flag_typeId) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				placeRecord.setTypeId(Integer.parseInt(new String(ch, start,length).trim()));
		}
		
		else if (flag_festivalId) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				placeRecord.setFestivalId(Integer.parseInt(new String(ch,start, length).trim()));
		}

		// Set value for string data
		else if (flag_name) 
		{
			placeRecord.setName((new String(ch, start, length)).trim());
		}
		
		else if (flag_latitude) 
		{
			placeRecord.setLatitude((new String(ch, start, length)).trim());
		}
		
		else if (flag_longitude) 
		{
			placeRecord.setLongitude((new String(ch, start, length)).trim());
		}
		
		else if (flag_placeImageUrl) 
		{
			placeRecord.setPlaceImageUrl((new String(ch, start, length)).trim());
		}

	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName)	throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals("ID")) 
		{
			flag_Id = false;
		}
		
		else if (localName.equals("NAME")) 
		{
			flag_name = false;
		}
		
		else if (localName.equals("LATITUDE")) 
		{
			flag_latitude = false;
		}
		
		else if (localName.equals("TYPE_ID")) 
		{
			flag_typeId = false;
		}
		
		else if (localName.equals("LONGITUDE")) 
		{
			flag_longitude = false;
		}
		
		else if (localName.equals("PLACE_IMAGE_URL")) 
		{
			flag_placeImageUrl = false;
		}
		
		else if (localName.equals("FESTIVAL_ID")) 
		{
			flag_festivalId = false;
		}

		else if (localName.equals("row")) 
		{
			debugger.logForAll(	2,	"R:-"," " 
								+ placeRecord.getId() 			+ "\t " 
								+ placeRecord.getName()			+ "\t " 
								+ placeRecord.getLatitude() 	+ "\t "
								+ placeRecord.getTypeId() 		+ "\t "
								+ placeRecord.getLongitude() 	+ "\t "
								+ placeRecord.getFestivalId() 	+ "\t "
								+ placeRecord.getPlaceImageUrl()+ "\t ");

			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(placeRecord);
			}

			flag_row = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();
		if (isUpdateInDatabase) 
		{
			database.close();
		}
		debugger.logForAll(2, "XMLHandler All record ", " End");
		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		PlacesXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}
}// END ArtistXmlHandler