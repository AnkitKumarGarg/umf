package chrome.umf.model.stage;

import chrome.utils.database.IRecords;
import chrome.utils.debug.IDebug;

public class StageRecord implements IRecords, IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_STAGE;
	public static final StageTable STAGE_TABLE = new StageTable();

	private int 	autoID;

	private int 	id;
	private int 	festivalId;
	private String 	description;

	private String 	name;
	private String 	latitude;
	private String 	longitude;
	private String 	mapImageUrl;

	public StageTable getTableObject() 
	{
		return STAGE_TABLE;
	}

	public int getAutoID() 
	{
		return autoID;
	}

	public void setAutoID(int autoID) 
	{
		this.autoID = autoID;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public int getFestivalId() 
	{
		return festivalId;
	}

	public void setFestivalId(int festivalId) 
	{
		this.festivalId = festivalId;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getLatitude() 
	{
		return latitude;
	}

	public void setLatitude(String latitude) 
	{
		this.latitude = latitude;
	}

	public String getLongitude() 
	{
		return longitude;
	}

	public void setLongitude(String longitude) 
	{
		this.longitude = longitude;
	}

	public String getMapImageUrl() 
	{
		return mapImageUrl;
	}

	public void setMapImageUrl(String mapImageUrl) 
	{
		this.mapImageUrl = mapImageUrl;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}