package chrome.umf.model.stage;

import java.util.ArrayList;

import chrome.utils.database.IRecords;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class StageTable implements ITable, IDebug {
	Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_STAGE;

	public static final String TABLE_NAME 		= "tableStageRecord";
	public static final String COL_AUTO_ID 		= "autoId";
	public static final String COL_ID 			= "id";
	public static final String COL_NAME 		= "name";
	public static final String COL_DESCRIPTION 	= "description";
	public static final String COL_LONGITUDE 	= "longitude";
	public static final String COL_LATITUDE 	= "latitude";
	public static final String COL_MAP_IMAGE_URL= "mapImageUrl";
	public static final String COL_FESTIVAL_ID 	= "festivalId";

	public static ContentValues values 			= new ContentValues();

	public final static String CREATE_TABLE = "create table " + TABLE_NAME	+ " ( " 
												+ COL_AUTO_ID 		+ " INTEGER " + " primary key autoincrement,"
												+ COL_ID 			+ " INTEGER , " 
												+ COL_NAME 			+ " text , " 
												+ COL_FESTIVAL_ID	+ " INTEGER , " 
												+ COL_DESCRIPTION 	+ " text , " 
												+ COL_LATITUDE		+ " text , " 
												+ COL_LONGITUDE 	+ " text , " 
												+ COL_MAP_IMAGE_URL	+ " text  " + " )";
	
	public final static String DROP_TABLE = "drop table " + TABLE_NAME;

	public StageTable() 
	{
		debugger = new Debugger(this);
	}// END Artist( )

	@Override
	public void insertIntoTable(IRecords BeanClassObject) 
	{
		StageRecord objArtistRecord = (StageRecord) BeanClassObject;
		debugger.logForInFunction();

		values.put(StageTable.COL_ID			, objArtistRecord.getId());
		values.put(StageTable.COL_NAME			, objArtistRecord.getName());
		values.put(StageTable.COL_FESTIVAL_ID	, objArtistRecord.getFestivalId());
		values.put(StageTable.COL_DESCRIPTION	, objArtistRecord.getDescription());
		values.put(StageTable.COL_LATITUDE		, objArtistRecord.getLatitude());
		values.put(StageTable.COL_LONGITUDE		, objArtistRecord.getLongitude());
		values.put(StageTable.COL_MAP_IMAGE_URL	, objArtistRecord.getMapImageUrl());
		debugger.logForOutFunction();
	}// END insertIntoTable()

	@Override
	public IRecords fetchObjForSingleRow(IRecords beanClassObject, int rowId,SQLiteDatabase database) 
	{
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from " + StageTable.TABLE_NAME + " where id = " + rowId, null);
		cursor.moveToFirst();
		StageRecord objArtistRecord = (StageRecord) beanClassObject;

		objArtistRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_AUTO_ID)));
		objArtistRecord.setId			(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_ID)));
		objArtistRecord.setName			(cursor.getString	(cursor.getColumnIndex(StageTable.COL_NAME)));
		objArtistRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_FESTIVAL_ID)));
		objArtistRecord.setDescription	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_DESCRIPTION)));
		objArtistRecord.setLatitude		(cursor.getString	(cursor.getColumnIndex(StageTable.COL_LATITUDE)));
		objArtistRecord.setLongitude	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_LONGITUDE)));
		objArtistRecord.setMapImageUrl	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_MAP_IMAGE_URL)));
		
		cursor.close();
		debugger.logForOutFunction();
		return (IRecords) objArtistRecord;
	}// END fetchObjForSinngleRow

	@Override
	public ArrayList<IRecords> fetchAllRow(SQLiteDatabase database) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery("select * from "+ StageTable.TABLE_NAME, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			StageRecord objArtistRecord = new StageRecord();

			objArtistRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_AUTO_ID)));
			objArtistRecord.setId			(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_ID)));
			objArtistRecord.setName			(cursor.getString	(cursor.getColumnIndex(StageTable.COL_NAME)));
			objArtistRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_FESTIVAL_ID)));
			objArtistRecord.setDescription	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_DESCRIPTION)));
			objArtistRecord.setLatitude		(cursor.getString	(cursor.getColumnIndex(StageTable.COL_LATITUDE)));
			objArtistRecord.setLongitude	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_LONGITUDE)));
			objArtistRecord.setMapImageUrl	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_MAP_IMAGE_URL)));
			
			aryListOfRecord.add(objArtistRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRow

	@Override
	public ArrayList<IRecords> fetchAllRowsWithCondition(SQLiteDatabase database, String strQuery) 
	{
		ArrayList<IRecords> aryListOfRecord = new ArrayList<IRecords>();
		debugger.logForInFunction();
		Cursor cursor = database.rawQuery(strQuery, null);
		cursor.moveToFirst();
		for (int i = 0; i < cursor.getCount(); i++) 
		{
			StageRecord objArtistRecord = new StageRecord();

			objArtistRecord.setAutoID		(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_AUTO_ID)));
			objArtistRecord.setId			(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_ID)));
			objArtistRecord.setName			(cursor.getString	(cursor.getColumnIndex(StageTable.COL_NAME)));
			objArtistRecord.setFestivalId	(cursor.getInt		(cursor.getColumnIndex(StageTable.COL_FESTIVAL_ID)));
			objArtistRecord.setDescription	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_DESCRIPTION)));
			objArtistRecord.setLatitude		(cursor.getString	(cursor.getColumnIndex(StageTable.COL_LATITUDE)));
			objArtistRecord.setLongitude	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_LONGITUDE)));
			objArtistRecord.setMapImageUrl	(cursor.getString	(cursor.getColumnIndex(StageTable.COL_MAP_IMAGE_URL)));
			
			aryListOfRecord.add(objArtistRecord);
			cursor.moveToNext();
		}
		cursor.close();
		debugger.logForOutFunction();
		return aryListOfRecord;
	}// fetchAllRowsWithCondition

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getTableName() 
	{
		return TABLE_NAME;
	}

	@Override
	public ContentValues getContentValues() 
	{
		return values;
	}

	@Override
	public String getDropTable() 
	{
		return DROP_TABLE;
	}

	@Override
	public String getCreateTable() 
	{
		return CREATE_TABLE;
	}

	@Override
	public boolean update(IRecords BeanClassObject, SQLiteDatabase database) 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database, String value) 
	{
		// TODO Auto-generated method stub
		return null;
	}
}