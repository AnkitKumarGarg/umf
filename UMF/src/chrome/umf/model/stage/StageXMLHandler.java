package chrome.umf.model.stage;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import chrome.umf.utils.IUrl;
import chrome.utils.database.Database;
import chrome.utils.database.ITable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;

public class StageXMLHandler extends ChromeDefaultHandler implements IDebug 
{
	private static boolean isUpdateInDatabase;
	private static final String strUrl = IUrl.URL_STAGE;

	private static final ITable table = new StageTable();

	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_MODEL_STAGE;
	private Debugger debugger;

	private boolean flag_row 			= false;
	private boolean flag_Id 			= false;
	private boolean flag_name 			= false;
	private boolean flag_latitude 		= false;
	private boolean flag_description 	= false;
	private boolean flag_longitude 		= false;
	private boolean flag_festivalId 	= false;
	private boolean flag_mapImageUrl 	= false;

	private StageRecord stageRecord = null;
	private Database database = null;

	@Override
	public void startDocument() throws SAXException 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		super.startDocument();
		if (isUpdateInDatabase) 
		{
			database = new Database();
			database.open();
		}
		debugger.logForAll("Start", "Document");
		debugger.logForAll(2, "XMLHandler All record ", " Start");
		debugger.logForAll(	2,"ID    Name    Latitude    Description   Longitude   FestivalId   MapImageUrl ",	" Start");
	}// END startDocument

	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		super.startElement(uri, localName, qName, attributes);
		debugger.logForAll("start element", "local name = " + localName);
		// debugger.logForAll(5, "XML Handler StartElement", localName);

		if (localName.equals("row")) 
		{
			flag_row = true;
			stageRecord = new StageRecord();
		}

		else if (localName.equals("ID")) 
		{
			flag_Id = true;
		}
		
		else if (localName.equals("NAME")) 
		{
			flag_name = true;
		}
		
		else if (localName.equals("LATITUDE")) 
		{
			flag_latitude = true;
		}
		
		else if (localName.equals("DESCRIPTION")) 
		{
			flag_description = true;
		}
		
		else if (localName.equals("LONGITUDE")) 
		{
			flag_longitude = true;
		}
		
		else if (localName.equals("FESTIVAL_ID")) 
		{
			flag_festivalId = true;
		} 
		
		else if (localName.equals("MAP_IMAGE_URL")) 
		{
			flag_mapImageUrl = true;
		}
	}// END startElement

	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
		super.characters(ch, start, length);
		debugger.logForAll("start element", "character name = "	+ new String(ch, start, length).trim());
		// Set value for integer data
		if (flag_Id) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				stageRecord.setId(Integer.parseInt(new String(ch, start, length).trim()));
		}

		else if (flag_festivalId) 
		{
			if (new String(ch, start, length).trim().length() > 0)
				stageRecord.setFestivalId(Integer.parseInt(new String(ch,start, length).trim()));
		}

		// Set value for string data
		else if (flag_description) 
		{
			stageRecord.setLatitude((new String(ch, start, length)).trim());
		}
		
		else if (flag_name) 
		{
			stageRecord.setName((new String(ch, start, length)).trim());
		}
		
		else if (flag_latitude) 
		{
			stageRecord.setLatitude((new String(ch, start, length)).trim());
		}
		
		else if (flag_longitude) 
		{
			stageRecord.setLongitude((new String(ch, start, length)).trim());
		} 
		
		else if (flag_mapImageUrl) 
		{
			stageRecord.setMapImageUrl((new String(ch, start, length)).trim());
		}

	}// END characters

	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		super.endElement(uri, localName, qName);
		debugger.logForAll("end element", "local name" + localName);

		if (localName.equals("ID")) 
		{
			flag_Id = false;
		}
		
		else if (localName.equals("NAME")) 
		{
			flag_name = false;
		}
		
		else if (localName.equals("LATITUDE")) 
		{
			flag_latitude = false;
		}
		
		else if (localName.equals("DESCRIPTION")) 
		{
			flag_description = false;
		}
		
		else if (localName.equals("LONGITUDE")) 
		{
			flag_longitude = false;
		}
		
		else if (localName.equals("MAP_IMAGE_URL")) 
		{
			flag_mapImageUrl = false;
		}
		
		else if (localName.equals("FESTIVAL_ID")) 
		{
			flag_festivalId = false;
		}

		else if (localName.equals("row")) 
		{
			debugger.logForAll(	2,	"R:-"," " 	+ stageRecord.getId() 			+ "\t " 
												+ stageRecord.getName()			+ "\t " 
												+ stageRecord.getLatitude() 	+ "\t "
												+ stageRecord.getDescription() 	+ "\t "
												+ stageRecord.getLongitude() 	+ "\t "
												+ stageRecord.getFestivalId() 	+ "\t "
												+ stageRecord.getMapImageUrl() 	+ "\t ");

			if (isUpdateInDatabase) 
			{
				// Insert data into table
				database.insertRowIntoTable(stageRecord);
			}

			flag_row = false;
		}// end if

	}// END endElement

	@Override
	public void endDocument() throws SAXException 
	{
		super.endDocument();
		if (isUpdateInDatabase) 
		{
			database.close();
		}

		debugger.logForInFunction();
	}// END endDocument

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

	@Override
	public String getUrl() 
	{
		return strUrl;
	}

	@Override
	public ITable getTable() 
	{
		return table;
	}

	@Override
	public boolean isUpdateInDatabase() 
	{
		return isUpdateInDatabase;
	}

	@Override
	public void setUpdateInDatabase(boolean isUpdateInDatabase) 
	{
		StageXMLHandler.isUpdateInDatabase = isUpdateInDatabase;
	}

}// END ArtistXmlHandler