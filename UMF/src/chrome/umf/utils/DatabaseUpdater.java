package chrome.umf.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import chrome.umf.model.artist.ArtistXMLHandler;
import chrome.umf.model.day.DayXMLHandler;
import chrome.umf.model.festival.FestivalXMLHandler;
import chrome.umf.model.festivalVideo.FestivalVideoXMLHandler;
import chrome.umf.model.lookUpCountry.LookUpCountryXMLHandler;
import chrome.umf.model.party.PartyXMLHandler;
import chrome.umf.model.performance.PerformanceXMLHandler;
import chrome.umf.model.places.PlacesXMLHandler;
import chrome.umf.model.stage.StageXMLHandler;
import chrome.umf.utils.ETables;
import chrome.umf.utils.IConstants;
import chrome.utils.database.Database;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.ChromeDefaultHandler;
import chrome.utils.xml.Parse;

public final class DatabaseUpdater extends AsyncTask<Void, Integer, Void> implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_UPDATE_DATBASE;
	private Debugger debugger;

	private boolean 			isUpdate;
	private final Activity 		parent;
	private final ProgressBar 	progress;
	private final boolean 		isUpdateInDatabaseIfTableHasRows;
	private ETables 			table;

	public DatabaseUpdater(final Activity parent, final ProgressBar progress, 
								final boolean isUpdateInDatabaseIfTableHasRows, ETables table) 
	{
		Database.context = parent;
		debugger = new Debugger(this);
		debugger.logForInFunction();
		this.parent = parent;
		this.progress = progress;
		this.isUpdateInDatabaseIfTableHasRows = isUpdateInDatabaseIfTableHasRows;
		this.table = table;
	}// end UpdateXml( , , )

	@Override
	protected void onPreExecute() 
	{
		debugger.logForInFunction();
		if(progress != null)
		{		
			progress.setVisibility(View.VISIBLE);
		}
	}// END onPreExecute

	
	private static void setUpdationFlagAndParse(ChromeDefaultHandler defaultHandler,boolean isUpdateInDatabaseIfTableHasRows)
	{
		Database database = new Database();
		database.open();
		boolean isTableHasRows = database.isTableHasRows(defaultHandler.getTable().getTableName());
		database.close();
		Log.e("Table has rows "," "+ isTableHasRows);
		Log.e("is Update in database "," "+ isUpdateInDatabaseIfTableHasRows);
		if (isUpdateInDatabaseIfTableHasRows || (! isTableHasRows)) 
		{
			defaultHandler.setUpdateInDatabase(isUpdateInDatabaseIfTableHasRows || (! isTableHasRows));
			database.open();
			database.updateTable(defaultHandler.getTable());
			database.close();
			Parse.parse(defaultHandler);
		}
	}
	
	@Override
	protected Void doInBackground(final Void... params) 
	{
		debugger.logForInFunction();
		switch (table) 
		{
		case ARTIST			: 	setUpdationFlagAndParse(new ArtistXMLHandler()			, isUpdateInDatabaseIfTableHasRows); 		
								break;
		case PLACES			:	setUpdationFlagAndParse(new PlacesXMLHandler()			, isUpdateInDatabaseIfTableHasRows);		
								break;
		case STAGE			:	setUpdationFlagAndParse(new StageXMLHandler()			, isUpdateInDatabaseIfTableHasRows);			
								break;
		case DAY			:	setUpdationFlagAndParse(new DayXMLHandler()				, isUpdateInDatabaseIfTableHasRows);		
								break;
		case LOOK_UP_COUNTRY:	setUpdationFlagAndParse(new LookUpCountryXMLHandler()	, isUpdateInDatabaseIfTableHasRows);	
								break;
		case FESTIVAL		:	setUpdationFlagAndParse(new FestivalXMLHandler()		, isUpdateInDatabaseIfTableHasRows);	
								break;
		case FESTIVAL_VEDIO	:	setUpdationFlagAndParse(new FestivalVideoXMLHandler()	, isUpdateInDatabaseIfTableHasRows);	
								break;
		case PARTY			:	setUpdationFlagAndParse(new PartyXMLHandler()			, isUpdateInDatabaseIfTableHasRows);			
								break;
		case PERFORMANCE	:	setUpdationFlagAndParse(new PerformanceXMLHandler()		, isUpdateInDatabaseIfTableHasRows);
								break;

		case ALL:
			setUpdationFlagAndParse(new ArtistXMLHandler()			, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new DayXMLHandler()				, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new FestivalXMLHandler()		, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new FestivalVideoXMLHandler()	, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new LookUpCountryXMLHandler()	, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new PartyXMLHandler()			, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new PerformanceXMLHandler()		, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new PlacesXMLHandler()			, isUpdateInDatabaseIfTableHasRows);
			setUpdationFlagAndParse(new StageXMLHandler()			, isUpdateInDatabaseIfTableHasRows);
			break;

		}
		return null;
	}// END doInBackground

	@Override
	protected void onProgressUpdate(final Integer... values) 
	{

	}// END onProgressUpdate

	@Override
	protected void onPostExecute(final Void result) 
	{
		if(progress != null)
		{
			parent.startActivity(new Intent(parent, chrome.umf.controller.main.MenuActivity.class));
			parent.finish();
		}

	}// END onPostExecute

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}// END UpdateXml