package chrome.umf.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateOperations 
{
	public static String getdateInSpecificString(Date date) 
	{
		String day = null;
		String month = null;
		int intDate;
		int year;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		switch (calendar.get(Calendar.DAY_OF_WEEK)) 
		{
			case Calendar.SUNDAY	:  	day = "Sunday"	;	break;
			case Calendar.MONDAY	:	day = "Monday"	;	break;
			case Calendar.TUESDAY	:	day = "Tuesday"	;	break;
			case Calendar.WEDNESDAY	:	day = "Wednesday";	break;
			case Calendar.THURSDAY	:	day = "Thursday";	break;
			case Calendar.FRIDAY	:	day = "Friday"	;	break;
			case Calendar.SATURDAY	:	day = "Saturday";	break;
		}

		switch (calendar.get(Calendar.MONTH)) 
		{
			case Calendar.JANUARY	:	month = "January"	;	break;
			case Calendar.FEBRUARY	:	month = "February"	;	break;
			case Calendar.MARCH		:	month = "March"		;	break;
			case Calendar.APRIL		:	month = "April"		;	break;
			case Calendar.MAY		:	month = "May"		;	break;
			case Calendar.JUNE		:	month = "June"		;	break;
			case Calendar.JULY		:	month = "July"		;	break;
			case Calendar.AUGUST	:	month = "August"	;	break;
			case Calendar.SEPTEMBER	:	month = "September"	;	break;
			case Calendar.OCTOBER	:	month = "October"	;	break;
			case Calendar.NOVEMBER	:	month = "November"	;	break;
			case Calendar.DECEMBER	:	month = "December"	;	break;
		}

		intDate = calendar.get(Calendar.DATE);
		year = calendar.get(Calendar.YEAR);

		String returnString = "" + day + ", " + month + " " + intDate + " ("	+ year + ")";
		return returnString;
	}
	
	public static String getTimeInHourAndMins(String strTime , String strdateFormat) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat(strdateFormat);
		String strHour 	= 	"";
		String strMins	=	"";
		try 
		{
			java.util.Date dateTime	=	dateFormat.parse(strTime);
			int hours = dateTime.getHours();
			int mins	=	dateTime.getMinutes();
			if(hours>12)
			{
				hours	=	hours 	-	12 ;
			}
			if(hours < 10)
			{
				strHour	=	"0" + 	hours;
			}
			else
			{
				strHour	=	"" +	hours;
			}
			
			if( mins < 10)
			{
				strMins	=	"0" + 	mins;
			}
			else
			{
				strMins	=	"" +	mins;
			}
			return "" + strHour + ":" + strMins ;
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
			return "";
		}
	}	
}