package chrome.umf.utils;

public interface IConstants 
{
	public static final int 	NO_OF_CONNECTIONS 								= 3;
	public static final int 	WAITING_TIME_IF_CONNECTION_NOT_FREE_IN_SECONDS 	= 2;
	public static final int 	TARGET_DATE 									= 15;
	public static final int 	TARTGET_MONTH 									= 4;
	public static final int 	TARGET_YEAR 									= 2013;
	public static final int 	CUSTOM_TITLE_BAR_ID 							= 10001;
	public static final int 	DEFAULT_ARTIST_ID 								= 1000;
	public static final String 	ASSETS_PATH_FOR_ARTIST_SMALL_IMAGE 				= "artistsimages/artistsmall/" ;
	public static final String 	ASSETS_PATH_FOR_ARTIST_NAME_IMAGE 				= "artistsimages/artistname/" ;
	public static final String 	TIME_FORMATE_FOR_PERFORMANCE_TABLE 				= "yyyy-MM-dd HH:mm" ;
	public static final String 	START_WORD_FOR_COUNTRY_NAME_IN_IMAGE 			= "country" ;
	public static final int 	INT_STAR_EMPTY 									= 0 ;
	public static final int 	INT_STAR_HALF 									= 50 ;
	public static final int 	INT_STAR_FULL 									= 100 ;
	public static final String 	ASSETS_EXTENSION_FOR_ARTIST_SMALL_IMAGE 		= ".jpg" ;
	public static final String 	ASSETS_ETENSION_FOR_ARTIST_NAME_IMAGE 			= ".gif" ;
}
