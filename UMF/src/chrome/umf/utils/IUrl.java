package chrome.umf.utils;

public interface IUrl {
	public static final String URL_ARTIST 			= "http://digdave.s155.eatj.com/Test/UMF/artist.html";
	public static final String URL_PLACES 			= "http://digdave.s155.eatj.com/Test/UMF/places.xml";
	public static final String URL_STAGE 			= "http://digdave.s155.eatj.com/Test/UMF/stage.xml";
	public static final String URL_DAY 				= "http://digdave.s155.eatj.com/Test/UMF/day.xml";
	public static final String URL_LOOK_UP_COUNTRY 	= "http://digdave.s155.eatj.com/Test/UMF/lookupcountry.xml";
	public static final String URL_FESTIVAL 		= "http://digdave.s155.eatj.com/Test/UMF/festival.xml";
	public static final String URL_FESTIVAL_VIDEO 	= "http://digdave.s155.eatj.com/Test/UMF/festivalvideo.xml";
	public static final String URL_PARTY 			= "http://digdave.s155.eatj.com/Test/UMF/party.xml";
	public static final String URL_PERFORMANCE 		= "http://digdave.s155.eatj.com/Test/UMF/performance.xml";
}
