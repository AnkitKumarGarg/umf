package chrome.umf.utils;

import chrome.umf.activities.R;
import chrome.umf.model.artist.ArtistRecord;
import chrome.utils.database.Database;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

public class StarUpdater extends AsyncTask <Void, Integer, Void>
{
	private ImageView 		imageViewStar;
	private ArtistRecord	artistRecord;
	private ListView 		listView;
	private int 			position;
	private Activity 		activity = new Activity();
	
	public StarUpdater(ImageView imageViewStar ,ArtistRecord artistRecord) 
	{
		this.imageViewStar	=	imageViewStar;
		this.artistRecord	=	artistRecord;
	}

	public StarUpdater(int imageViewStarRId ,ArtistRecord artistRecord, ListView listView , int position) 
	{
		Activity activity;
		
		
		View v				= 	listView.getChildAt(position - listView.getFirstVisiblePosition());
		
		this.imageViewStar 	= 	(ImageView) v.findViewById(imageViewStarRId);
		this.artistRecord	=	artistRecord;
		this.listView		=	listView;
		this.position		=	position;
	}

	

	@Override
	protected Void doInBackground(Void... params) 
	{
		
		switch(artistRecord.getStarPerformance())
		{
		case  EMPTY:  	artistRecord.setStarPerformance(EStarPerformanceRate.HALF);
						break;
		case  HALF:		artistRecord.setStarPerformance(EStarPerformanceRate.FULL);
						break;
		case  FULL:		artistRecord.setStarPerformance(EStarPerformanceRate.EMPTY);
						break;
		}
		Database database1 = new Database();
		database1.open();
		database1.updateInDatabase(artistRecord);
		database1.close();
		return null;
	}

	
	@Override
	protected void onPostExecute(Void result) 
	{
		
		super.onPostExecute(result);
		switch(artistRecord.getStarPerformance())
		{
		case  EMPTY:  	imageViewStar.setImageResource(R.drawable.starempty);
						break;
		case  HALF:		imageViewStar.setImageResource(R.drawable.starhalf);
						break;
		case  FULL:		imageViewStar.setImageResource(R.drawable.starfull);
						break;
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... values) 
	{
		super.onProgressUpdate(values);
	}

}
