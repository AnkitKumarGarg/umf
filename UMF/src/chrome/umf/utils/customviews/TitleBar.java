package chrome.umf.utils.customviews;

import chrome.umf.activities.R;
import chrome.umf.utils.IConstants;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TitleBar extends RelativeLayout implements IDebug 
{
	Debugger debugger;

	public TitleBar(Context context) 
	{
		super(context);
		final RelativeLayout.LayoutParams paramsLayout = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsLayout.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsLayout.addRule(RelativeLayout.CENTER_VERTICAL);
		this.setLayoutParams(paramsLayout);
	}// END TiltleBar( , )

	public TitleBar(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		final RelativeLayout.LayoutParams paramsLayout = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		paramsLayout.addRule(RelativeLayout.CENTER_HORIZONTAL);
		paramsLayout.addRule(RelativeLayout.CENTER_VERTICAL);
		this.setLayoutParams(paramsLayout);
	}// END TiltleBar( , , )

	// Constructor which called in starting
	public TitleBar(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		debugger = new Debugger(this);

		// Add text view dynamically
		TextView textView = new TextView(context);
		textView.setId(IConstants.CUSTOM_TITLE_BAR_ID);
		// Set properties for dynamically generated textview
		final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		params.addRule(RelativeLayout.CENTER_VERTICAL);

		textView.setLayoutParams(params);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		textView.setTypeface(null, Typeface.BOLD);

		// Fetch array of properties defined in attrs.xml for respective
		// declared custom_title_bar
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.custom_title_bar);

		// Get property value which is declared layout Xml of respective
		// activity
		CharSequence s = a.getString(R.styleable.custom_title_bar_text);

		debugger.logForAll("Sequence",
				"demo " + a.getString(R.styleable.custom_title_bar_text)
						+ "stop");

		if (s != null) {
			textView.setText(s);
		}

		this.addView(textView, params);
		// Should recycle after use TypedArray
		a.recycle();

		this.setBackgroundResource(R.drawable.backgroundtop);
	}

	@Override
	public boolean isDebugable() 
	{
		// TODO Auto-generated method stub
		return false;
	}
}