package chrome.utils.commonFunctions;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

public class FileOperations implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_FILE_OPERATION;
	private Debugger debugger = null;

	public FileOperations() 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
	}// END FileOperations

	public static String getStringFromUrlByHttpClient(String strUrl) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);

		String xml = null;

		try 
		{
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(strUrl);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			xml = EntityUtils.toString(httpEntity);

		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}// end catch

		// return XML in string form
		return xml.trim();
	}// END getStrinfFromUrlByHttpClient

	public static InputSource getInputSourceFromString(String string) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		return new InputSource(new ByteArrayInputStream(string.getBytes()));
	}// end getInputSourceFromString

	public static String getStringFromUrlByBuffer(URL url) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		StringBuffer finalString = new StringBuffer("");
		try 
		{
			BufferedReader in = new BufferedReader(new InputStreamReader( url.openStream()));
			String str = "";
			while ((str = in.readLine()) != null) 
			{
				finalString.append(str);
				Debugger.staticlogForAll(IS_DEBUGABLE, "String", "" + str);
			}
			in.close();

			Debugger.staticlogForAll(IS_DEBUGABLE, "String", "" + finalString.toString());

		}
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}// end catch

		// return xml in string
		return finalString.toString();
	}// end getStringFromUrlByBuffer

	public String getFileNameFromUrl(URL url) 
	{
		debugger.logForInFunction();
		String strFullPathOfFile = url.getFile();
		int numPosOfLastDot = strFullPathOfFile.lastIndexOf("/");
		String strActualNameOfFile = strFullPathOfFile.substring( numPosOfLastDot + 1, strFullPathOfFile.length());
		return strActualNameOfFile;
	}// end getFileNameFromUrl

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}// END FileOperation