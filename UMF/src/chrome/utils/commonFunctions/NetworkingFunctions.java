package chrome.utils.commonFunctions;

import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkingFunctions implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_NETWORKING_FUNCTION;
	Debugger debugger;

	public NetworkingFunctions() 
	{
		debugger.logForInFunction();
	}// END NetworkingFunctions( )

	public static boolean isInternetConnection(Context context) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		ConnectivityManager connectivityManager = (ConnectivityManager) context .getSystemService(Context.CONNECTIVITY_SERVICE);
		return connectivityManager.getActiveNetworkInfo() .isConnectedOrConnecting();
	}// end isInternetConnection

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}

}// END NetworkingFunctions
