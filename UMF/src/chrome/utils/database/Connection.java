package chrome.utils.database;

import java.util.Date;

import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Connection implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_CONNECTION;
	private Debugger debugger;

	private int id;
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase databaseInstance;
	private boolean isFree;
	private Date createDate;
	private Date assignDate;
	private Date lastModifiedDate;

	public Connection(Context context, int id) 
	{
		debugger = new Debugger(this);
		debugger.logForAll(1,"New Conection", " id : " + id);

		setId(id);
		setDatabaseHelper(context);
		setDatabaseInstance();
	}

	public SQLiteDatabase getDatabaseInstance() 
	{
		return databaseInstance;
	}

	private void setDatabaseInstance() 
	{
		databaseInstance = databaseHelper.getWritableDatabase();
	}

	public int getId() 
	{
		return id;
	}

	private void setId(int id) 
	{
		this.id = id;
	}

	private void setDatabaseHelper(Context context) 
	{
		databaseHelper = new DatabaseHelper(context);
	}

	public Date getCreateDate() 
	{
		return createDate;
	}

	public void setCreateDate(Date createDate) 
	{
		this.createDate = createDate;
	}

	public boolean isFree() 
	{
		return isFree;
	}

	public void setFree(boolean isFree) 
	{
		this.isFree = isFree;
	}

	public Date getAssignDate() 
	{
		return assignDate;
	}

	public void setAssignDate(Date assignDate) 
	{
		this.assignDate = assignDate;
	}

	public Date getLastModifiedDate() 
	{
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) 
	{
		this.lastModifiedDate = lastModifiedDate;
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}