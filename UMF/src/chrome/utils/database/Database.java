package chrome.utils.database;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import chrome.umf.model.artist.ArtistTable;
import chrome.umf.utils.IConstants;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

public class Database implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_DATABASE;
	private Debugger debugger;
	private SQLiteDatabase databaseInstance = null;
	private static Connection[] aryListConnecton = null;
	private static int databaseInstanceId = -1;
	public static Context context = null;

	static 
	{
		aryListConnecton = new Connection[IConstants.NO_OF_CONNECTIONS];
	}

	public Database() 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		debugger.logForOutFunction();
	}// END Database( )

	public SQLiteDatabase open() 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id :", "" + databaseInstanceId);

		if (databaseInstance != null) 
		{
			debugger.logForAll(1, "Exception","You are try to open pre opened connnection");
		} 
		else 
		{
			databaseInstanceId = getNewConnectionId(context);
			databaseInstance = aryListConnecton[databaseInstanceId].getDatabaseInstance();
		}// end if
		debugger.logForOutFunction();
		return databaseInstance;
	}// END open

	public void close() {
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		if (databaseInstance == null) 
		{
			debugger.logForAll(1, "Excepion",
					"You are try to close a pre closed connection");
		}
		else 
		{
			aryListConnecton[databaseInstanceId].setFree(true);
			databaseInstanceId = -1;
			databaseInstance = null;
		}// end if
		debugger.logForOutFunction();
	}// END close

	public int getNewConnectionId(Context context) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		boolean isConnectionGet = false;
		for (int i = 0; i < aryListConnecton.length; i++) 
		{
			if (aryListConnecton[i] == null) 
			{
				aryListConnecton[i] = new Connection(context, i);
				databaseInstanceId = i;
				aryListConnecton[i].setFree(false);
				isConnectionGet = true;
				break;
			}
			else if (aryListConnecton[i].isFree() == true) 
			{
				databaseInstanceId = i;
				aryListConnecton[i].setFree(false);
				isConnectionGet = true;
				break;
			}// end if
		}// end for

		if (isConnectionGet == false) 
		{
			try 
			{
				wait(2000);
				getNewConnectionId(context);
			} 
			catch (InterruptedException e) 
			{
				debugger.logForAll(1, "Exception", "in wait for get connection"+ e);
			}// end try
		}// end if
		return databaseInstanceId;
	}// END getNewConnectionId

	public boolean insertRowIntoTable(IRecords beanClassObject) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		ITable tableClassObject = beanClassObject.getTableObject();
		tableClassObject.insertIntoTable(beanClassObject);
		long rowID = databaseInstance.insert(tableClassObject.getTableName(), null, tableClassObject.getContentValues());
		if (rowID < 0) 
		{
			debugger.logForAll(1, "Error Occour For Insert Record","Table Name" + tableClassObject.getTableName());
			debugger.logForOutFunction();
			return false;
		} 
		else 
		{
			debugger.logForOutFunction();
			return true;
		}// end if
	}// END insertRowIntoTable

	public IRecords fetchRowForTable(IRecords beanClassObject, int rowId) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		ITable tableClassObject = beanClassObject.getTableObject();
		tableClassObject.insertIntoTable(beanClassObject);
		beanClassObject = tableClassObject.fetchObjForSingleRow( beanClassObject, rowId, databaseInstance);
		debugger.logForOutFunction();
		return beanClassObject;
	}// END fetchRowForTable

	public ArrayList<IRecords> fetchAllRows(IRecords objRecord) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		ArrayList<IRecords> aryArtistRecords;
		ITable tableClassObject = objRecord.getTableObject();
		aryArtistRecords = tableClassObject.fetchAllRow(databaseInstance);
		debugger.logForOutFunction();
		return aryArtistRecords;
	}// END fetchAllRows

	
	public ArrayList<IRecords> fetchManyRowsByGivenValue(IRecords beanClassObject, String value) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		ArrayList<IRecords> aryArtistRecords;
		ITable tableClassObject = beanClassObject.getTableObject();
		aryArtistRecords = tableClassObject.fetchManyRowsByGivenValue(databaseInstance,value);
		debugger.logForOutFunction();
		return aryArtistRecords;
	}// END fetchRowForTable

	public ArrayList<IRecords> fetchAllRowsByQuery(IRecords objRecord,String strQuery) 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		ArrayList<IRecords> aryArtistRecords;
		ITable tableClassObject = objRecord.getTableObject();
		aryArtistRecords = tableClassObject.fetchAllRowsWithCondition(databaseInstance, strQuery);
		debugger.logForOutFunction();
		return aryArtistRecords;
	}// END fetchAllRowsByQuery

	public int countTables() 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		int count = 0;
		String SQL_GET_ALL_TABLES = "SELECT count(*) FROM sqlite_master "
				+ "WHERE type = 'table' " + "AND name != 'android_metadata' "
				+ "AND name != 'sqlite_sequence'";
		open();
		Cursor cursor = databaseInstance.rawQuery(SQL_GET_ALL_TABLES, null);
		cursor.moveToFirst();
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) 
		{
			count++;
		}// end for
		cursor.close();
		close();
		return count;
	}// END countTables

	public boolean deleteDatabase() 
	{
		debugger.logForInFunction();
		debugger.logForAll("Connecton Id : ", "" + databaseInstanceId);

		return context.getApplicationContext().deleteDatabase( DatabaseHelper.DATABASE_NAME);
	}// END upgradeDatabase

	public boolean updateTable(ITable table) 
	{
		debugger.logForInFunction();
		try 
		{
			if (isTableExists(table.getTableName())) 
			{
				databaseInstance.execSQL(table.getDropTable());
				debugger.logForAll("Table " + table.getTableName()
						+ " Exist and Drop", "SuccessFully");
			}
			databaseInstance.execSQL(table.getCreateTable());
			debugger.logForAll("Table " + table.getTableName() + " Create",
					"SuccessFully");
			return true;
		} 
		catch (Exception e) 
		{
			Log.e( "Exception occour in ",
					"in updation for table : " + table.getTableName());
			return false;
		}
	}

	public boolean isTableExists(String tableName) 
	{
		Cursor cursor = databaseInstance.rawQuery(
				"select DISTINCT tbl_name from sqlite_master "
						+ "where tbl_name = '" + tableName + "'", null);
		
		if (cursor != null) 
		{
			cursor.moveToFirst();
			if (cursor.getCount() > 0) 
			{
				cursor.close();
				return true;
			}
			cursor.close();
		}
		return false;
	}

	public boolean isTableHasRows(String tableName) 
	{
		Cursor cursor = databaseInstance.rawQuery(
				"select * from " + tableName + "", null);
		
		if (cursor != null) 
		{
			cursor.moveToFirst();
			if (cursor.getCount() > 0) 
			{
				cursor.close();
				return true;
			}
			cursor.close();
		}
		return false;
	}
	
	public boolean updateInDatabase(IRecords objRecord) 
	{
		ITable tableClassObject = objRecord.getTableObject();

		return tableClassObject.update(objRecord, databaseInstance);
	}
	
	

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}// END Database