package chrome.utils.database;

import chrome.umf.model.artist.ArtistTable;
import chrome.umf.model.artist.ArtistXMLHandler;
import chrome.umf.model.day.DayTable;
import chrome.umf.model.day.DayXMLHandler;
import chrome.umf.model.festival.FestivalTable;
import chrome.umf.model.festival.FestivalXMLHandler;
import chrome.umf.model.festivalVideo.FestivalVideoTable;
import chrome.umf.model.festivalVideo.FestivalVideoXMLHandler;
import chrome.umf.model.lookUpCountry.LookUpCountryTable;
import chrome.umf.model.party.PartyTable;
import chrome.umf.model.performance.PerformanceTable;
import chrome.umf.model.places.PlacesTable;
import chrome.umf.model.stage.StageTable;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;
import chrome.utils.xml.Parse;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper implements IDebug 
{
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_DATABASE_HELPER;
	private Debugger debugger;

	public final static String DATABASE_NAME = "UMFEssential";
	public final static int VERSION = 38;

	public DatabaseHelper(Context context) 
	{
		super(context.getApplicationContext(), DATABASE_NAME, null, VERSION);
		debugger = new Debugger(this);
		debugger.logForInFunction();
	}// END DatabaseHelper( )

	public DatabaseHelper(Context context, String DATABASE_NAME, CursorFactory factory, int VERSION) 
	{
		super(context, DATABASE_NAME, factory, VERSION);
		debugger.logForInFunction();
	}// END DatabaseHelper( , , , )

	@Override
	public void onCreate(SQLiteDatabase database) 
	{
		debugger.logForInFunction();

		database.execSQL(ArtistTable.CREATE_TABLE);
		database.execSQL(DayTable.CREATE_TABLE);
		database.execSQL(FestivalTable.CREATE_TABLE);
		database.execSQL(FestivalVideoTable.CREATE_TABLE);
		database.execSQL(LookUpCountryTable.CREATE_TABLE);
		database.execSQL(PartyTable.CREATE_TABLE);
		database.execSQL(PerformanceTable.CREATE_TABLE);
		database.execSQL(PlacesTable.CREATE_TABLE);
		database.execSQL(StageTable.CREATE_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) 
	{
		debugger.logForInFunction();
		database.execSQL(ArtistTable.DROP_TABLE);
		database.execSQL(ArtistTable.CREATE_TABLE);
		database.execSQL(DayTable.DROP_TABLE);
		database.execSQL(DayTable.CREATE_TABLE);
		database.execSQL(FestivalTable.DROP_TABLE);
		database.execSQL(FestivalTable.CREATE_TABLE);
		database.execSQL(FestivalVideoTable.DROP_TABLE);
		database.execSQL(FestivalVideoTable.CREATE_TABLE);
		database.execSQL(LookUpCountryTable.DROP_TABLE);
		database.execSQL(LookUpCountryTable.CREATE_TABLE);
		database.execSQL(PartyTable.DROP_TABLE);
		database.execSQL(PartyTable.CREATE_TABLE);
		database.execSQL(PerformanceTable.DROP_TABLE);
		database.execSQL(PerformanceTable.CREATE_TABLE);
		database.execSQL(PlacesTable.DROP_TABLE);
		database.execSQL(PlacesTable.CREATE_TABLE);
		database.execSQL(StageTable.DROP_TABLE);
		database.execSQL(StageTable.CREATE_TABLE);
	}

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}