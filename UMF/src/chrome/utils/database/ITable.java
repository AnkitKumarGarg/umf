package chrome.utils.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public interface ITable 
{
	public 	void	 			insertIntoTable				(IRecords beanClassObject);
	public 	IRecords			fetchObjForSingleRow		(IRecords BeanClassObject,int rowId,SQLiteDatabase database);
	public 	ArrayList<IRecords>	fetchAllRow					(SQLiteDatabase database);
	public 	ArrayList<IRecords> fetchAllRowsWithCondition	(SQLiteDatabase database, String strQuery);
	public	String				getTableName();
	public	ContentValues		getContentValues();
	public 	String				getDropTable();
	public 	String				getCreateTable();
	public 	boolean				update(IRecords BeanClassObject , SQLiteDatabase database);
	public ArrayList<IRecords> fetchManyRowsByGivenValue(SQLiteDatabase database , String value);
} 