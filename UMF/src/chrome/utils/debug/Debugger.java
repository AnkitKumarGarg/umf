package chrome.utils.debug;

import android.util.Log;

/**
 * @company Chrome Infotech
 * @author Ankit Kumar Garg
 * @category Hold all methods for Debugging purpose
 * 
 */
public class Debugger implements IDebug 
{
	/**
	 * For store that Calling class is Debugable or not
	 */
	private boolean isDebugable = true;

	/**
	 * Constructor which assign value to isDebugable from isDebugable() method
	 * of calling class
	 * 
	 * @param IDebug
	 *            interface reference which able to hold context of calling
	 *            class because calling class must implement IDebug for get
	 *            debugging facility
	 */
	public Debugger(IDebug iConstant) 
	{
		isDebugable = iConstant.isDebugable();
	}

	/**
	 * 
	 * @return MethodName of calling method, if it is call by logForFunction()
	 *         and logForOutFunction()
	 */
	private String getMethodName() 
	{
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		return ste[4].getMethodName();
	}

	/**
	 * 
	 * @return MethodName of calling method, if it is static and call by
	 *         logForFunction() and logForOutFunction()
	 */
	private static String staticGetMethodName() 
	{
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		return ste[4].getMethodName();
	}

	/**
	 * 
	 * @return class name of calling method, if it is call by logForFunction()
	 *         and logForOutFunction()
	 * @see if you are use this method by call out side, change depth = 5
	 */
	private String getClassName() 
	{
		int depth = 4;
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		String strFullNameOfClass = ste[depth].getClassName();
		int numPosOfLastDot = strFullNameOfClass.lastIndexOf(".");
		String strActualNameOfClass = strFullNameOfClass.substring(	numPosOfLastDot + 1, strFullNameOfClass.length());
		return strActualNameOfClass;
	}

	/**
	 * 
	 * @return class name of static calling method, if it is call by
	 *         logForFunction() and logForOutFunction()
	 * @see if you are use this method by call out side, change depth = 5
	 */
	private static String staticGetClassName() 
	{
		int depth = 4;
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		String strFullNameOfClass = ste[depth].getClassName();
		int numPosOfLastDot = strFullNameOfClass.lastIndexOf(".");
		String strActualNameOfClass = strFullNameOfClass.substring(	numPosOfLastDot + 1, strFullNameOfClass.length());
		return strActualNameOfClass;
	}

	/**
	 * @throws information
	 *             on cat log that called function start
	 */
	public void logForInFunction(int priority) 
	{
		if (FLAG_MAIN && (priority <= DEBUG_ABLE_PRIORITY_LEVEL) && isDebugable) 
		{
			Log.e("Log", getClassName() + "  Inside Of   " + getMethodName());
		}
	}

	/**
	 * @throws information
	 *             on cat log that called function start
	 */
	public void logForInFunction() 
	{
		if (FLAG_MAIN && DEBUG_ABLE_PRIORITY_LEVEL > 3 && isDebugable) 
		{
			Log.e("Log", getClassName() + "  Inside Of   " + getMethodName());
		}
	}

	/**
	 * @throws it
	 *             throws information on cat log that called function end
	 */
	public void logForOutFunction(int priority) {

		if (FLAG_MAIN && priority <= DEBUG_ABLE_PRIORITY_LEVEL && isDebugable) 
		{
			Log.e("Log", getClassName() + "  Outside Of " + getMethodName());

		}
	}

	/**
	 * @throws information
	 *             on cat log that called function end
	 */
	public void logForOutFunction() 
	{

		if (FLAG_MAIN && DEBUG_ABLE_PRIORITY_LEVEL > 3 && isDebugable) 
		{
			Log.e("Log", getClassName() + "  Outside Of " + getMethodName());

		}
	}

	/**
	 * @throws information
	 *             on cat log with are come as arguments
	 */
	public void logForAll(int priority, String className, String methodName) 
	{
		if (FLAG_MAIN && priority <= DEBUG_ABLE_PRIORITY_LEVEL && isDebugable) 
		{
			Log.e("C:" + getClassName() + " M:" + getMethodName(), className + methodName);
		}
	}

	/**
	 * @throws information
	 *             on cat log with are come as arguments
	 */
	public void logForAll(String className, String methodName) 
	{
		if (FLAG_MAIN && DEBUG_ABLE_PRIORITY_LEVEL > 3 && isDebugable) 
		{
			Log.e("C:" + getClassName() + " M:" + getMethodName(), className + methodName);
		}
	}

	/**
	 * @throws information
	 *             on cat log that static called function start
	 */
	public static void staticlogForInFunction(boolean IS_DEBUGABLE, int priority) 
	{
		if (FLAG_MAIN && priority <= DEBUG_ABLE_PRIORITY_LEVEL) 
		{
			Log.e("Log", staticGetClassName() + "  Inside Of   " + staticGetMethodName());
		}
	}

	/**
	 * @throws information
	 *             on cat log that static called function start when priority is
	 *             not given as argument
	 */
	public static void staticlogForInFunction(boolean IS_DEBUGABLE) 
	{
		if (FLAG_MAIN && DEBUG_ABLE_PRIORITY_LEVEL > 3) 
		{
			Log.e("Log", staticGetClassName() + "  Inside Of   "	+ staticGetMethodName());
		}
	}

	/**
	 * @throws information
	 *             on cat log that static called function end when priority is
	 *             given
	 */
	public static void staticlogForOutFunction(boolean IS_DEBUGABLE, int priority) 
	{

		if (FLAG_MAIN && priority <= DEBUG_ABLE_PRIORITY_LEVEL) 
		{
			Log.e("Log", staticGetClassName() + "  Outside Of "	+ staticGetMethodName());

		}
	}

	/**
	 * @throws information
	 *             on cat log that static called function end when priority is
	 *             not given as argument
	 */
	public static void staticlogForOutFunction(boolean IS_DEBUGABLE) 
	{
		if (FLAG_MAIN && DEBUG_ABLE_PRIORITY_LEVEL > 3) 
		{
			Log.e("Log", staticGetClassName() + "  Outside Of "	+ staticGetMethodName());
		}
	}

	/**
	 * @throws information
	 *             on cat log that static called function middle where you used
	 *             when priority is given
	 */
	public static void staticlogForAll(boolean IS_DEBUGABLE, int priority,	String className, String methodName) 
	{
		if (FLAG_MAIN && priority <= DEBUG_ABLE_PRIORITY_LEVEL) 
		{
			Log.e("C:" + staticGetClassName() + " M:" + staticGetMethodName(),	className + methodName);
		}
	}

	/**
	 * @throws information
	 *             on cat log that static called function middle where you used
	 *             when priority is not given
	 */
	public static void staticlogForAll(boolean IS_DEBUGABLE, String className,	String methodName) 
	{
		if (FLAG_MAIN && DEBUG_ABLE_PRIORITY_LEVEL > 3) 
		{
			Log.e("C:" + staticGetClassName() + " M:" + staticGetMethodName(),	className + methodName);
		}
	}

	/**
	 * @info There is no use of this function but we need to define it because
	 *       it is method of parent interface IDebug
	 */
	public boolean isDebugable() 
	{
		return false;
	}

}