package chrome.utils.debug;

/**
 * 
 * @author Ankit Kumar Garg
 * @company Chrome Infotech
 * @info It hold all the flags for control debugging and methods
 * 
 */
public interface IDebug 
{
	/**
	 * @info FLAG for control debugging of all activity with implement IConstant
	 */
	public static final boolean FLAG_MAIN = false;

	/**
	 * @info Minimum priority level for debugging for all application
	 */
	public static final int DEBUG_ABLE_PRIORITY_LEVEL = 2;

	// Flags of all the classes which want to come in group of debugging

	// Activities
	public static final boolean IS_DEBUGABLE_FLAG_SPLASH_SCREEN 		= true;
	public static final boolean IS_DEBUGABLE_FLAG_MAIN_ACTIVITY 		= true;
	public static final boolean IS_DEBUGABLE_FLAG_ARTISTLIST 			= true;

	// Custom View
	public static final boolean IS_DEBUGABLE_FLAG_TITLE_BAR 			= true;

	// Utils
	public static final boolean IS_DEBUGABLE_FLAG_EXTENDED_BASE_ADAPTER = true;
	public static final boolean IS_DEBUGABLE_FLAG_GRID_VIEW_ADAPTER 	= true;
	public static final boolean IS_DEBUGABLE_FLAG_UPDATE_DATBASE 		= true;

	// Chrome Utils

	// Common Functions
	public static final boolean IS_DEBUGABLE_FLAG_FILE_OPERATION 		= true;
	public static final boolean IS_DEBUGABLE_FLAG_NETWORKING_FUNCTION 	= true;

	// Database
	public static final boolean IS_DEBUGABLE_FLAG_CONNECTION 			= true;
	public static final boolean IS_DEBUGABLE_FLAG_DATABASE 				= true;
	public static final boolean IS_DEBUGABLE_FLAG_DATABASE_HELPER 		= true;

	// Xml
	public static final boolean IS_DEBUGABLE_FLAG_PARSE 				= true;
	public static final boolean IS_DEBUGABLE_FLAG_XMLPARSER 			= true;

	// Models
	
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_ARTIST 			= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_DAY 			= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_FESTIVAL 		= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_FESTIVAL_VIDEO 	= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_LOOK_UP_COUNTRY = true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_PARTY 			= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_PERFORMANCE 	= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_PLACES 			= true;
	public static final boolean IS_DEBUGABLE_FLAG_MODEL_STAGE 			= true;
	
	

	/**
	 * 
	 * @info must implement method for all child class of interface IConstant if
	 *       child class want to use the standard way of debugging
	 */
	boolean isDebugable();
}