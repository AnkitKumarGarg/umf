package chrome.utils.xml;

import org.xml.sax.helpers.DefaultHandler;

import chrome.utils.database.ITable;

public abstract class ChromeDefaultHandler extends DefaultHandler 
{
	public abstract String getUrl();

	public abstract ITable getTable();

	public abstract void setUpdateInDatabase(boolean isUpdateInDatabase);

	public abstract boolean isUpdateInDatabase();
}
