package chrome.utils.xml;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import chrome.utils.commonFunctions.FileOperations;
import chrome.utils.database.Database;
import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

import android.content.res.AssetManager;

public class Parse implements IDebug 
{
	private Debugger debugger;
	private static final boolean IS_DEBUGABLE = IDebug.IS_DEBUGABLE_FLAG_PARSE;

	public static void parse(String FullString,	ChromeDefaultHandler defaultHandler) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		new XMLParser(FileOperations.getInputSourceFromString(FullString), defaultHandler);
	}// END parse()

	public static void parse(InputSource inputSource,ChromeDefaultHandler defaultHandler) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		new XMLParser(inputSource, defaultHandler);
	}// END parse()

	public static void parse(URL url, ChromeDefaultHandler defaultHandler) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		String string = FileOperations.getStringFromUrlByBuffer(url);
		new XMLParser(FileOperations.getInputSourceFromString(string),	defaultHandler);
	}// END parse()

	public static void parse(String stringUrl,ChromeDefaultHandler defaultHandler, String setNull) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		String string = FileOperations.getStringFromUrlByHttpClient(stringUrl);
		new XMLParser(FileOperations.getInputSourceFromString(string),	defaultHandler);
	}// END parse()

	public static void parse(InputStream inputStream, ChromeDefaultHandler defaultHandler) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		new XMLParser(new InputSource(inputStream), defaultHandler);
	}// END parse()

	public synchronized static void parse(ChromeDefaultHandler defaultHandler) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		try 
		{
			parse(new URL(defaultHandler.getUrl()), defaultHandler);
		}
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
	}// END parse()

	public static void parse(AssetManager assetManager, String FileName,ChromeDefaultHandler defaultHandler) 
	{
		Debugger.staticlogForInFunction(IS_DEBUGABLE);
		InputStream inputStream;
		try 
		{
			inputStream = assetManager.open(FileName);
			new XMLParser(new InputSource(inputStream), defaultHandler);
		}
		catch (IOException e) 
		{
			Debugger.staticlogForInFunction(IS_DEBUGABLE);
			Debugger.staticlogForAll(IS_DEBUGABLE, "Exception is occour ",	"For get" + FileName + " in fetch from Assets ");
			e.printStackTrace();
		}
	}// END parse()

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}// END Parse