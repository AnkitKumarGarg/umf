package chrome.utils.xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import chrome.utils.debug.Debugger;
import chrome.utils.debug.IDebug;

//Parser for all actvity , they use it by just pass "inputsource of xml" and "its respective handler

public class XMLParser implements IDebug 
{
	private Debugger debugger;
	private static final boolean IS_DEBUGABLE = IS_DEBUGABLE_FLAG_XMLPARSER;

	public XMLParser(InputSource inputSource,ChromeDefaultHandler defaultHandler) 
	{
		debugger = new Debugger(this);
		debugger.logForInFunction();
		try 
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader xr = parser.getXMLReader();
			xr.setContentHandler(defaultHandler);
			xr.parse(inputSource);
			// Or we can also use above 3 line by single line
			// parser.parser(inputSource,defaultHandler);
		}
		catch (Exception e) 
		{
			debugger.logForAll(1, "Exception ", "occour " + e.toString());
			e.printStackTrace();
		}// end try
		debugger.logForOutFunction();
	}// END XMLParser( , )

	@Override
	public boolean isDebugable() 
	{
		return IS_DEBUGABLE;
	}
}// END XMLParser